#!/bin/bash

for x in /sys/kernel/mm/hugepages/hugepages-2048kB/nr_hugepages /sys/devices/system/node/node?/hugepages/hugepages-2048kB/nr_hugepages
do
  echo 2048 > $x
done

[ ! -d /mnt/huge ] && mkdir -p /mnt/huge
mount -t hugetlbfs nodev /mnt/huge
