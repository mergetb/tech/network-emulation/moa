#!/bin/bash

sudo pip3 install --upgrade \
  grpcio \
  grpcio-tools \
  futures \
  pyyaml \
  networkx \
  matplotlib==2.0.2 \
  pygraphviz \
