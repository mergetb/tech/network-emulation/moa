// test driver for moactld

package main

import (
	"context"
	"flag"
	"fmt"
	"gitlab.com/mergetb/tech/network-emulation/moa/pkg/common"
	mc "gitlab.com/mergetb/tech/network-emulation/moa/pkg/moactld"
	"log"
	"os"
)

type Options struct {
	help    bool
	version bool
	debug   bool
	host    string
	port    int
	id      int
}

func init_options() *Options {
	options := Options{}

	flag.BoolVar(&options.version, "version", false, "print the version number")
	flag.BoolVar(&options.version, "v", false, "alias for -version")
	flag.BoolVar(&options.help, "help", false, "print the version number")
	flag.BoolVar(&options.help, "h", false, "alias for -help")
	flag.BoolVar(&options.debug, "debug", false, "enable verbose logging")
	flag.BoolVar(&options.debug, "d", false, "alias for -debug")
	flag.StringVar(&options.host, "host", "localhost", "host where moad is running")
	flag.IntVar(&options.port, "port", 50052, "port moad is listening on")
	flag.IntVar(&options.id, "id", 100, "emulation ID")
	flag.Parse()

	if options.help {
		flag.PrintDefaults()
		os.Exit(0)
	}

	if options.version {
		fmt.Printf("%s\n", common.Version)
		os.Exit(0)
	}

	return &options
}

func main() {
	options := init_options()

	err := mc.WithMoaControl("localhost", func(cli mc.MoaControlClient) error {
		var err error

		// configure to run on localhost
		_, err = cli.Init(context.TODO(), &mc.InitRequest{EmulationHost: options.host, EmulationPort: int32(options.port), EmulationId: int32(options.id)})
		if err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		log.Fatal(err)
	}
}
