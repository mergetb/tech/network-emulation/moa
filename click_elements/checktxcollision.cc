#include <click/config.h>
#include <click/args.hh>
#include <click/glue.hh>
#include <click/error.hh>
#include <click/packet_anno.hh>
#include <click/confparse.hh>
#include "checktxcollision.hh"

CLICK_DECLS

CheckTXCollision::CheckTXCollision()
{
}

CheckTXCollision::~CheckTXCollision()
{
}

int
CheckTXCollision::configure(Vector<String> &conf, ErrorHandler* errh)
{
  int port;
  if(Args(conf, this, errh)
     .read_mp("PORT", port) 
     .complete() < 0)
    return errh->error("Bad Configuration");

  _wireless_port = port;
  return 0;
}

int
CheckTXCollision::initialize(ErrorHandler *errh)
{
  _collision_time = _model.getCollisionTime(_wireless_port);
  _our_nonce = _model.getRandom64();
  _is_colliding = false;
  return 0;
}

Packet*
CheckTXCollision::simple_action(Packet* p)
{
  if(!_is_colliding || *(_collision_time) < Timestamp::now())
  {
    if(_model.checkTXCollision(_wireless_port))
    {
      _is_colliding = true;
      p->kill();
      _collision_time = _model.getCollisionTime(_wireless_port);
      return NULL;
    }
    else
    {
      _is_colliding = false;
    }
  }
  else
  {
    p->kill();
    return NULL;
  }

  p->set_anno_u64(PERFCTR_ANNO_OFFSET, _our_nonce);
  _model.setCollision(_wireless_port, p->length(), _our_nonce);
  _our_nonce++;
  return p;
}

CLICK_ENDDECLS
EXPORT_ELEMENT(CheckTXCollision)
