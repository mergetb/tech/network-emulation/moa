#include "wirelessmanager.hh"
#include <click/string.hh>
#include <string>
#include <set>
#include "wireless/wirelessnode.hh"
#include <click/args.hh>

CLICK_DECLS

WirelessManager::WirelessManager()
  : _timer(this)
{
}

WirelessManager::~WirelessManager()
{
}

int
WirelessManager::configure(Vector<String> &conf, ErrorHandler *errh)
{
  String topo_filename = "NONE";
  String mob_filename = "NONE";
  String mob_type = "NONE";

  int interval = 1000;
  bool m_active = false;

  if(Args(conf, this, errh)
     .read("TOPOFILE", StringArg(), topo_filename)
     .read("INTERVAL", interval)
     .read("MOBILITY_ACTIVE", m_active)
     .read("MOBFILE", StringArg(), mob_filename)
     .read("MOBMODEL", StringArg(), mob_type)
     .complete() < 0)
    return -1;

  if(topo_filename != "NONE")
  {
    _model.readTopoFromFile(std::string(topo_filename.c_str()));
  }

  if(mob_filename != "NONE" or mob_type != "NONE")
  {
    if(mob_type == "NONE" or mob_filename == "NONE")
    {
      click_chatter("Both mobility type and file must be specified");
      return -1;
    }
    _model.configureMobility(std::string(mob_filename.c_str()),
                             std::string(mob_type.c_str()));
  }
  _interval = interval;
  _mobility_enabled = m_active;

  return 0;
}

int
WirelessManager::initialize(ErrorHandler *errh)
{ 
  _timer.initialize(this);
  _model.update();
  if(_mobility_enabled)
  {
    _timer.schedule_after_msec(_interval);
  }
 
    
  return 0;
}

void
WirelessManager::run_timer(Timer* t)
{
  if(_mobility_enabled)
  {
     _model.update();
     _timer.reschedule_after_msec(_interval);
  }

}
  
CLICK_ENDDECLS
ELEMENT_REQUIRES(WirelessModel)
EXPORT_ELEMENT(WirelessManager)
  
