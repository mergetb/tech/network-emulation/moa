// mimes interconnected by a switch

a :: Mime(
  IP       10.0.0.1,
  MAC      00:00:00:00:00:01,

  COMPUTE0 10ms, 
  DATA0    100000000,
  DST0     10.0.0.2,
  MAC0     00:00:00:00:00:02,

  COMPUTE1 10ms, 
  DATA1    100000000,
  DST1     10.0.0.3,
  MAC1     00:00:00:00:00:03,

  STOP     true
);

b :: Mime(
  IP      10.0.0.2,
  MAC     00:00:00:00:00:02,

  COMPUTE0 50ms
);

c :: Mime(
  IP      10.0.0.3,
  MAC     00:00:00:00:00:03,

  COMPUTE0 50ms
);

s :: EtherSwitch();

a -> [0]s[0] -> Queue(100000) -> a;
b -> [1]s[1] -> Queue(100000) -> b;
c -> [2]s[2] -> Queue(100000) -> c;
