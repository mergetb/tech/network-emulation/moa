import sys
sys.path.insert(0, '../')

from ClickPrimitives import ClickPrimitives
from MoaErrors import *

def main():
    unitTests()
    

def unitTests():
    cp = ClickPrimitives()
    
    testCreateElement(cp)
    testCreateClassifier(cp)
    testCreateLink(cp)
    testCreateInputDevice(cp)
    testCreateOutputDevice(cp)
    testCreateVXLANDemux(cp)
    testCreateVXLANEncap(cp)
    testCreateBroadcastMPL(cp)


def validateTest(expected, result, error_type, error_msg_hdr = ""):
    if(expected != result):
        raise MoaTestError(error_type, "%s %s != %s" % (error_msg_hdr, expected, result))
    
def testCreateElement(cp):
    #Test no args
    (ele_name, ele_declaration) = cp.createElement("test0", "ThreadSafeQueue", "")

    test_name = "test0"
    test_decl = "test0 :: ThreadSafeQueue();"

    validateTest(test_name, ele_name, "Element Creation Error")
    validateTest(test_decl, ele_declaration, "Element Creation Error")

    
    # test 1 args
    test_name = "test"
    test_decl = "test :: ThreadSafeQueue(1000);"
    (ele_name, ele_declaration) = cp.createElement("test", "ThreadSafeQueue", "1000")

    validateTest(test_name, ele_name, "Element Creation Error")
    validateTest(test_decl, ele_declaration, "Element Creation Error")

    # Test multiple args
    test_name = "test"
    test_decl = "test :: LinkUnqueue(0ms, 1Gbps);"
    (ele_name, ele_declaration) = cp.createElement("test", "LinkUnqueue", "0ms, 1Gbps")

    validateTest(test_name, ele_name, "Element Creation Error")
    validateTest(test_decl, ele_declaration, "Element Creation Error")
    
    print "Element Creation Tests Passed"
    return 0

def testCreateClassifier(cp):
    # Test empty classifier
    try:
        (ele_name, ele_declaration) = cp.createClassifier("test0", [])
    except MoaInputError as mie:
        if(mie.iput != "classifierOptions = None"):
            raise 

    # Test IP classifier

    test_name = "test_classifier"
    test_decl = "test_classifier :: Classifier(12/0800, -);"
    (ele_name, ele_declaration) = cp.createClassifier("test", ["IP"])

    validateTest(test_name, ele_name, "Classifier Creation Error")
    validateTest(test_decl, ele_declaration, "Classifier Creation Error")
    

    # Test ARP REQUEST Classifier
    test_name = "test_classifier"
    test_decl = "test_classifier :: Classifier(12/0806 20/0001, -);"
    (ele_name, ele_declaration) = cp.createClassifier("test", ["ARP_REQUEST"])

    validateTest(test_name, ele_name, "Classifier Creation Error")
    validateTest(test_decl, ele_declaration, "Classifier Creation Error")

    # Test ARP REPLY Classifier
    test_name = "test_classifier"
    test_decl = "test_classifier :: Classifier(12/0806 20/0002, -);"
    (ele_name, ele_declaration) = cp.createClassifier("test", ["ARP_REPLY"])

    validateTest(test_name, ele_name, "Classifier Creation Error")
    validateTest(test_decl, ele_declaration, "Classifier Creation Error")
    
    # Test IP and all ARP Classifier
    test_name = "test_classifier"
    test_decl = "test_classifier :: Classifier(12/0800, 12/0806 20/0001, 12/0806 20/0002, -);"
    (ele_name, ele_declaration) = cp.createClassifier("test", ["IP", "ARP_REQUEST", "ARP_REPLY"])

    validateTest(test_name, ele_name, "Classifier Creation Error")
    validateTest(test_decl, ele_declaration, "Classifier Creation Error")

    print "Classifier Creation Tests Passed"
    return 0
    

def testCreateLink(cp):
    # Test default options
    (link_pipeline, ele_declarations) = cp.createLink("test0", "out")
    test_pipeline = "test0_in_queue -> test0_codel -> test0_loss -> test0_bwdelay -> out;"
    test_declarations = ["test0_in_queue :: ThreadSafeQueue(1000);",
                         "test0_codel :: CoDel();",
                         "test0_loss :: RandomSample(DROP 0);",
                         "test0_bwdelay :: LinkUnqueue(0ms, 1Gbps);"]


    validateTest(test_pipeline, link_pipeline, "Link Creation Error", error_msg_hdr = "Pipeline")
    validateTest(test_declarations, ele_declarations, "Link Creation Error", error_msg_hdr = "Declarations")

    # Test configured link options
    (link_pipeline, ele_declarations) = cp.createLink("test", "out", loss = "0.2", bandwidth = "2Gbps", delay = "1ms")
    test_pipeline = "test_in_queue -> test_codel -> test_loss -> test_bwdelay -> out;"
    test_declarations = ["test_in_queue :: ThreadSafeQueue(1000);",
                         "test_codel :: CoDel();",
                         "test_loss :: RandomSample(DROP 0.2);",
                         "test_bwdelay :: LinkUnqueue(1ms, 2Gbps);"]

    validateTest(test_pipeline, link_pipeline, "Link Creation Error", error_msg_hdr = "Pipeline")
    validateTest(test_declarations, ele_declarations, "Link Creation Error", error_msg_hdr = "Declarations")

    # Test without Codel
    (link_pipeline, ele_declarations) = cp.createLink("test", "out", useCodel = False)
    test_pipeline = "test_in_queue -> test_loss -> test_bwdelay -> out;"
    test_declarations = ["test_in_queue :: ThreadSafeQueue(1000);",
                         "test_loss :: RandomSample(DROP 0);",
                         "test_bwdelay :: LinkUnqueue(0ms, 1Gbps);"]
    
    validateTest(test_pipeline, link_pipeline, "Link Creation Error", error_msg_hdr = "Pipeline")
    validateTest(test_declarations, ele_declarations, "Link Creation Error", error_msg_hdr = "Declarations")

    # Test with Extra Elements
    (link_pipeline, ele_declarations) = cp.createLink("test", "out", additional_elements = ["special1", "special2"])
    test_pipeline = "test_in_queue -> test_codel -> test_loss -> special1 -> special2 -> test_bwdelay -> out;"
    test_declarations = ["test_in_queue :: ThreadSafeQueue(1000);",
                         "test_codel :: CoDel();",
                         "test_loss :: RandomSample(DROP 0);",
                         "test_bwdelay :: LinkUnqueue(0ms, 1Gbps);"]
    
    validateTest(test_pipeline, link_pipeline, "Link Creation Error", error_msg_hdr = "Pipeline")
    validateTest(test_declarations, ele_declarations, "Link Creation Error", error_msg_hdr = "Declarations")

    # Test with input element
    (link_pipeline, ele_declarations) = cp.createLink("test", "out", inbound_element = "hi")
    test_pipeline = "hi -> test_in_queue -> test_codel -> test_loss -> test_bwdelay -> out;"
    test_declarations = ["test_in_queue :: ThreadSafeQueue(1000);",
                         "test_codel :: CoDel();",
                         "test_loss :: RandomSample(DROP 0);",
                         "test_bwdelay :: LinkUnqueue(0ms, 1Gbps);"]
    
    validateTest(test_pipeline, link_pipeline, "Link Creation Error", error_msg_hdr = "Pipeline")
    validateTest(test_declarations, ele_declarations, "Link Creation Error", error_msg_hdr = "Declarations")

    print "Link Creation Tests Passed"
    return 0

def testCreateInputDevice(cp):
    # test with default options
    try:
        (input_pipeline, ele_declarations) = cp.createDeviceInput("test", 0)
    except MoaInputError as mie:
        if(mie.iput != None):
            raise 

    # test with specified queue
    (input_pipeline, ele_declarations) = cp.createDeviceInput("test", "0", queue = 1, nextElement = 'test_classifier')
    test_pipeline = "test_dpdk_in -> test_classifier;"
    test_declarations = ["test_dpdk_in :: FromDPDKDevice(0, QUEUE 1, N_QUEUES 1);"]

    validateTest(test_pipeline, input_pipeline, "Input Device Creation Error", error_msg_hdr = "Pipeline")
    validateTest(test_declarations, ele_declarations, "Input Device Creation Error", error_msg_hdr = "Declarations")


    # test using VXLANDemux
    (input_pipeline, ele_declarations) = cp.createDeviceInput("test", "0", nextElement = "vxlan_demux")
    test_pipeline = "test_dpdk_in -> vxlan_demux;"
    test_declarations = ["test_dpdk_in :: FromDPDKDevice(0);"]

    validateTest(test_pipeline, input_pipeline, "Input Device Creation Error", error_msg_hdr = "Pipeline")
    validateTest(test_declarations, ele_declarations, "Input Device Creation Error", error_msg_hdr = "Declarations")

    
    # test using all    
    (input_pipeline, ele_declarations) = cp.createDeviceInput("test", "0", queue = 1, nextElement = "vxlan_demux")
    test_pipeline = "test_dpdk_in -> vxlan_demux;"
    test_declarations = ["test_dpdk_in :: FromDPDKDevice(0, QUEUE 1, N_QUEUES 1);"]

    validateTest(test_pipeline, input_pipeline, "Input Device Creation Error", error_msg_hdr = "Pipeline")
    validateTest(test_declarations, ele_declarations, "Input Device Creation Error", error_msg_hdr = "Declarations")
    
    print "Input Device Creation Passed"
    return 0

def testCreateOutputDevice(cp):
    # test with default options
    (output_pipeline, ele_declarations) = cp.createDeviceOutput("test", "0")
    test_pipeline = ""
    test_declarations = ["test_dpdk_out :: ToDPDKDevice(0);"]
                         
    validateTest(test_pipeline, output_pipeline, "Output Device Creation Error", error_msg_hdr = "Pipeline")
    validateTest(test_declarations, ele_declarations, "Output Device Creation Error", error_msg_hdr = "Declarations")
    

    # test with specified queue
    (output_pipeline, ele_declarations) = cp.createDeviceOutput("test", "0", queue = 1)
    test_pipeline = ""
    test_declarations = ["test_dpdk_out :: ToDPDKDevice(0, QUEUE 1, N_QUEUES 1);"]
                       
    validateTest(test_pipeline, output_pipeline, "Output Device Creation Error", error_msg_hdr = "Pipeline")
    validateTest(test_declarations, ele_declarations, "Output Device Creation Error", error_msg_hdr = "Declarations")
   
    print "Output Device Creation Passed"
    return 0

def testCreateVXLANDemux(cp):
    try:
        cp.createVXLANDemux("test", [])
    except MoaInputError as mie:
        if(mie.iput != []):
            raise 

    (output_name, ele_declaration) = cp.createVXLANDemux("", ["121"])
    test_name = "vxlan_demux"
    test_declaration = "vxlan_demux :: VXLANDemux(VXLAN 121);"

    validateTest(test_name, output_name, "VXLAN Demux Creation Error", error_msg_hdr = "Element Name")
    validateTest(test_declaration, ele_declaration, "VXLAN Creation Error", error_msg_hdr = "Declaration")

    (output_name, ele_declaration) = cp.createVXLANDemux("test_vxlan_demux", ["121"])
    test_name = "test_vxlan_demux"
    test_declaration = "test_vxlan_demux :: VXLANDemux(VXLAN 121);"

    validateTest(test_name, output_name, "VXLAN Demux Creation Error", error_msg_hdr = "Element Name")
    validateTest(test_declaration, ele_declaration, "VXLAN Creation Error", error_msg_hdr = "Declaration")

    (output_name, ele_declaration) = cp.createVXLANDemux("test_vxlan_demux", ["121", "131"])
    test_name = "test_vxlan_demux"
    test_declaration = "test_vxlan_demux :: VXLANDemux(VXLAN 121, VXLAN 131);"

    validateTest(test_name, output_name, "VXLAN Demux Creation Error", error_msg_hdr = "Element Name")
    validateTest(test_declaration, ele_declaration, "VXLAN Creation Error", error_msg_hdr = "Declaration")

    print "VXLAN Demux creation Passed"
    return 0

def testCreateVXLANEncap(cp):
    try:
        cp.createVXLANEncap("", "121", "10.0.0.1", "10.0.0.2", "AA:BB:CC:DD:EE:FF", "00:11:22:33:44:55", None)
    except MoaInputError as mie:
        if(mie.iput != None):
            raise
    try:
        cp.createVXLANEncap("", "121", "10.0.0.1", "10.0.0.2", "AA:BB:CC:DD:EE:FF", "00:11:22:33:44:55", "")
    except MoaInputError as mie:
        if(mie.iput != ""):
            raise

        
    (output_pipeline, ele_declarations) = cp.createVXLANEncap("", "121", "10.0.0.1", "10.0.0.2", "AA:BB:CC:DD:EE:FF",
                                                              "00:11:22:33:44:55", "out_dev")

    test_pipeline = "encap_vxlan_121 -> CheckIPHeader(14) -> out_dev;"
    test_declaration = "encap_vxlan_121 :: VXLANEncap(SRC_MAC AA:BB:CC:DD:EE:FF, DST_MAC 00:11:22:33:44:55, SRC_IP 10.0.0.1, DST_IP 10.0.0.2, VXLAN_ID 121);"

    validateTest(test_pipeline, output_pipeline, "VXLAN Encap Creation Error", error_msg_hdr = "Pipeline")
    validateTest(test_declaration, ele_declarations, "VXLAN Encap Creation Error", error_msg_hdr = "Declarations")

    (output_pipeline, ele_declarations) = cp.createVXLANEncap("test", "121", "10.0.0.1", "10.0.0.2", "AA:BB:CC:DD:EE:FF",
                                                              "00:11:22:33:44:55", "out_dev")

    test_pipeline = "test -> CheckIPHeader(14) -> out_dev;"
    test_declaration = "test :: VXLANEncap(SRC_MAC AA:BB:CC:DD:EE:FF, DST_MAC 00:11:22:33:44:55, SRC_IP 10.0.0.1, DST_IP 10.0.0.2, VXLAN_ID 121);"

    validateTest(test_pipeline, output_pipeline, "VXLAN Encap Creation Error", error_msg_hdr = "Pipeline")
    validateTest(test_declaration, ele_declarations, "VXLAN Encap Creation Error", error_msg_hdr = "Declarations")

    print "VXLAN Encap Creation Passed"
    return 0


def testCreateBroadcastMPL(cp):
    (output_pipelines, ele_declaration) = cp.createBroadcastMPL("test0", ["to_out1", "to_out2", "to_out3"])

    test_output_pipelines = ["test0[0] -> to_out1;",
                             "test0[1] -> to_out2;",
                             "test0[2] -> to_out3;"]
    
    test_declaration = "test0 :: Tee();"

    validateTest(test_output_pipelines, output_pipelines, "Broadcast MPL Creation Error", error_msg_hdr = "Output Pipelines")
    validateTest(test_declaration, ele_declaration, "Bradcast MPL Creation Error", error_msg_hdr = "Declarations")


    (output_pipelines, ele_declaration) = cp.createBroadcastMPL("test0", ["to_out1", "to_out2", "to_out5"])
    
    test_output_pipelines = ["test0[0] -> to_out1;",
                             "test0[1] -> to_out2;",
                             "test0[2] -> to_out5;"]
    
    test_declaration = "test0 :: Tee();"
                         
    validateTest(test_output_pipelines, output_pipelines, "Broadcast MPL Creation Error", error_msg_hdr = "Output Pipelines")
    validateTest(test_declaration, ele_declaration, "Bradcast MPL Creation Error", error_msg_hdr = "Declarations")

    
    print "Broadcast MPL Creation Passed"
    return 0;
    
if __name__ == "__main__":
    main()
