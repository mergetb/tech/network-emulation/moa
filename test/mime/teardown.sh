#!/bin/bash

if [[ $UID -ne 0 ]]; then
  echo "must be root"
  exit 1
fi

set -x

ip link del vgreen
ip link del vblue
ip netns del green
ip netns del blue
