#ifndef CLICK_SIMPLE_CSMA_HH
#define CLICK_SIMPLE_CSMA_HH

#include <click/element.hh>
#include <click/timestamp.hh>
#include <click/timer.hh>
#include <click/notifier.hh>
#include "wireless/wirelessmodel.hh"

CLICK_DECLS

class SimpleCSMA : public Element {
  public:
    SimpleCSMA() CLICK_COLD;
    ~SimpleCSMA() CLICK_COLD;

    const char *class_name() const     	{ return "SimpleCSMA"; }
    const char *port_count() const     	{ return "1/1"; }
    const char *processing() const     	{ return PULL; }

    int configure(Vector<String> &, ErrorHandler *) CLICK_COLD;
    int initialize(ErrorHandler*) CLICK_COLD;

    Packet* pull(int port);

    void run_timer(Timer*);

  private:
    WirelessModel& _model = WirelessModel::getInstance();
    bool _is_colliding;
    int _wireless_port;
    Packet* _our_p;
    ActiveNotifier _notifier;
    NotifierSignal _upstream_signal;


    Timer _timer;
    uint64_t _our_nonce;

};

CLICK_ENDDECLS
#endif
