#include <click/config.h>
#include "VXLANEncap.hh"
#include <click/etheraddress.hh>
#include <click/ipaddress.hh>
#include <click/args.hh>
#include <click/error.hh>
#include <click/glue.hh>
CLICK_DECLS

VXLANEncap::VXLANEncap()
{
}

VXLANEncap::~VXLANEncap()
{
}

int
VXLANEncap::initialize(ErrorHandler *)
{
  _id = 0;
  return 0;
}

int
VXLANEncap::configure(Vector<String> &conf, ErrorHandler *errh)
{
  click_ether ethh;
  click_ip iph;
  click_udp udph;
  struct vxlanhdr vxlanh;

  int vxlanid = 0;
  int protocol = -1;

  uint32_t vni_field = 0;
  
  memset(&iph, 0, sizeof(click_ip));
  memset(&ethh, 0, sizeof(click_ether));
  memset(&udph, 0, sizeof(click_udp));
  // vxlan things

  if (Args(conf, this, errh)
      .read_m("SRC_MAC", EtherAddressArg(), ethh.ether_shost)
      .read_m("DST_MAC", EtherAddressArg(), ethh.ether_dhost)
      .read_m("SRC_IP", iph.ip_src)
      .read_m("DST_IP", iph.ip_dst)
      .read_m("VXLAN_ID", vxlanid)
      .read("GPE", protocol)
      .complete() < 0)
    return -1;
  
  // Configure ethernet header (SRC and DST should be configured)
  ethh.ether_type = htons(ETHERTYPE_IP);

  // Configure IP header (except packet length and checksum, and id)
  iph.ip_p = IP_PROTO_UDP;    // Set protocol to UDP
  iph.ip_ttl = 254;
  iph.ip_v = 4;
  iph.ip_hl = sizeof(click_ip) >> 2;
  
  // Configure UDP header (except length)
  udph.uh_sport = htons(VXLAN_PORT);
  udph.uh_dport = htons(VXLAN_PORT);
  udph.uh_sum = 0;

  // Configure VXLAN header
  // Ignoring GPE for now.  We'll need to configure header if using GPE.
  vni_field = vxlan_vni_field(vxlanid);

  vxlanh.vx_flags = VXLAN_HF_VNI;
  vxlanh.vx_vni = htonl(vni_field);
  
  _ethh = ethh;
  _iph = iph;
  _udph = udph;
  _vxlanh = vxlanh;

  #if HAVE_FAST_CHECKSUM
    _iph.ip_sum = ip_fast_csum((unsigned char *) &_iph, sizeof(click_ip) >> 2);
  #else
    _iph.ip_sum = click_in_cksum((unsigned char *) &_iph, sizeof(click_ip));
  #endif

  
  return 0;
}

/**
 * This code is taken from elements/ip/ipencap.cc
 **/

inline void
VXLANEncap::update_cksum(click_ip *ip, int off) const
{
#if HAVE_INDIFFERENT_ALIGNMENT
    click_update_in_cksum(&ip->ip_sum, 0, ((uint16_t *) ip)[off/2]);
#else
    const uint8_t *u = (const uint8_t *) ip;
# if CLICK_BYTE_ORDER == CLICK_BIG_ENDIAN
    click_update_in_cksum(&ip->ip_sum, 0, u[off]*256 + u[off+1]);
# else
    click_update_in_cksum(&ip->ip_sum, 0, u[off] + u[off+1]*256);
# endif
#endif
}


Packet *
VXLANEncap::simple_action(Packet *p_in)
{
  // Make the packet writable
  uint16_t original_length = p_in->length();
  uint16_t udp_len = original_length + sizeof(click_udp) + sizeof(struct vxlanhdr);
  uint16_t ip_len = udp_len + sizeof(click_ip);
  
  
  WritablePacket *p_writable = p_in->push(sizeof(click_ether) + sizeof(click_ip) + sizeof(click_udp) + sizeof(struct vxlanhdr));

  click_ether *ethh = reinterpret_cast<click_ether *> (p_writable->data());
  click_ip *iph = reinterpret_cast<click_ip *> (p_writable->data() + sizeof(click_ether));
  click_udp *udph = reinterpret_cast<click_udp *> (p_writable->data() + sizeof(click_ether) + sizeof(click_ip));
  struct vxlanhdr *vxlanh = reinterpret_cast<struct vxlanhdr *> (p_writable->data() + sizeof(click_ether) + sizeof(click_ip) + sizeof(click_udp));

  // We'll copy over backwards, starting with vxlanhdr

  memcpy(vxlanh, &_vxlanh, sizeof(struct vxlanhdr));

  // vxlan header is fully configured, so we just move onto udp header

  memcpy(udph, &_udph, sizeof(click_udp));

  // Configure UDP length (UDP Header + Payload)
  udph->uh_ulen = htons(udp_len);

  // Copy IP header

  memcpy(iph, &_iph, sizeof(click_ip));

  // Configre IP length, IP id, and IP checksum
  iph->ip_len = htons(ip_len);
  iph->ip_id = htons(_id.fetch_and_add(1));  // If not using a test environment, may want random ids.

  // We update the checksum with 2 halfwords first
  update_cksum(iph, 2);
  update_cksum(iph, 4);

  p_writable->set_ip_header(iph, sizeof(click_ip));

  // Finally, encapsulate with Ethernet header
  memcpy(ethh, &_ethh, sizeof(click_ether));

  p_writable->set_mac_header(p_writable->data(), sizeof(click_ether));

  return p_writable;
}
  
CLICK_ENDDECLS
CLICK_ENDDECLS
EXPORT_ELEMENT(VXLANEncap)
ELEMENT_MT_SAFE(VXLANEncap)
