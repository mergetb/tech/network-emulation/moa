#include <click/config.h>
#include "reachabilityswitch.hh"
#include <click/glue.hh>
#include <click/args.hh>
#include <click/straccum.hh>
#include <click/error.hh>
#include <click/confparse.hh>
#include <click/string.hh>
#include <clicknet/ether.h>
#include "wireless/wirelessnode.hh"
#include <algorithm>

CLICK_DECLS

ReachabilitySwitch::ReachabilitySwitch()
{
}

ReachabilitySwitch::~ReachabilitySwitch()
{
  for(auto& addr : _port_to_address_map)
  {
    delete addr;
  }
}

// Specify option based reachability!
int
ReachabilitySwitch::configure(Vector<String> &conf, ErrorHandler* errh)
{
  Vector<String> macs;
  if (Args(conf, this, errh)
      .read_all("MAC", macs)
      .complete() < 0)
    return errh->error("Bad Configuration");

  for(int c = 0; c < macs.size(); c++)
  {
    EtherAddress* address = new EtherAddress();
    EtherAddressArg().parse(macs[c], *address);
    _port_to_address_map.push_back(address);
    _address_to_port_map[address] = c;
  }

  _forwarding_table.resize(noutputs());
  _pointer_status.resize(noutputs());
  return 0;
}

int
ReachabilitySwitch::initialize(ErrorHandler *errh)
{
  _model.setPortAddressMappings(_port_to_address_map);
  return 0;
}

void
ReachabilitySwitch::push(int port, Packet* p)
{
  std::set<WirelessNode*>::iterator it;
  int num_reachable;

  it = _model.getSetPointer(port);
  if(it != _pointer_status[port])
  {
    std::lock_guard<std::mutex> guard(_model._model_mutex);
    _forwarding_table[port] = _model.getReachability(port);
    _pointer_status[port] = it;
  }
  
  num_reachable = _forwarding_table[port].size();
  if(num_reachable < 1)
  {
    p->kill();
    return;
  }

  if(!p->has_mac_header())
  {
    p->kill();
    return;
  }

  const struct click_ether* ehdr = p->ether_header();
  EtherAddress eaddr(ehdr->ether_dhost);

  if(eaddr.is_broadcast())
  { 
    broadcast(port, p);
  }
  else
  {
    WirelessNode* destination = _model.getNode(&eaddr);
    it = _forwarding_table[port].find(destination);
    if(it == _forwarding_table[port].end())
    {
      p->kill();
      return;
    }
    
    output((*it)->getPort()).push(p);
  }
}

void
ReachabilitySwitch::broadcast(int port, Packet *p)
{
  std::set<WirelessNode*>::iterator it;
  int remaining_to_forward = 0;

  it = _model.getSetPointer(port);
  if(it != _pointer_status[port])
  {
    std::lock_guard<std::mutex> guard(_model._model_mutex);
    _forwarding_table[port] = _model.getReachability(port);
    _pointer_status[port] = it;
  }
  
  remaining_to_forward = _forwarding_table[port].size();
  
  if (remaining_to_forward < 1)
  {
    p->kill();
    return;
  }
    
  for (auto const& node : _forwarding_table[port])
  {
    Packet *new_p = NULL;
    if (remaining_to_forward == 1)
    {
      new_p = p;
    }
    else
    {
      new_p = p->clone();
    }

    output(node->getPort()).push(new_p);
    remaining_to_forward--;
  }
}
    
CLICK_ENDDECLS
EXPORT_ELEMENT(ReachabilitySwitch)
