vtep4_in  :: FromDevice(vtep4)
vtep4_out :: ToDevice(vtep4)
vtep7_in  :: FromDevice(vtep7)
vtep7_out :: ToDevice(vtep7)

vtep4_in -> Queue(10000000) -> vtep7_out;
vtep7_in -> Queue(10000000) -> vtep4_out;
