## Link 1 Elements
link_1_left_in_queue :: ThreadSafeQueue(1000);
link_1_left_codel :: CoDel();
link_1_left_loss :: RandomSample(DROP 0.1);
link_1_left_bwdelay :: LinkUnqueue(10ms, 10Gbps);
link_1_right_in_queue :: ThreadSafeQueue(1000);
link_1_right_codel :: CoDel();
link_1_right_loss :: RandomSample(DROP 0.1);
link_1_right_bwdelay :: LinkUnqueue(10ms, 10Gbps);

## Link 1 Pipeline
vxlan[0] -> link_1_left_in_queue -> link_1_left_codel -> link_1_left_loss -> link_1_left_bwdelay -> vxlan_encap2;
vxlan[1] -> link_1_right_in_queue -> link_1_right_codel -> link_1_right_loss -> link_1_right_bwdelay -> vxlan_encap1;

