/* 2 Node Moa Testing Topology
 * ===========================
 *
 *             -------
 *             | moa |
 *             -------
 *                | 
 *        ------------------
 *        |     switch     |
 *        ------------------
 *            |        |
 *          -----    -----
 *          | a |    | b |
 *          -----    -----
 *
 */

topo = {
  name: 'moa-2node-switched',
  nodes: [buster('a'), buster('b'), bullseye('moa')],
  switches: [ cumulus('sw') ],
  links: [
    v2v('moa', 1, 'sw', 1),
    v2v('a',   1, 'sw', 2, {mac: {a: '04:07:00:01:70:1c'}}),
    v2v('b',   1, 'sw', 3, {mac: {b: '04:07:00:01:70:1d'}}),
  ],
}

function buster(name) {
  return {
    name: name,
    defaultnic: 'e1000',
    image: 'debian-buster',
    cpu: { cores: 2 },
    memory: { capacity: GB(4) },
  }
}

function bullseye(name) {
  return {
    name: name,
    defaultnic: 'e1000',
    image: 'debian-kass',
    cpu: { cores: 2, passthru: true }, /* passthru required for fastclick */
    memory: { capacity: GB(4) },
  }
}

function cumulus(name) {
  return {
    name: name,
    defaultnic: 'e1000',
    image: 'cumulusvx-4.2',
    cpu: { cores: 2 },
    memory: { capacity: GB(4) },
  }
}

function v2v(a, ai, b, bi, props={}) {
	lnk = Link(a, ai, b, bi, props);
	lnk.v2v = true;
	return lnk;
}
