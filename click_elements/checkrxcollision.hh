#ifndef CLICK_CHECK_RXCOLLISION_HH
#define CLICK_CHECK_RXCOLLISION_HH

#include <click/element.hh>
#include <click/timestamp.hh>
#include "wireless/wirelessmodel.hh"

CLICK_DECLS

class CheckRXCollision : public Element {
  public:
    CheckRXCollision() CLICK_COLD;
    ~CheckRXCollision() CLICK_COLD;

    const char *class_name() const     	{ return "CheckRXCollision"; }
    const char *port_count() const     	{ return "1/1"; }
    const char *processing() const     	{ return AGNOSTIC; }

    int configure(Vector<String> &, ErrorHandler *) CLICK_COLD;
    int initialize(ErrorHandler*) CLICK_COLD;

    Packet* simple_action(Packet* p);

  private:
    WirelessModel& _model = WirelessModel::getInstance();
    Timestamp _collision_time;  // This is controlled by the model!
    bool _is_colliding;
    int _wireless_port;
    uint64_t _valid_nonce;
};

CLICK_ENDDECLS
#endif
