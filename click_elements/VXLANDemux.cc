/*
 * vxlandemux.{cc,hh} -- element routes packets to one output of several
 *  Based on VXLAN ID.  Strips all headers associated with the VXLAN
 *  Based on Switch element by Eddie Kohler
 *
 * Copyright (c) 2002 MIT
 * Copyright (c) 2018 USC/ISI
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, subject to the conditions
 * listed in the Click LICENSE file. These conditions include: you must
 * preserve this copyright notice, and you cannot mention the copyright
 * holders in advertising related to the Software without their permission.
 * The Software is provided WITHOUT ANY WARRANTY, EXPRESS OR IMPLIED. This
 * notice is a summary of the Click LICENSE file; the license in that file is
 * legally binding.
 */

#include <click/config.h>
#include "VXLANDemux.hh"
#include <click/args.hh>
#include <click/error.hh>
#include <click/packet_anno.hh>
#include <clicknet/ether.h>
#include <clicknet/ip.h>
#include <clicknet/udp.h>
  // NEED TO INCLUDE LINUX VXLAN HEADER

CLICK_DECLS

VXLANDemux::VXLANDemux()
{
  _ethtype_ip_nbo = htons(ETHERTYPE_IP);
  _ethtype_vlan_nbo = htons(ETHERTYPE_8021Q);
  _vxlan_port_nbo = htons(VXLAN_PORT);
}



int
VXLANDemux::configure(Vector<String> &conf, ErrorHandler *errh)
{
  Vector<int> vxlans;
  int hasVlans;
  if (Args(conf, this, errh).read_all("VXLAN", vxlans).complete() < 0)
    return -1;
  
  // Convert all ids to net and do all comparisons using net byte coding
  
  _vxlans = vxlans;
  
  uint32_t temp;
  for(int c = 0; c < _vxlans.size(); c++){
      // From linux vxlan.h, convert vxlan id to vxlan field, then run htonl
    temp = vxlan_vni_field(_vxlans[c]);
    _vxlans[c] = htonl(temp);
  } 

  for(int c = 0; c < vxlans.size(); c++) {
    click_chatter("vxlandemux: output port %d = vxlan id %d", c,  vxlans[c]);
  }

  if(vxlans.size() == noutputs())
  {
    // Too few outputs configured.  there should always be one extra output compared to vlans
    // to discard unknown packets.
    errh->error("Incorrect outputs.  %d configured when %d needed.\nShould be one more output than VXLANs for unknown packets",
                noutputs(), vxlans.size());
    return -1;
  }
  
  return 0;
}

void
VXLANDemux::push(int, Packet *p)
{
  // THIS ALL NEEDS TO CHANGE
  /* Strip MAC, IP, UDP headers leaving only VXLAN Header and Encapsulated packet.
     Retrieve VXLAN ID from VXLAN header and strip VXLAN Header
     Forward based on VXLAN ID

     We compare VXLAN IDS based on encapsulated, nbo format.

     Should we annotate the new IP header?
  */

  // Strip MAC, IP and UDP.  We'll check for VLAN TAG first.

  int ethh_length = 0;
  int iph_length = 0;
  int udph_length = sizeof(click_udp);  // UDP Headers are a fixed 8 bytes
  int vxlanh_length = sizeof(struct vxlanhdr); // VXLAN Headers are a fixed 8 bytes.
  int total_length = 0;

  const click_ether *ethh = (const click_ether*) p->data(); // Cast to an ethernet header.
  const click_ip *iph = NULL;
  const click_udp *udph = NULL;
  const struct vxlanhdr *vxlanh = NULL;

  if (ethh->ether_type == _ethtype_ip_nbo)
  {
    ethh_length = sizeof(click_ether); //Ether header length should be 14 bytes.
  }
  else if (ethh->ether_type == _ethtype_vlan_nbo)
  {
    const click_ether_vlan *eth_vlanh = (const click_ether_vlan*) p->data(); // Cast to an ethernet header with 802.1q tags

    if(eth_vlanh->ether_vlan_encap_proto != _ethtype_ip_nbo)
    {
      // Send packets out last interface.
      // click_chatter("Received non IP, VLAN encap packet");
      checked_output_push(noutputs() - 1, p);
      return;
    }
    else
    {
      ethh_length = sizeof(click_ether_vlan); //Ether header length with vlan encap should be 18 bytes.
    }
  }
  else
  {
    // click_chatter("Unknown ETHERNET packet of type %d", noths(ethh->ether_type));
    checked_output_push(noutputs() - 1, p);
    return;
  }
  
  iph = (const click_ip*) (p->data() + ethh_length);

  if (iph->ip_p != IP_PROTO_UDP)
  {
    // click_chatter("Not a UDP packet, IP PROTO %d", iph->ip_p);
    checked_output_push(noutputs() - 1, p);
    return;
  }
  else
  {
    iph_length = iph->ip_v * iph->ip_hl;
  }

  udph = (const click_udp*) (p->data() + ethh_length + iph_length);

  // Need to validate that UDP length spans vxlan header
  if (udph->uh_dport != _vxlan_port_nbo)
  {
    // click_chatter("Not a VXLAN packet, port %d", ntohs(udph->uh_dport));
    checked_output_push(noutputs() - 1, p);
    return;
  }

  vxlanh = (const struct vxlanhdr*) (p->data() + ethh_length + iph_length + udph_length);
  total_length = ethh_length + iph_length + udph_length + vxlanh_length;

  int vxlanid = vxlanh->vx_vni;
  int max_output = min(static_cast<int>(_vxlans.size()), noutputs());

  // Should probably suppress this.
  // To print out vxlanid, we need to nthol and then convert back from vxlan id field to vxlan id.
  //uint32_t temp = vxlan_vni(ntohl(vxlanid));
  //click_chatter("vxlanswitch: recv packet vxlan id %d", temp);
  
  for (int output_port = 0; output_port < max_output; ++output_port) {
    if (_vxlans[output_port] == vxlanid) {
      p->pull(total_length);
      p->set_mac_header(p->data());
      checked_output_push(output_port, p);
      return;
    }
  }
  
  // no match, send to default (last) port
  if (noutputs() > 0)
    checked_output_push(noutputs() - 1, p);
  
}

CLICK_ENDDECLS
EXPORT_ELEMENT(VXLANDemux)
ELEMENT_MT_SAFE(VXLANDemux)
