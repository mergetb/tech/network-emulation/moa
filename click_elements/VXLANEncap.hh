#ifndef CLICK_VXLAN_ENCAP_HH
#define CLICK_VXLAN_ENCAP_HH
#include <click/element.hh>
#include <clicknet/ether.h>
#include <clicknet/ip.h>
#include <clicknet/udp.h>
#include "vxlan.h"

// NEED TO INCLUDE VXLAN

CLICK_DECLS

class VXLANEncap : public Element {
  public:
    VXLANEncap() CLICK_COLD;
    ~VXLANEncap() CLICK_COLD;
    
    const char *class_name() const    { return "VXLANEncap"; }
    const char *port_count() const    { return PORTS_1_1; }
    
    int configure(Vector<String> &, ErrorHandler *) CLICK_COLD;
    bool can_live_reconfigure() const    { return true; }
    int initialize(ErrorHandler*) CLICK_COLD;
    //void add_handlers() CLICK_COLD;
    
    Packet *simple_action(Packet *) override;

  protected:
    inline void update_cksum(click_ip*, int off) const;

    
  private:
    click_ether _ethh;
    click_ip _iph;
    click_udp _udph;

    struct vxlanhdr _vxlanh;

    atomic_uint32_t _id;
};

CLICK_ENDDECLS
#endif
