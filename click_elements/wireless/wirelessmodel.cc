#include <iostream>
#include <fstream>
#include <sstream>
#include "wirelessmodel.hh"
#include <string>
#include <click/etheraddress.hh>
#include <click/string.hh>
#include <click/args.hh>
#include "randommobility.hh"

CLICK_DECLS

WirelessModel::WirelessModel()
{
  _topo = NULL;
  _is_updated = false;
  _generator = std::mt19937_64(std::random_device{}());
  _distribution =  std::uniform_int_distribution<long long int>(std::llround(std::pow(2,61)), std::llround(std::pow(2,62)));
}

WirelessModel::~WirelessModel()
{
  if(_topo != NULL)
  {
    delete _topo;
  }
}

uint64_t
WirelessModel::getRandom64()
{
  return _distribution(_generator);
}

std::set<WirelessNode*>
WirelessModel::getReachability(int port)
{
  // Should check if it exists
  return _reachability[port];
}

std::set<WirelessNode*>::iterator
WirelessModel::getSetPointer(int port)
{
  return _reachability[port].begin();
}

void
WirelessModel::update()
{
  // Tell mobility to do things
  // if updated, recalculate reachability
  if(_topo != NULL && _mobility != NULL)
  {
    std::vector<WirelessNode*>* nodes = _topo->getNodesPointer();
   
    for(int pos = 0; pos < nodes->size(); pos++)
    {
      double dx = 0.0;
      double dy = 0.0;
      WirelessNode* node = (*nodes)[pos];
      _mobility->updateNode(node, dx, dy);
      _topo->updateNode(node, dx, dy);
    }
      
    for(int pos = 0; pos < _reachability.size(); pos++)
    {
      {
        std::lock_guard<std::mutex> guard(_model_mutex);
        _reachability[pos] = _topo->getReachability(pos);
      }
    }
  }
}


void
WirelessModel::setPortAddressMappings(std::vector<EtherAddress*> map)
{
  std::lock_guard<std::mutex> guard(_model_mutex);
  for(int port = 0; port < map.size(); port++)
  {
    WirelessNode* node = getNode(map[port]);
    node->setPort(port);
  }
  _topo->sortByPorts();

  for(int pos = 0; pos < _reachability.size(); pos++)
  {
    _reachability[pos] = _topo->getReachability(pos);
  }
}


void
WirelessModel::readTopoFromFile(std::string filename)
{
  std::ifstream f_handler;
  
  std::string line;
  std::string token;
  std::vector<std::string> data;
  std::vector<WirelessNode*> nodes;

  double x_boundary;
  double y_boundary;

  if(_topo != NULL)
  {
    // Maybe log an error here?
    return;
  }
  
  f_handler.open(filename);

  // Read the first line specifying our bounds
  if(f_handler)
  {
    std::getline(f_handler, line);
    std::stringstream line_stream(line);
    while(std::getline(line_stream, token, ' '))
    {
      data.push_back(token);
    }

    if(data.size() < 2)
    {
      // This is an error
      f_handler.close();
      return;
    }
    
    x_boundary = std::stod(data[0]);
    y_boundary = std::stod(data[1]);
    _topo = new WirelessTopology(x_boundary, y_boundary);

    int count = 0;
    std::string mac, x, y, radius, tx_bw;
    while(f_handler >> mac >> x >> y >> radius >> tx_bw)
    {
      WirelessNode* node;
      EtherAddress* address;
      Timestamp ts;
      String bw(tx_bw.c_str());
      uint32_t tx_bw_in_100Bps;

      BandwidthArg().parse(bw, tx_bw_in_100Bps);
      tx_bw_in_100Bps /= 100;
      
      ts.assign_now();
      
      address = new EtherAddress();
      EtherAddressArg().parse(String(mac.c_str()), *address);

      // Give a BS port number
      node = new WirelessNode(address,
                              std::stod(x),
                              std::stod(y),
                              std::stod(radius),
                              count);

      node->setTXBandwidth(tx_bw_in_100Bps);
      nodes.push_back(node);

      // Initialize this nodes reachability
      _reachability.push_back(std::set<WirelessNode*>());
      _collisionTimes.push_back(ts);
      _collisionNonces.push_back(0);
      count++;
    }
   
    f_handler.close();
    _topo->addNodesToTopology(nodes);
    _topo->createCells();
  }
}

void
WirelessModel::configureMobility(std::string filename, std::string mob_type)
{
  auto it = mmodels.find(mob_type);
  int model_num = -1;
  if (it != mmodels.end())
  {
    model_num = it->second();
  }

  switch(model_num)
  {
  case 1:
    _mobility = new RandomMobility();
    break;
  default:
    _mobility = NULL;
  }

  if(_mobility != NULL)
  {
    _mobility->configureFromFile(filename);
  }
}

bool
WirelessModel::checkTXCollision(int port)
{
  std::lock_guard<std::mutex> guard(_model_mutex);
  return _collisionTimes[port] > Timestamp::now();
}

bool
WirelessModel::checkRXCollision(int port, uint64_t nonce)
{
  std::lock_guard<std::mutex> guard(_model_mutex);
  if((nonce != _collisionNonces[port]) &&
     (_collisionTimes[port] > Timestamp::now()))
  {
    return true;
  }
  return false;
}


void
WirelessModel::setCollision(int port, int plength, uint64_t nonce)
{ 
  WirelessNode* this_node = _topo->getNode(port);

  // Bandwidth is in Bps of intervals of 100.
  uint32_t bandwidth = this_node->getTXBandwidth();
  uint32_t delay_us = (plength * 10000) / bandwidth;
  Timestamp cwnd_us = Timestamp::make_usec(delay_us);
  
  std::lock_guard<std::mutex> guard(_model_mutex);
  for(auto const& node : _reachability[port])
  {
    int recv_port = node->getPort();
    
    Timestamp ts = _collisionTimes[recv_port];
    Timestamp now_cwnd = Timestamp::now() + cwnd_us;
    if(ts > Timestamp::now())
    {
      // Multiple packets colliding simultaneously, mark both to be dropped
      // Its possible either will have nonce 0 but unlikely (2^64)
      _collisionNonces[recv_port] = 0;
      if(now_cwnd > ts)
      {
        // Our collision will exist longer
        ts.assign_usec(now_cwnd.sec(), now_cwnd.usec());
      }
    }
    else
    {
      ts.assign_usec(now_cwnd.sec(), now_cwnd.usec());
      _collisionNonces[recv_port] = nonce;
    }
  }
}

CLICK_ENDDECLS
ELEMENT_PROVIDES(WirelessModel)
