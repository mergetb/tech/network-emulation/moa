// xdp vni test

eth1 :: XDPDevice(DEV eth1, MODE skb, PROG xdpvxlanvni, VNI 47)
eth2 :: XDPDevice(DEV eth2, MODE skb, PROG xdpvxlanvni, VNI 47)

eth1 -> Queue(10) -> eth2;
eth2 -> Queue(10) -> eth1;

