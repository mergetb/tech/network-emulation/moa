#ifndef CLICK_WIRELESS_MOBILITY_HH
#define CLICK_WIRELESS_MOBILITY_HH

#include "wirelessnode.hh"
#include "cell.hh"
#include <string>
#include <vector>
#include <unordered_map>
#include <functional>

CLICK_DECLS

const std::unordered_map<std::string, std::function<int()>> mmodels {
  {"random", [&]() { return 1; }},
};

class Mobility
{
  protected:

  public:
    Mobility() {}
    ~Mobility() {}
      
    virtual void updateNode(WirelessNode* node, double &dx, double &dy) = 0;
    virtual void configureFromFile(std::string filename) = 0;

};


CLICK_ENDDECLS
#endif
