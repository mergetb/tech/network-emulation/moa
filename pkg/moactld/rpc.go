package moactld

import (
	"context"
	"fmt"
	"io/ioutil"
	"strings"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
)

const (
	// where to find the auth token
	AuthTokenPath = "/etc/moa/token"

	// the tcp port that moactld listens on
	ControlPort = 15000
)

type moaAuth struct {
	token string
}

// implement the grpc.credentials.PERRPCCredentials interface

func (a moaAuth) GetRequestMetadata(ctx context.Context, uri ...string) (map[string]string, error) {
	return map[string]string{
		"auth-token": a.token,
	}, nil
}

func (a moaAuth) RequireTransportSecurity() bool {
	return false
}

func WithMoaControl(endpoint string, f func(MoaControlClient) error) error {
	addr := fmt.Sprintf("%s:%d", endpoint, ControlPort)

	var token string

	// load the moactld authentication token from the local fs (if it exists)
	bs, err := ioutil.ReadFile(AuthTokenPath)
	if err == nil {
		token = strings.TrimSpace(string(bs))
		log.Info("loaded auth token from /etc/moa/token")
	}

	conn, err := grpc.Dial(addr, grpc.WithInsecure(), grpc.WithPerRPCCredentials(moaAuth{token: token}))
	if err != nil {
		return fmt.Errorf("failed to connect to moactld service: %v", err)
	}
	client := NewMoaControlClient(conn)
	defer conn.Close()

	return f(client)
}
