import mergexp as mx
from mergexp.meta import kind
from mergexp.unit import gbps
from mergexp.net import capacity
from mergexp.mime import Mime, Flow
from mergexp.moa  import MultipathRouter, Route

topo = mx.Topology('mime-chubby')

a = Mime(
    name = 'a',
    mac = '00:00:00:00:00:01',
    ip = '10.0.0.1',
    flows = [Flow(
        compute = '10ms',
        data = int(1e8),
        dst = '10.0.0.2',
        mac = '00:00:00:00:00:02'
    )],
    stop = True
)
a.props['color'] = '#444'

b = Mime(
    name = 'b',
    ip = '10.0.0.2',
    mac = '00:00:00:00:00:02',
    flows = [Flow(
        compute = '50ms',
    )]
)
b.props['color'] = '#444'

leaf0 = MultipathRouter(
    name = 'leaf0',
    routes = [
        Route('10.0.0.0/24', 0),
        Route('10.0.1.0/24', 1),
        Route('10.0.1.0/24', 2),
    ]
)
leaf0.props['color'] = '#3a3'

leaf1 = MultipathRouter(
    name = 'leaf1',
    routes = [
        Route('10.0.1.0/24', 0),
        Route('10.0.0.0/24', 1),
        Route('10.0.0.0/24', 2),
    ]
)
leaf1.props['color'] = '#3a3'

spine0 = MultipathRouter(
    name = 'spine0',
    routes = [
        Route('10.0.0.0/24', 0),
        Route('10.0.1.0/24', 1),
    ]
)

spine1 = MultipathRouter(
    name = 'spine1',
    routes = [
        Route('10.0.0.0/24', 0),
        Route('10.0.1.0/24', 1),
    ]
)


topo.add_devices(a, b, leaf0, leaf1, spine0, spine1)


topo.connect([a, leaf0], capacity == gbps(1))
topo.connect([b, leaf1], capacity == gbps(1))
topo.connect([leaf0, spine0], capacity == gbps(1))
topo.connect([leaf0, spine1], capacity == gbps(1))
topo.connect([leaf1, spine0], capacity == gbps(1))
topo.connect([leaf1, spine1], capacity == gbps(1))

mx.experiment(topo)
