#ifndef CLICK_VXLANDEMUX_HH
#define CLICK_VXLANDEMUX_HH
#include <click/element.hh>
#include "vxlan.h"
CLICK_DECLS

/*
=c

VXLANDemux

=s VXLAN Ids

sends packet stream to output chosen per-packet based on VXLan ID.

=d

VXLANDemux strips all VXLAN headers (Ethernet, IP, UDP, VXLAN) leaving
only the encapsulated packet.  It then emits the packet out of its port
based on the matching VXLAN ID given in the input configuraiton string.

=a StaticSwitch, PullSwitch, RoundRobinSwitch, StrideSwitch, HashSwitch,
RandomSwitch, Paint, PaintTee */


class VXLANDemux : public Element { public:

    VXLANDemux() CLICK_COLD;

    const char *class_name() const		{ return "VXLANDemux"; }
    const char *port_count() const		{ return "1/-"; }
    const char *processing() const		{ return PUSH; }

    int configure(Vector<String> &conf, ErrorHandler *errh) CLICK_COLD;

    void push(int, Packet *);

  private:
  
    Vector<int> _vxlans;
    int _hasVlans;

    uint16_t _ethtype_ip_nbo;
    uint16_t _ethtype_vlan_nbo;
    uint16_t _vxlan_port_nbo;
    
    
};

CLICK_ENDDECLS
#endif
