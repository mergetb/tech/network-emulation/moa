package common

import "fmt"

func PrettyCapacity(v int64) string {
	if v >= 1000*1000*1000 {
		return fmt.Sprintf("%dGbps", v/(1000*1000*1000))
	}
	if v >= 1000*1000 {
		return fmt.Sprintf("%dMbps", v/(1000*1000))
	}
	if v >= 1000 {
		return fmt.Sprintf("%dKbps", v/1000)
	}
	return fmt.Sprintf("%dbps", v)

}

func PrettyLatency(v int32) string {
	if v >= 1000*1000*1000 {
		return fmt.Sprintf("%ds", v/(1000*1000*1000))
	}
	if v >= 1000*1000 {
		return fmt.Sprintf("%dms", v/(1000*1000))
	}
	if v >= 1000 {
		return fmt.Sprintf("%dus", v/1000)
	}
	return fmt.Sprintf("%dns", v)
}
