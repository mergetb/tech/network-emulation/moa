/*
 * moactld - proxy for dynamic link configuration handling
 */

package main

import (
	"context"
	"flag"
	"fmt"
	"io/ioutil"
	"net"
	"os"
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"

	"gitlab.com/mergetb/tech/network-emulation/moa/pkg/common"
	moa "gitlab.com/mergetb/tech/network-emulation/moa/pkg/manager"
	pb "gitlab.com/mergetb/tech/network-emulation/moa/pkg/moactld"
)

type moaControlServer struct {
	pb.UnimplementedMoaControlServer
	emulationHost string
	emulationPort int
	emulationId   int32
}

var (
	authToken string
)

func invalidArgument(s string) (*pb.MoaControlResponse, error) {
	return nil, status.Error(codes.InvalidArgument, s)
}

func isAuthorized(ctx context.Context) bool {
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return false
	}
	hdr, ok := md["auth-token"]
	if !ok {
		return false
	}
	return hdr[0] == authToken

}

// configuration information from cogs/rex
func (s *moaControlServer) Init(ctx context.Context, req *pb.InitRequest) (*pb.MoaControlResponse, error) {
	log.WithFields(log.Fields{"req": req}).Info("init request")

	if !isAuthorized(ctx) {
		log.Error("not authenticated")
		return &pb.MoaControlResponse{}, status.Error(codes.Unauthenticated, "not authenticated")
	}

	s.emulationHost = req.EmulationHost
	s.emulationPort = int(req.EmulationPort)
	s.emulationId = req.EmulationId
	return &pb.MoaControlResponse{}, nil
}

// user request to dynamically reconfigure Moa links
func (s *moaControlServer) Update(ctx context.Context, req *pb.UpdateRequest) (*pb.MoaControlResponse, error) {
	log.WithFields(log.Fields{"req": req}).Info("update request")

	linkArgs := &moa.LinkParams{
		Capacity:    req.Capacity,
		Latency:     req.Latency,
		Loss:        req.Loss,
		EnforceLoss: req.EnforceLoss,
	}

	addr := fmt.Sprintf("%s:%d", s.emulationHost, s.emulationPort)
	err := moa.WithMoa(addr, func(c moa.MoaClient) error {
		_, err := c.Update(context.TODO(), &moa.UpdateRequest{
			EmulationId: s.emulationId,
			Updates: []*moa.UpdateSpec{
				&moa.UpdateSpec{
					Tags:       req.Tags,
					LinkParams: linkArgs,
				},
			},
		})
		if err != nil {
			log.WithFields(log.Fields{"err": err}).Error("rpc to moa failed")
			return fmt.Errorf("update emulation rpc failed: %v", err.Error())
		}
		return nil
	})

	if err != nil {
		errorString := err.Error()
		log.WithFields(log.Fields{"err": errorString}).Error("rpc to moad failed")
		// convert normal error to grpc.status
		err = status.Error(codes.Internal, errorString)
	}
	return &pb.MoaControlResponse{}, err
}

func (s *moaControlServer) Show(ctx context.Context, req *pb.ShowRequest) (*pb.ShowResponse, error) {
	log.WithFields(log.Fields{"req": req}).Info("show request")

	links := make([]*pb.LinkInfo, 0)

	addr := fmt.Sprintf("%s:%d", s.emulationHost, s.emulationPort)
	err := moa.WithMoa(addr, func(c moa.MoaClient) error {

		showreq := moa.ShowRequest{Mzid: strconv.FormatInt(int64(s.emulationId), 10)}
		resp, err := c.Show(context.TODO(), &showreq)
		if err != nil {
			log.WithFields(log.Fields{"err": err, "req": showreq}).Error("show request to moa failed")
			return status.Error(codes.Internal, "rpc to moa failed")
		}
		for _, link := range resp.Links {
			links = append(links, &pb.LinkInfo{
				Params: &pb.LinkParams{
					Capacity: link.Params.Capacity,
					Latency:  link.Params.Latency,
					Loss:     link.Params.Loss,
				},
				Tags:     link.Tags,
				ExpNodes: link.ExpNodes,
			})
		}

		return nil
	})

	return &pb.ShowResponse{Links: links}, err
}

func loadToken() {
	bs, err := ioutil.ReadFile(pb.AuthTokenPath)
	if err != nil {
		log.Fatalf("unable to read %s: %v", pb.AuthTokenPath, err.Error())
	}
	authToken = strings.TrimSpace(string(bs))
}

type Options struct {
	version bool
	help    bool
	debug   bool
}

func init_options() *Options {
	options := Options{}

	flag.BoolVar(&options.version, "version", false, "print the version number")
	flag.BoolVar(&options.version, "v", false, "alias for -version")
	flag.BoolVar(&options.help, "help", false, "print the version number")
	flag.BoolVar(&options.help, "h", false, "alias for -help")
	flag.BoolVar(&options.debug, "debug", false, "enable verbose logging")
	flag.BoolVar(&options.debug, "d", false, "alias for -debug")
	flag.Parse()

	if options.help {
		flag.PrintDefaults()
		os.Exit(0)
	}

	return &options
}

func main() {

	options := init_options()

	if options.version {
		fmt.Printf("%s\n", common.Version)
		os.Exit(0)
	}

	if options.debug {
		log.SetLevel(log.DebugLevel)
	} else {
		log.SetLevel(log.InfoLevel)
	}

	log.Info(fmt.Sprintf("moactld %s starting up on port %d", common.Version, pb.ControlPort))
	loadToken()
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", pb.ControlPort))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	grpcServer := grpc.NewServer()
	pb.RegisterMoaControlServer(grpcServer, &moaControlServer{})
	grpcServer.Serve(lis)
}
