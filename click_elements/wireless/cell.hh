#ifndef CLICK_WIRELESS_CELL_HH
#define CLICK_WIRELESS_CELL_HH

#include <click/config.h>
#include <click/glue.hh>
#include <list>
#include <set>
#include "wirelessnode.hh"

CLICK_DECLS

/* We split the overall topology into a number of cells where cells are:
 * Cell Size = Maximum Divisor;
 * For each dimension
 */
class Cell
{
  private:
    double _x_min;
    double _x_max;
    double _y_min;
    double _y_max;
    
    int _x_pos;         // X Position within the Cell Array
    int _y_pos;         // Y Position within the Cell Array
    
    std::list<WirelessNode*> _nodes;

  public:
    Cell();
    Cell(double x_min, double x_max,
         double y_min, double y_max,
         int cell_x_pos, int cell_y_pos);
    ~Cell();

    std::set<WirelessNode*>* checkReachable(WirelessNode* node);
    void addNode(WirelessNode* node);
    void removeNode(WirelessNode* node);
    
}; 

CLICK_ENDDECLS
#endif
