#include "wirelessnode.hh"

CLICK_DECLS

WirelessNode::WirelessNode()
  :  _mac_addr(NULL), _x(0), _y(0),
     _radius(0), _in_port(-1)
{
  _radius2 = 0;
}

WirelessNode::WirelessNode(EtherAddress* addr, double x, double y,
                           double radius)
  :  WirelessNode(addr, x, y, radius, -1)
{
}

WirelessNode::WirelessNode(EtherAddress* addr, double x, double y,
                           double radius, int port)
  : _mac_addr(addr), _x(x), _y(y),
    _radius(radius), _in_port(port)
{
  _radius2 = radius * radius;
}

WirelessNode::~WirelessNode()
{
  if(_mac_addr != NULL)
  {
    delete _mac_addr;
  }
}

bool
WirelessNode::checkReachability(WirelessNode* to_check)
{
  bool to_ret = false;
  double to_check_x = 0;
  double to_check_y = 0;
  double dx = 0;          // Delta X
  double dy = 0;          // Delta Y
  double d2 = 0;          // distance squared  
  
  to_check->getPosition(to_check_x, to_check_y);

  dx = (to_check_x - _x);
  dy = (to_check_y - _y);

  d2 = dx * dx + dy * dy;

  if (d2 <= _radius2)
  {
    to_ret = true;
  }
  return to_ret;
}

CLICK_ENDDECLS
ELEMENT_PROVIDES(WirelessNode)
