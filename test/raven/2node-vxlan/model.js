/* 2 Node Moa Testing Topology
 * ===========================
 *
 *          -----
 *          | a |
 *          -----
 *            |
 *        ---------
 *        |  moa  |
 *        ---------
 *            |
 *          -----
 *          | b |
 *          -----
 *
 */

topo = {
  name: 'moa-2node-vxlan',
  nodes: [deb('a'), deb('moa'), deb('b')],
  links: [
    Link('a', 1, 'moa', 1),
    Link('b', 1, 'moa', 2),
  ],
}

function deb(name) {
  return {
    name: name,
    image: 'debian-buster',
    cpu: { cores: 2 },
    memory: { capacity: GB(4) },
    mounts: [{ source: env.PWD+"/../../..", point: '/tmp/moa'}],
  }
}

