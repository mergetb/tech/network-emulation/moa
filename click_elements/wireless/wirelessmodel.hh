#ifndef CLICK_WIRELESS_MODEL_HH
#define CLICK_WIRELESS_MODEL_HH

#include <click/config.h>
#include <click/glue.hh>
#include <click/string.hh>
#include <string>
#include <vector>
#include "topology.hh"
#include <set>
#include <mutex>
#include <random>
#include <click/timestamp.hh>
#include "mobility.hh"

CLICK_DECLS

class WirelessModel
{
  private:    
    WirelessTopology* _topo;
    Mobility* _mobility;
    
    bool _is_updated;

    // Should this be a vector rather than a set?
    // depends on if find or traversal is more important
    std::vector<std::set<WirelessNode*>> _reachability;
    std::vector<Timestamp> _collisionTimes;
    std::vector<uint64_t> _collisionNonces;

    std::mt19937_64 _generator;

    std::uniform_int_distribution<long long int> _distribution;


    
    WirelessModel();
    ~WirelessModel();

  public:
    std::mutex _model_mutex;
    
    WirelessModel(const WirelessModel&) = delete;
    WirelessModel& operator=(const WirelessModel&) = delete;

    static WirelessModel& getInstance()
    {
      static WirelessModel instance;
      return instance;
    }

    std::set<WirelessNode*> getReachability(int port);
    std::set<WirelessNode*>::iterator getSetPointer(int port);

    void readTopoFromFile(std::string filename);
    void update();

    inline WirelessNode* getNode(EtherAddress* eaddr)
    {
      return _topo->getNode(eaddr);
    }

    uint64_t getRandom64();
    void setPortAddressMappings(std::vector<EtherAddress*> map);
    void configureMobility(std::string filename, std::string mob_type);

    bool checkTXCollision(int port);
    bool checkRXCollision(int port, uint64_t nonce);
    void setCollision(int port, int plength, uint64_t nonce);
    inline Timestamp getCollisionTime(int port) { 
      return _collisionTimes[port];
    }
    inline uint64_t getCollisionNonce(int port) {
      return _collisionNonces[port];
    }
   
    
};

CLICK_ENDDECLS
#endif
