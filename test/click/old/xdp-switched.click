// test using xdp with vtep devices under evpn control

vtep47 :: XDPDevice(DEV vxlan47, MODE skb, PROG xdpallrx, TRACE true)
vtep48 :: XDPDevice(DEV vxlan48, MODE skb, PROG xdpallrx, TRACE true)

vtep47 -> Queue(10000000) -> vtep47;
vtep48 -> Queue(10000000) -> vtep48;
