module gitlab.com/mergetb/tech/network-emulation/moa

go 1.12

require (
	cloud.google.com/go/bigquery v1.4.0 // indirect
	dmitri.shuralyov.com/gpu/mtl v0.0.0-20191203043605-d42048ed14fd // indirect
	github.com/cncf/udpa/go v0.0.0-20200124205748-db4b343e48c1 // indirect
	github.com/envoyproxy/go-control-plane v0.9.4 // indirect
	github.com/ghodss/yaml v1.0.0
	github.com/golang/mock v1.4.0 // indirect
	github.com/golang/protobuf v1.3.3
	github.com/google/btree v1.0.0 // indirect
	github.com/google/pprof v0.0.0-20200212024743-f11f1df84d12 // indirect
	github.com/hashicorp/golang-lru v0.5.4 // indirect
	github.com/kr/pretty v0.2.0 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/mitchellh/mapstructure v1.1.2
	github.com/prometheus/client_model v0.2.0 // indirect
	github.com/rogpeppe/go-internal v1.5.2 // indirect
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/cobra v0.0.5
	github.com/stretchr/objx v0.2.0 // indirect
	gitlab.com/mergetb/tech/nex v0.5.4 // indirect
	gitlab.com/mergetb/tech/ns v0.0.0-20190430032646-8adac3d3e097
	gitlab.com/mergetb/tech/rtnl v0.1.9
	gitlab.com/mergetb/xir v0.1.10-0.20200223002639-eeb466dd4b7a
	go.etcd.io/bbolt v1.3.4
	golang.org/x/net v0.0.0-20200114155413-6afb5195e5aa
	google.golang.org/grpc v1.26.0
)
