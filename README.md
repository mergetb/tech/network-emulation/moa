# Moa

Moa is the Merge network emulation engine. It's based on a modified version of
[fastclick](https://github.com/mergetb/fastclick), that uses [Linux
XDP](https://www.kernel.org/doc/html/v4.18/networking/af_xdp.html) for the data
path.

Moa comes in two parts, a management daemon that that is implemented in this
repository and an [updated fastclick]((https://github.com/mergetb/fastclick) with XDP trappings.

The common data path through Moa is via VXLAN plumbing into the emulator where
packets of interest are grabbed by XDP before they enter the kernel networking
stack. Moa currently implements.

- Delay
- Loss
- Capacity constraints

