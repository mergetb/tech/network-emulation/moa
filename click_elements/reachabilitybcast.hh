#ifndef CLICK_REACHABILITYBCAST_HH
#define CLICK_REACHABILITYBCAST_HH

#include <click/element.hh>
#include <click/hashtable.hh>
#include <click/bitvector.hh>
#include <click/vector.hh>
#include <click/etheraddress.hh>
CLICK_DECLS

/*
TODO Better description!

Forward packets through outputs based on who can "hear" whom (reachable)
*/


class ReachabilityBroadcast : public Element {
  public:
    ReachabilityBroadcast() CLICK_COLD;
    ~ReachabilityBroadcast() CLICK_COLD;

    const char *class_name() const     	{ return "ReachabilityBroadcast"; }
    const char *port_count() const     	{ return "2-/="; }
    const char *processing() const     	{ return PUSH; }

    int configure(Vector<String> &, ErrorHandler *) CLICK_COLD;
    int initialize(ErrorHandler *errh);

    void add_handlers() CLICK_COLD;

    void push(int port, Packet* p);
    
  private:
    WirelessModel& _model = WirelessModel::getInstance();
}
    
CLICK_ENDDECLS
#endif
