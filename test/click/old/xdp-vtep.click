// test using xdp with vtep devices

x0 :: XDPDevice(DEV vxlan47, MODE skb, PROG xdpallrx)
x1 :: XDPDevice(DEV vxlan48, MODE skb, PROG xdpallrx)

x0 -> Queue(10) -> x1;
x1 -> Queue(10) -> x0;
