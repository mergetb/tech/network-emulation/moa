/*
 * mime.{cc,hh} -- A network workload emulator. Emulates local computation, and
 * communication with other Mimes. A set of Mimes can be used to emulate
 * distributed a distributed workload over an emulated network.
 *
 * Ryan Goodfellow
 *
 * Copyright (c) 2019 mergetb.org
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, subject to the conditions
 * listed in the Click LICENSE file. These conditions include: you must
 * preserve this copyright notice, and you cannot mention the copyright
 * holders in advertising related to the Software without their permission.
 * The Software is provided WITHOUT ANY WARRANTY, EXPRESS OR IMPLIED. This
 * notice is a summary of the Click LICENSE file; the license in that file is
 * legally binding.
 */
#include "mime.hh"
#include <click/args.hh>

CLICK_DECLS

static constexpr unsigned long long 
  DEFAULT_FLOW_SIZE{1000000000}; // 1GB

static constexpr unsigned short
  DEFAULT_MINPKT{256},
  DEFAULT_MAXPKT{1500},
  SPORT_BEGIN{1000};

const char* Mime::class_name() const { return "Mime"; }
const char* Mime::processing() const { return PULL_TO_PUSH; }
const char* Mime::port_count() const { return PORTS_1_1; }

int Mime::configure(Vector<String> &conf, ErrorHandler *errh) {

  Args args(conf, this, errh);
  args
    .read_mp("IP", _ip)
    .read_or_set("MAC", _mac, EtherAddress::make_random())
    .read_or_set("STOP", _stop, false);

  auto ret = args.consume();
  if (ret < 0) { return CONFIGURE_FAIL; }

  // parse up to  64 flows, yep this is quite crusty, but without implementing
  // more complex argument types for click, i don't see a way around it
  for(size_t i=0; i<64; i++) {

    auto f = new Flow();
    args
      .read_pi(     "COMPUTE", i, f->compute)
      .read_pi(     "DST",     i, f->dstip)
      .read_or_seti("DATA",    i, f->data,   DEFAULT_FLOW_SIZE)
      .read_or_seti("MAC",     i, f->dstmac, EtherAddress::make_random())
      .read_or_seti("MINPKT",  i, f->minpkt, DEFAULT_MINPKT)
      .read_or_seti("MAXPKT",  i, f->maxpkt, DEFAULT_MAXPKT);

    if (args.consume() < 0) { return CONFIGURE_FAIL; }
    if (f->compute == Timestamp{}) { continue; }

    _flows[i+SPORT_BEGIN] = f;

  }

  if (args.complete() < 0) { return CONFIGURE_FAIL; }

  printf("flow count %zu\n", _flows.size());

  _stop_h = new HandlerCall("stop");

  return CONFIGURE_SUCCESS;

}

int Mime::initialize(ErrorHandler *errh) {

  if (_stop_h->initialize_write(this, errh) < 0) { return -1; }

  for (auto & x : _flows) {

    unsigned short dport = (click_random() >> 2) % 0xFFF,
                   sport = x.first;

    auto &f = x.second;

    click_ether eth;
    _mac.assign(eth.ether_shost);
    f->dstmac.assign(eth.ether_dhost);

    f->flow.syn_packet = FastTCPFlows::make_packet(
        60,
        eth,
        _ip.in_addr(),
        f->dstip.in_addr(),
        sport,
        dport,
        TH_SYN
    );

    std::default_random_engine generator;
    std::uniform_int_distribution<unsigned short> dist(f->maxpkt, f->minpkt);
    f->len = dist(generator);

    f->flow.data_packet = FastTCPFlows::make_packet(
        f->len,
        eth,
        _ip.in_addr(),
        f->dstip.in_addr(),
        sport,
        dport,
        TH_PUSH | TH_ACK
    );

    f->flow.syn_packet = FastTCPFlows::make_packet(
        60, 
        eth, 
        _ip.in_addr(),
        f->dstip.in_addr(),
        sport,
        dport,
        TH_FIN
    );

    f->timer = new FlowTimer();
    f->timer->flow = f;
    f->timer->initialize(this);
    f->timer->schedule_after(f->compute);

  }



  return INITIALIZE_SUCCESS;

}

void Mime::run_timer(Timer *t) {

  response_push();
  request_push();
  do_pull();

  auto ft = static_cast<FlowTimer*>(t);

  t->reschedule_after(ft->flow->compute);

}

void Mime::request_push() {

  for (unsigned i=0; i<BURST;) {

    bool rq_finished{true};
    for (auto & x : _flows) {


      auto &f = x.second;
      if(f->data == 0) { continue; }

      if (f->txd < f->data) {
        rq_finished = false;

        //TODO only data packet for now, eventually we'll want real-ish TCP flows
        //     here
        Packet *p = f->flow.data_packet->clone()->uniqueify();
        p->timestamp_anno().assign_now();
        output(0).push(p);
        i++;
        f->tx++;
        f->txd += f->len;

      } else {
        break;
      }

    }

    if (rq_finished) { 
      _rq_finished = true; 
      break;
    }
  }


}

void Mime::response_push() {
  
  for (auto & r : _processing_requests) {

    auto *p = r.packet;
    uint8_t *data = (uint8_t*)p->data();

    //swap macs
    uint8_t tmp[6],
            *src = &data[0],
            *dst = &data[6];

    memcpy(tmp, src, 6);
    memcpy(src, dst, 6);
    memcpy(dst, tmp, 6);

    click_ip *ip = reinterpret_cast<click_ip*>(p->data()+14);
    uint32_t srcip = ip->ip_src.s_addr;
    ip->ip_src.s_addr = ip->ip_dst.s_addr;
    ip->ip_dst.s_addr = srcip;

    click_tcp *tcp = reinterpret_cast<click_tcp*>(ip+1);
    auto srcp = tcp->th_sport;
    tcp->th_sport = tcp->th_dport;
    tcp->th_dport = srcp;
    tcp->th_flags = TH_FIN;

    output(0).push(r.packet);
    rtx++;

  }
  _processing_requests.clear();

  for (auto & r : _new_requests) {

    if (!r.started) { 
      r.started = true; 
      continue;
    }

  }
  _processing_requests.insert(
      _processing_requests.end(),
      _new_requests.begin(),
      _new_requests.end()
  );
  _new_requests.clear();

}

void Mime::do_pull() {


  unsigned i = 0;
  for (Packet *p = input(0).pull(); p != nullptr; p = input(0).pull()) {

    auto *ip = reinterpret_cast<const click_ip*>(p->data()+14);
    auto *tcp = reinterpret_cast<const click_tcp*>(ip+1);

    //XXX just use PUSH and FIN for now as 'TCP-like', queue the tomatoes
    //    FIN packets increment the rx counter and get tossed
    if(tcp->th_flags == TH_FIN) {

      auto iter = _flows.find(tcp->th_dport);
      if (iter == _flows.end()) { 
        printf("flow not found");
        continue; 
      }
      iter->second->rx++;
      iter->second->rxd += ntohs(ip->ip_len);

    }


    else {

      Request rq;
      rq.packet = p->uniqueify();
      rq.started = false;
      _new_requests.push_back(rq);
      rrx++;

    }

    if(i >= BURST) {
      break;
    }
    i++;

  }



  bool finished = true;
  for(auto f : _flows) {
    printf("%s[%d] rxp/rxd: %llu/%llu\n", 
        name().c_str(), f.first, f.second->rx, f.second->rxd);

    printf("%s[%d] txp/txd: %llu/%llu\n", 
        name().c_str(), f.first, f.second->tx, f.second->txd);
    if(
        f.second->tx == 0 || 
        f.second->rx < f.second->tx || 
        f.second->txd < f.second->data
    ) {
      finished = false;
    }
  }

  if (finished) {
    _rp_finished = true;
    if(_stop) {
      _stop_h->call_write();
    }
  }

}

static String Mime_rqfinished_read_handler(Element *e, void *) {

  Mime *m = (Mime*)e;
  if (m->_rq_finished) {
    return "true";
  } else {
    return "false";
  }

}

static String Mime_rpfinished_read_handler(Element *e, void *) {

  Mime *m = (Mime*)e;
  if (m->_rp_finished) {
    return "true";
  } else {
    return "false";
  }

}

void Mime::add_handlers() {

  add_read_handler("rqfinished", Mime_rqfinished_read_handler, 0);
  add_read_handler("rpfinished", Mime_rqfinished_read_handler, 0);

}

CLICK_ENDDECLS
EXPORT_ELEMENT(Mime)
