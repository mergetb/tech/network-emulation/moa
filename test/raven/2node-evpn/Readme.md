# 2-Node EVPN Test Case

This is a simple 2 node test case for connecting two nodes through Moa using
EVPN.

![](diagram.png)

There are two nodes `a` and `b` each connected to a switch running FRR. These
nodes connect to VLAN access ports on VID `4` and `7` respectively. These ports
are attached to a VLAN-aware bridge. On the other side of the bridge are two
VTEPs `vtep4` and `vtep7`. Each of these VTEPs has VLAN tag `4` and `7` to
forward traffic from downstream nodes appropriately.

The forwarding and routing for the VTEPs themselves is managed by FRR which is
installed on the Cumulus switch. When the test case is initialized the Moa node,
which is running the GoBGP/Gobble EVPN stack, advertises that `b`'s MAC is
reachable through Moa on `VNI=4` and `a`'s MAC is reachable through MOA on `VNI=7`. 
This way when `a` want's to talk to `b`, the traffic goes through Moa.

Traffic forwarding through Moa is available by launching the
`xdp-evpn-simple.click` program, located at `/tmp/moa/test/click` on the moa
node in the test case raven topology. This program is not stated by default, so
you must start it for traffic to actually flow between `a` and `b`.

## Running

Besides launching the click instance, this entire experiment is automated. You
launch it by running `run.sh` as root. [Raven](https://gitlab.com/rygoo/raven)
must be installed.

To access nodes use the raven ssh command. For example to ssh into the moa node
use.
```shell
eval $(rvn ssh moa)
```

For details on how the experiment is setup up, read through `run.sh` and the
associated ansible scripts. The topology for the test case is defined in
`model.js`.
