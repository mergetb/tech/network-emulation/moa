package main

// implement the click control socket communicator

import (
	"fmt"
	"net/textproto"
)

type ClickSock struct {
	conn *textproto.Conn
}

func Connect(s string) (*ClickSock, error) {
	var err error
	var conn *textproto.Conn

	if conn, err = textproto.Dial("unix", s); err != nil {
		return nil, err
	}

	// read server greeting
	var buf string
	if buf, err = conn.ReadLine(); err != nil {
		conn.Close()
		return nil, err
	}

	if buf != "Click::ControlSocket/1.3" {
		conn.Close()
		return nil, fmt.Errorf("%s: unexpected Click control protocol version", buf)
	}

	return &ClickSock{conn}, nil
}

// write a line to the click control socket
func (s *ClickSock) WriteHandler(element, handler, value string) error {
	if err := s.conn.PrintfLine("WRITE %s.%s %s", element, handler, value); err != nil {
		return fmt.Errorf("failed to write command (%s)", err.Error())
	}

	if code, msg, err := s.conn.ReadResponse(200); err != nil {
		return fmt.Errorf("unexpected server response code %d (msg=%s) (err=%s)", code, msg, err.Error())
	}
	return nil
}

func (s *ClickSock) Close() {
	// attempt graceful shutdown
	s.conn.PrintfLine("QUIT")
	s.conn.Close()
}
