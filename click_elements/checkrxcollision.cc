#include <click/config.h>
#include <click/args.hh>
#include <click/glue.hh>
#include <click/error.hh>
#include <click/packet_anno.hh>
#include <click/confparse.hh>
#include "checkrxcollision.hh"

CLICK_DECLS

CheckRXCollision::CheckRXCollision()
{
  _valid_nonce = 0;
}

CheckRXCollision::~CheckRXCollision()
{
}

int
CheckRXCollision::configure(Vector<String> &conf, ErrorHandler* errh)
{
  int port;
  if(Args(conf, this, errh)
     .read_mp("PORT", port) 
     .complete() < 0)
    return errh->error("Bad Configuration");

  _wireless_port = port;
  return 0;
}

int
CheckRXCollision::initialize(ErrorHandler *errh)
{
  _collision_time = _model.getCollisionTime(_wireless_port);
  _is_colliding = false;
  return 0;
}

Packet*
CheckRXCollision::simple_action(Packet* p)
{
  uint64_t nonce = p->anno_u64(PERFCTR_ANNO_OFFSET);
 
  if(_collision_time < Timestamp::now())
  {
    if(_model.checkRXCollision(_wireless_port, nonce))
    {
      p->kill();
      _collision_time = _model.getCollisionTime(_wireless_port);
      _valid_nonce = _model.getCollisionNonce(_wireless_port);
      p = NULL;
    }
  }
  else
  {
    if(nonce != _valid_nonce)
    {
      p->kill();
      p = NULL;
    }
  }

  return p;
}

CLICK_ENDDECLS
EXPORT_ELEMENT(CheckRXCollision)
