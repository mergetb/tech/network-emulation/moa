import sys
import os
sys.path.insert(0, '../')

from MoaErrors import *
from Moa import Moa
import moa_pb2


def validateTest(expected, result, error_type, error_msg_hdr = ""):
    if(expected != result):
        raise MoaTestError(error_type, "%s %s != %s" % (error_msg_hdr, expected, result))

def main():
    link_em = Moa()

    test_simple_links(link_em)

def test_simple_links(link_em):
    test_simple_vxlan_links1(link_em)
    test_simple_native_links1(link_em)
    
def test_simple_vxlan_links1(link_em):
    link_em.loadInterfacesFromJsonFile('interface_configurations/simple_vxlan_test1.json')

    #Simple Links
    link1 = {'left_vxlan': '121', 'right_vxlan': '131',
             'left_io_point': 'eth0', 'right_io_point': 'eth0',
             'id': '1',
             'args': {'loss': '0', 'bandwidth': '10Gbps', 'delay': '0ms', 'useCodel': True}}
    link2 = {'left_vxlan': '141', 'right_vxlan': '151',
             'left_io_point': 'eth0', 'right_io_point': 'eth0',
             'id': '2',
             'args': {'loss': '0', 'bandwidth': '100Gbps', 'delay': '0ms', 'useCodel': True}}

    link_em.createEmulation("link", [link1, link2], 1)

    output_fh = open("/tmp/Emulation_1.click", "r")
    output = output_fh.read()
    output_fh.close()
    
    expected_fh = open("link_emulation_expected/simple_vxlan_test1.click", "r")
    expected = expected_fh.read()
    expected_fh.close()
    validateTest(expected, output, "Output mismatch", error_msg_hdr="MoaSimpleVXLANLinks1")

    os.remove("/tmp/Emulation_1.click")
    link_em.DiscardEmulation(moa_pb2.DiscardEmulationRequest(emulation_id = 1), None)
    
def test_simple_native_links1(link_em):
    link_em.loadInterfacesFromJsonFile('interface_configurations/simple_native_test1.json')

    #Simple Links
    link1 = {'left_vxlan': '121', 'right_vxlan': '131', 'left_io_point': 'eth0', 'right_io_point': 'eth0',
             'id': '1',
             'args': {'loss': '0', 'bandwidth': '10Gbps', 'delay': '0ms', 'useCodel': True}}
    link2 = {'left_vxlan': '141', 'right_vxlan': '151', 'left_io_point': 'eth0', 'right_io_point': 'eth0',
             'id': '2',
             'args': {'loss': '0', 'bandwidth': '100Gbps', 'delay': '0ms', 'useCodel': True}}

    link_em.createEmulation("link", [link1, link2], 1)

    output_fh = open("/tmp/Emulation_1.click", "r")
    output = output_fh.read()
    output_fh.close()
    
    expected_fh = open("link_emulation_expected/simple_native_test1.click", "r")
    expected = expected_fh.read()
    expected_fh.close()
    validateTest(expected, output, "Output mismatch", error_msg_hdr="MoaSimpleNativeLinks1")

    os.remove("/tmp/Emulation_1.click")
    link_em.DiscardEmulation(moa_pb2.DiscardEmulationRequest(emulation_id = 1), None)


    
if __name__ == "__main__":
    main()

