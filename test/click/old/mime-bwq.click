// mimes connected thru a bandwidth queue
// should take no less than 8 seconds. in reality around 9 due to how the mime
// works, e.g. it's not supposed to be a steady stream, but a stream with
// interleaving computation. In this case computation interleaving is minimal at
// 1 microsecond, but still has scheduling overhead.

a :: Mime(
  IP       10.0.0.1,
  MAC      00:00:00:00:00:01,
  COMPUTE0 1us, 
  DATA0    100000000,
  DST0     10.0.0.2,
  MAC0     00:00:00:00:00:02,
  STOP     true
);

b :: Mime(
  IP      10.0.0.2,
  MAC     00:00:00:00:00:02,

  COMPUTE0 1us
);

a_b :: BandwidthRatedUnqueue(RATE 1Gbps);
b_a :: BandwidthRatedUnqueue(RATE 1Gbps);

a -> Queue(100000) -> a_b -> Queue(100000) -> b;
b -> Queue(100000) -> b_a -> Queue(100000) -> a;

