## Link 1 Elements
link_1_left_in_queue :: ThreadSafeQueue(1000);
link_1_left_codel :: CoDel();
link_1_left_loss :: RandomSample(DROP 0.1);
link_1_left_bwdelay :: LinkUnqueue(10ms, 10Gbps);
link_1_right_in_queue :: ThreadSafeQueue(1000);
link_1_right_codel :: CoDel();
link_1_right_loss :: RandomSample(DROP 0.1);
link_1_right_bwdelay :: LinkUnqueue(10ms, 10Gbps);

## Link 2 Elements
link_2_left_in_queue :: ThreadSafeQueue(1000);
link_2_left_codel :: CoDel();
link_2_left_loss :: RandomSample(DROP 0);
link_2_left_bwdelay :: LinkUnqueue(5ms, 100Gbps);
link_2_right_in_queue :: ThreadSafeQueue(1000);
link_2_right_codel :: CoDel();
link_2_right_loss :: RandomSample(DROP 0);
link_2_right_bwdelay :: LinkUnqueue(5ms, 100Gbps);

## Link 1 Pipeline
vxlan[0] -> link_1_left_in_queue -> link_1_left_codel -> link_1_left_loss -> link_1_left_bwdelay -> vxlan_encap2;
vxlan[1] -> link_1_right_in_queue -> link_1_right_codel -> link_1_right_loss -> link_1_right_bwdelay -> vxlan_encap1;

## Link 2 Pipeline
vxlan[2] -> link_2_left_in_queue -> link_2_left_codel -> link_2_left_loss -> link_2_left_bwdelay -> vxlan_encap4;
vxlan[3] -> link_2_right_in_queue -> link_2_right_codel -> link_2_right_loss -> link_2_right_bwdelay -> vxlan_encap3;

