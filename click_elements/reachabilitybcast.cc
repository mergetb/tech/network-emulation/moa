#include <click/config.h>
#include "reachabiliybcast.hh"
#include <click/glue.hh>
#include <click/args.hh>
#include <click/straccum.hh>
#include <click/error.hh>
#include <click/confparse.hh>
#include <click/string.hh>
#include "wireless/wirelessnode.hh"
#include <vector>
#include <mutex>

CLICK_DECLS

ReachabilityBroadcast::ReachabilityBroadcast()
{
}

ReachabilityBroadcast::~ReachabilityBroadcast()
{
}

// Specify option based reachability!
int
ReachabilityBroadcast::configure(Vector<String> &conf, ErrorHandler* errh)
{
  if (Args(conf, this, errh).complete() < 0)
    return errh->error("Bad Configuration");

  // May need to map MAC -> Port somewhere

  return 0;
}

int
ReachablitySwitch::initialize(ErrorHandler *errh)
{
  return 0;
}

void
ReachabilityBroadcast::push(int port, Packet* p)
{
  set<WirelessNode*> nodes_to_forward;

  {
    std::lock_guard<std::mutex> guard(_model._model_mutex);
    nodes_to_forward = _model.getReachability(port);
  }
  
  remaining_to_forward = nodes_to_forward.size();
  
  if (remaining_to_forward < 1)
  {
    p->kill();
    return;
  }
    
  for (auto const& node : nodes_to_forward)
  {
    Packet *new_p = NULL;
    if (remaining_to_forward == 1)
    {
      new_p = p;
    }
    else
    {
      new_p = p->clone();
    }

    output(node->getPort()).push(new_p);
    remaining_to_forward--;
  }
}

CLICK_ENDDECLS
    
