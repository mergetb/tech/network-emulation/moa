#include "topology.hh"
#include "string.h"
#include <math.h>
#include <algorithm>

CLICK_DECLS

WirelessTopology::WirelessTopology()
  : _x_boundary(0), _y_boundary(0), _x_cell_size(0), _y_cell_size(0),
    _x_max_cell(0), _y_max_cell(0), _max_radius(0)
{

}

WirelessTopology::WirelessTopology(double x_boundary, double y_boundary)
  : _x_boundary(x_boundary), _y_boundary(y_boundary), _x_cell_size(0),
    _y_cell_size(0), _x_max_cell(0), _y_max_cell(0), _max_radius(0)
{

}

WirelessTopology::~WirelessTopology()
{
  for(auto& node : _nodes)
  {
    delete node;
    node = NULL;
  }

  _nodes.clear();
  deleteCells();
}

bool
WirelessTopology::createCells()
{
  if(_max_radius <= 0 || _x_boundary <= 0 || _y_boundary <= 0)
    return false;

  _x_cell_size = _max_radius;
  _y_cell_size = _max_radius;

  _x_max_cell = ceil(_x_boundary / _x_cell_size);
  _y_max_cell = ceil(_y_boundary / _y_cell_size);

  _cells.reserve(_x_max_cell * _y_max_cell);
  
  for(int x = 0; x < _x_max_cell; x++) 
  {
    for(int y = 0; y < _y_max_cell; y++)
    {
      int array_pos = x * _x_max_cell + y;
      _cells.push_back(new Cell(_x_cell_size * x, _x_cell_size * (x + 1),
                                _y_cell_size * y, _y_cell_size * (y + 1),
                                x, y));
    }
  }

  for(auto const& node_to_add : _nodes)
  {
    double x_pos = 0.0;
    double y_pos = 0.0;

    node_to_add->getPosition(x_pos, y_pos);

    Cell* the_cell = getCellFromPosition(x_pos, y_pos);
    the_cell->addNode(node_to_add);
  }
  return true;
}


void
WirelessTopology::deleteCells()
{
  for(int count = 0; count < (_x_max_cell * _y_max_cell); count++)
  {
    delete _cells[count];
  }
}

Cell*
WirelessTopology::getCellFromArrayPos(int x, int y)
{
  int actual_pos = x * _x_max_cell + y;
  if(actual_pos >= _cells.size())
    return NULL;
  return _cells[actual_pos];
}

Cell*
WirelessTopology::getCellFromPosition(double x, double y)
{
  int x_cell = floor(x / _x_cell_size);
  int y_cell = floor(y / _y_cell_size);

  return getCellFromArrayPos(x_cell, y_cell);
}

bool
WirelessTopology::addNodesToTopology(std::vector<WirelessNode*> new_nodes)
{
  _nodes.insert(std::end(_nodes), std::begin(new_nodes), std::end(new_nodes));

  _port_to_node_map.resize(_nodes.size());
  for(auto const & node_to_add : new_nodes )
  {
    double x_pos = 0.0;
    double y_pos = 0.0;

    node_to_add->getPosition(x_pos, y_pos);

    Cell* the_cell = getCellFromPosition(x_pos, y_pos);
    if(the_cell != NULL)
       the_cell->addNode(node_to_add);
    
    if(_max_radius < node_to_add->getRadius())
       _max_radius = node_to_add->getRadius();
    _mac_to_node_map[node_to_add->getMacAddress()->hashcode()] = node_to_add;
    _port_to_node_map[node_to_add->getPort()] = node_to_add;
  }

  return true;
}

std::set<WirelessNode*>
WirelessTopology::getReachability(WirelessNode* node)
{
  std::set<WirelessNode*> to_ret;
  double x_pos = 0.0;
  double y_pos = 0.0;
  int x_cell = 0;
  int y_cell = 0;

  if(node == NULL)
  {
     return to_ret;
  }
  
  node->getPosition(x_pos, y_pos);
  x_cell = floor(x_pos / _x_cell_size);
  y_cell = floor(y_pos / _y_cell_size);

  // Check the 9 cells centers on x_cell, y_cell
  for(int x_offset = -1; x_offset < 2; x_offset++)
  {
    int curr_x = x_cell + x_offset;
    if(curr_x < 0 || curr_x >= _x_max_cell)
    {
      continue;
    }
    for(int y_offset = -1; y_offset < 2; y_offset++)
    {
      int curr_y = y_cell + y_offset;
      if(curr_y < 0 || curr_y >= _y_max_cell)
      {
        continue;
      }
    
      Cell* curr_cell = getCellFromArrayPos(curr_x, curr_y);

      std::set<WirelessNode*>* ret_val = curr_cell->checkReachable(node);
      to_ret.insert(ret_val->begin(), ret_val->end());
      delete ret_val;
    }
  }
  return to_ret;
}

std::set<WirelessNode*>
WirelessTopology::getReachability(EtherAddress* address)
{
  try
  {
    WirelessNode* node = _mac_to_node_map.at(address->hashcode());
    return getReachability(node);

  }
  catch (const std::out_of_range& e)
  {
    return std::set<WirelessNode*>();
  }
}

std::set<WirelessNode*>
WirelessTopology::getReachability(int port)
{
  try
  {
    WirelessNode* node = _port_to_node_map[port];
    return getReachability(node);
  }
  catch (const std::out_of_range& e)
  {
    return std::set<WirelessNode*>();
  }
}

void
WirelessTopology::sortByPorts()
{
  std::sort(_port_to_node_map.begin(),
            _port_to_node_map.end(),
            WirelessNode::portComparison);
}

void
WirelessTopology::updateNode(WirelessNode* node, double dx, double dy)
{
  double x, y;
  Cell* old_c;
  Cell* new_c;
  
  node->getPosition(x, y);

  old_c = getCellFromPosition(x, y);
  
  x += dx;
  y += dy;
  
  if(x < 0)
  {
    x = 0;
  }
  else if(x > _x_boundary)
  {
    x = _x_boundary - 1;
  }

  if(y < 0)
  {
    y = 0;
  }
  else if(y > _y_boundary)
  {
    y = _y_boundary - 1;
  }

  node->setPosition(x, y);
  new_c = getCellFromPosition(x, y);

  if(new_c != old_c)
  {
    // May need a mutex here
    old_c->removeNode(node);
    new_c->addNode(node);
  }
}

CLICK_ENDDECLS
ELEMENT_PROVIDES(WirelessTopology)
