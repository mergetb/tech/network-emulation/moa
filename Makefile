prefix ?= /usr

VERSION = $(shell git describe --always --long --dirty --tags)
LDFLAGS = "-X gitlab.com/mergetb/tech/network-emulation/moa/pkg/common.Version=$(VERSION)"

all: build/moactl build/moactld build/moacmd build/moactld_test build/moad

build/moactl: cmd/moactl/main.go | build
	go build -ldflags=$(LDFLAGS) -o $@ $<

build/moactld: svc/moactld/main.go | build
	go build -ldflags=$(LDFLAGS) -o $@ $<

build/moacmd: cmd/moacmd/main.go | build
	go build -ldflags=$(LDFLAGS) -o $@ $<

build/moactld_test: cmd/moactld_test/main.go | build
	go build -ldflags=$(LDFLAGS) -o $@ $<

build/moad: svc/moad/main.go svc/moad/*.go | build
	go build -ldflags=$(LDFLAGS) -o $@ $^

build:
	mkdir -p build

clean:
	rm -rf build

install: build/moactl build/moacmd build/moad
	install -D build/moactl $(DESTDIR)$(prefix)/bin/moactl
	install -D build/moacmd $(DESTDIR)$(prefix)/bin/moacmd
	install -D build/moad $(DESTDIR)$(prefix)/sbin/moad
	install -D build/moactld $(DESTDIR)$(prefix)/sbin/moactld
	install -D build/moactld_test $(DESTDIR)$(prefix)/bin/moactld_test
	mkdir -p $(DESTDIR)/var/moa/emulation
	mkdir -p $(DESTDIR)/var/moa/log
	mkdir -p $(DESTDIR)/etc/moa
	dd if=/dev/urandom count=512 | sha1sum | cut -d ' ' -f 1 > $(DESTDIR)/etc/moa/token
