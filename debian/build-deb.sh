#!/bin/bash

rm -f moa_* moacmd_* moactl_* moactld_*
debuild -i -us -uc -b
mv ../../moa_* .
mv ../../moactl_* .
mv ../../moacmd_* .
mv ../../moactld_* .
