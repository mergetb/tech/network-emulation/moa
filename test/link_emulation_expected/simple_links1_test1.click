## DPDK Output for Device eth0 (PCI 0)
eth0_dpdk_out :: ToDPDKDevice(0);

## VXLAN Encap Elements for Device eth0 (PCI 0)
encap_vxlan_121 :: VXLANEncap(SRC_MAC 00:10:22:A1:B2:C3, DST_MAC 00:AA:BB:CC:11:22, SRC_IP 10.1.0.1, DST_IP 10.1.0.2, VXLAN_ID 121);
encap_vxlan_131 :: VXLANEncap(SRC_MAC 00:10:22:A1:B2:C3, DST_MAC 00:11:22:AA:BB:33, SRC_IP 10.1.0.1, DST_IP 10.1.0.3, VXLAN_ID 131);
encap_vxlan_141 :: VXLANEncap(SRC_MAC 00:10:22:A1:B2:C3, DST_MAC 00:FF:12:34:56:78, SRC_IP 10.1.0.1, DST_IP 10.1.0.4, VXLAN_ID 141);
encap_vxlan_151 :: VXLANEncap(SRC_MAC 00:10:22:A1:B2:C3, DST_MAC AA:11:22:AA:BB:33, SRC_IP 10.1.0.1, DST_IP 10.1.0.5, VXLAN_ID 151);

## ToDevice output chains with VXLAN Encapsulation
encap_vxlan_121 -> CheckIPHeader(14) -> eth0_dpdk_out;
encap_vxlan_131 -> CheckIPHeader(14) -> eth0_dpdk_out;
encap_vxlan_141 -> CheckIPHeader(14) -> eth0_dpdk_out;
encap_vxlan_151 -> CheckIPHeader(14) -> eth0_dpdk_out;

## DPDK Input for Device eth0 (PCI 0)
eth0_dpdk_in :: FromDPDKDevice(0);

## VXLAN Demux for Interface eth0 (PCI 0)
vxlan_demux_0 :: VXLANDemux(VXLAN 121, VXLAN 131, VXLAN 141, VXLAN 151);

## Classifier to seperate ARP requests for VXLAN Demux 0
vxlan_0_classifier :: Classifier(12/0806 20/0001, -);

## Interface eth0 (PCI 0) ARP Responder
arp_responder_vxlan_0 :: ARPResponder(10.1.0.0/24 00:10:22:A1:B2:C3);

## DPDK Packet Arrival Handling for Device eth0 (PCI 0)
eth0_dpdk_in -> vxlan_0_classifier;

## VXLAN Demux 0 Classifier Outputs
vxlan_0_classifier[0] -> arp_responder_vxlan_0 -> eth0_dpdk_out;
vxlan_0_classifier[1] -> vxlan_demux_0;
vxlan_0_classifier[2] -> Print("Unknown Packet") -> Discard();

## VXLAN Demux 0 error pipeline
vxlan_demux_0 -> Print("Unknown Packet from VXLAN Demux 0") -> Discard();

## Link 1 Elements
link_1_left_in_queue :: ThreadSafeQueue(1000);
link_1_left_codel :: CoDel();
link_1_left_loss :: RandomSample(DROP 0);
link_1_left_bwdelay :: LinkUnqueue(0ms, 10Gbps);
link_1_right_in_queue :: ThreadSafeQueue(1000);
link_1_right_codel :: CoDel();
link_1_right_loss :: RandomSample(DROP 0);
link_1_right_bwdelay :: LinkUnqueue(0ms, 10Gbps);

## Link 1 Elements
link_1_left_in_queue :: ThreadSafeQueue(1000);
link_1_left_codel :: CoDel();
link_1_left_loss :: RandomSample(DROP 0);
link_1_left_bwdelay :: LinkUnqueue(0ms, 100Gbps);
link_1_right_in_queue :: ThreadSafeQueue(1000);
link_1_right_codel :: CoDel();
link_1_right_loss :: RandomSample(DROP 0);
link_1_right_bwdelay :: LinkUnqueue(0ms, 100Gbps);

## Link 1 Pipeline
vxlan_demux_0[0] -> link_1_left_in_queue -> link_1_left_codel -> link_1_left_loss -> link_1_left_bwdelay -> encap_vxlan_131;
vxlan_demux_0[1] -> link_1_right_in_queue -> link_1_right_codel -> link_1_right_loss -> link_1_right_bwdelay -> encap_vxlan_121;

## Link 1 Pipeline
vxlan_demux_0[2] -> link_1_left_in_queue -> link_1_left_codel -> link_1_left_loss -> link_1_left_bwdelay -> encap_vxlan_151;
vxlan_demux_0[3] -> link_1_right_in_queue -> link_1_right_codel -> link_1_right_loss -> link_1_right_bwdelay -> encap_vxlan_141;

