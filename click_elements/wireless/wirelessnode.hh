#ifndef CLICK_WIRELESS_NODE_HH
#define CLICK_WIRELESS_NODE_HH

#include <click/config.h>
#include <click/glue.hh>
#include <click/etheraddress.hh>
#include <click/string.hh>

CLICK_DECLS

class WirelessNode
{
  private:
    EtherAddress* _mac_addr;  // MAC Address
    double _x;                // X coordinate
    double _y;                // Y coordinate
    double _radius;           // Broadcast radius
    double _radius2;          // The radius squared
    int _in_port;             // This is the inbound port for Click
    uint32_t _tx_bandwidth;   // Our transmission bandwidth in units of 100 Bps

  public:
    WirelessNode();
    ~WirelessNode();
    WirelessNode(EtherAddress* addr, double x, double y, double radius);
    WirelessNode(EtherAddress* addr, double x, double y, double radius, int port);

    inline int getPort();
    inline EtherAddress* getMacAddress();
    inline void getPosition(double &x, double &y);
    inline double getRadius();
    inline uint32_t getTXBandwidth();
    
    inline void setPort(int port);
    inline void setPosition(double x, double y);
    inline void move(double dx, double dy);
    inline void setRadius(double radius);
    inline void setTXBandwidth(uint32_t bw);
    
    bool checkReachability(WirelessNode* to_check);

    static inline bool portComparison(WirelessNode* a, WirelessNode* b);
};


inline int
WirelessNode::getPort()
{
  return _in_port;
}

inline EtherAddress*
WirelessNode::getMacAddress()
{
  return _mac_addr;
}

inline void
WirelessNode::getPosition(double &x, double &y)
{
  x = _x;
  y = _y;
}

inline double
WirelessNode::getRadius()
{
  return _radius;
}

inline uint32_t
WirelessNode::getTXBandwidth()
{
  return _tx_bandwidth;
}

inline void
WirelessNode::setPort(int port)
{
  _in_port = port;
}

inline void
WirelessNode::setPosition(double x, double y)
{
  _x = x;
  _y = y;
}

inline void
WirelessNode::move(double dx, double dy)
{
  _x += dx;
  _y += dy;
}

inline void
WirelessNode::setRadius(double radius)
{
  _radius = radius;
  _radius2 = radius * radius;
}

inline void
WirelessNode::setTXBandwidth(uint32_t bw)
{
  _tx_bandwidth = bw;
}


inline bool
WirelessNode::portComparison(WirelessNode* a, WirelessNode* b)
{
  return (a->getPort() < b->getPort());
}


CLICK_ENDDECLS
#endif
