#!/usr/bin/python3

import json
#sys.path.insert(0, '../../')

#from moa.errors import *
#from moa import Moa
import grpc
from moa.proto import moa_pb2
from moa.proto import moa_pb2_grpc

interface_blob = json.dumps({
    "vtep100": {
        "type": "xdpinterface",
        "id": "vtep100",
        "mode": "skb",
        "ip": "",
        "prefix": "",
        "mac": "",
    },
    "vtep101": {
        "type": "xdpinterface",
        "id": "vtep101",
        "mode": "skb",
        "ip": "",
        "prefix": "",
        "mac": "",
    }
})

link_blob = json.dumps([{
    "id": "dl100_101",
    "type": "link",
    "left_to_right": {
        "id": "left100_101",
        "type": "unilink",
        "inputs": [
            "vtep100"
        ],
        "output": "vtep101",
        "args": {
            "bandwidth": "10000000bps",
            "delay": "10000000ns",
            "loss": ""
        }
    },
    "right_to_left": {
        "id": "right100_101",
        "type": "unilink",
        "inputs": [
            "vtep101"
        ],
        "output": "vtep100",
        "args": {
            "bandwidth": "10000000bps",
            "delay": "10000000ns",
            "loss": ""
        }
    }
}])


with grpc.insecure_channel('127.0.0.1:50052') as channel:

    stub = moa_pb2_grpc.MoaStub(channel)

    try:
        resp = stub.update_interfaces(moa_pb2.InterfaceUpdateRequest(
            interface_blob=interface_blob,
        ))
        print("update interfaces ok")
        print(resp)

        resp = stub.new_emulation(moa_pb2.NewEmulationRequest(
            emulation_id=1,
            emulator='click',
            emulation_blob=link_blob,
        ))
        print("new emulation ok")
        print(resp)

        resp = stub.finalize_emulation(moa_pb2.FinalizeEmulationRequest(
            emulation_id=1,
        ))
        print("finalize emulation ok")
        print(resp)

    except grpc.RpcError as e:
        print("grpc error")
        raise e

    except Exception as e:
        raise e
