#ifndef CLICK_WIRELESS_TOPOLOGY_HH
#define CLICK_WIRELESS_TOPOLOGY_HH

#include "cell.hh"
#include "wirelessnode.hh"
#include <click/etheraddress.hh>
#include <set>
#include <map>
#include <vector>
CLICK_DECLS


class WirelessTopology
{
  private: 
    double _x_boundary;        // X Boundary of our 2d topology
                               // (0 <= x <= _x_boundary)
    double _y_boundary;        // Y Boundary of our 2d topology
                               // (0 <= y <= _y_boundary)

    double _x_cell_size;       // Extent of size in X
    double _y_cell_size;       // Extent of size in Y

    int _x_max_cell;           // X maximum for our 2d array
    int _y_max_cell;           // Y maximum for our 2d array
    int _cell_array_size;      // Cell array size (_x_max_cell * _y_max_cell)

    int _total_nodes;          // Total wireless nodes 
    
    std::vector<WirelessNode*> _nodes;     // Our array of wireless nodes
    std::vector<Cell*> _cells;             // Our 2D array of Cells
                                           // (This is special, more later)

    std::map<size_t, WirelessNode*> _mac_to_node_map; // A Map of MAC to node,
                                                      // where MACs are hashes
                                                      // via EtherAddress::hashcode()
    std::vector<WirelessNode*> _port_to_node_map;   // A map of port to node

    double _max_radius;        // Maximum radius of a node
    
    void deleteCells();

    std::set<WirelessNode*> getReachability(WirelessNode* node);
  public:
    WirelessTopology();
    WirelessTopology(double x_boundary, double y_boundary);
    ~WirelessTopology();

    bool createCells();
    bool addNodesToTopology(std::vector<WirelessNode*> nodes);

    Cell* getCellFromArrayPos(int x, int y);
    Cell* getCellFromPosition(double x, double y);
    
    std::set<WirelessNode*> getReachability(EtherAddress* address);
    std::set<WirelessNode*> getReachability(int port);

    inline WirelessNode* getNode(EtherAddress* address)
    {
      return _mac_to_node_map[address->hashcode()];
    }
    
    inline WirelessNode* getNode(int port)
    {
      return _port_to_node_map[port];
    }

    inline std::vector<WirelessNode*>* getNodesPointer()
    {
      return &(_nodes);
    }
    
    void sortByPorts();
    void updateNode(WirelessNode* node, double dx, double dy);

};
    
CLICK_ENDDECLS
#endif
