#include <click/config.h>
#include <click/args.hh>
#include <click/glue.hh>
#include <click/error.hh>
#include <click/packet_anno.hh>
#include <click/confparse.hh>
#include "simpleCSMA.hh"

CLICK_DECLS

SimpleCSMA::SimpleCSMA()
  :  _timer(this)
{
}

SimpleCSMA::~SimpleCSMA()
{
}

int
SimpleCSMA::configure(Vector<String> &conf, ErrorHandler* errh)
{
  int port;
  if(Args(conf, this, errh)
     .read_mp("PORT", port) 
     .complete() < 0)
    return errh->error("Bad Configuration");

  _wireless_port = port;
  return 0;
}

int
SimpleCSMA::initialize(ErrorHandler *errh)
{
  _is_colliding = false;
  _notifier.initialize(Notifier::EMPTY_NOTIFIER, router());
  _upstream_signal = Notifier::upstream_empty_signal(this, 0, &_notifier);
  _notifier.set_active(true, false);
  _timer.initialize(this);
  _our_nonce = _model.getRandom64();
  _our_p = NULL;
  return 0;
}

Packet*
SimpleCSMA::pull(int port)
{
  Packet* p = NULL;
  if(_our_p != NULL)
  {
    p = _our_p;
  }
  else
  {
    p = input(port).pull();
  }
 
  if(p)
  {
    if(!p->timestamp_anno().sec())
    {
      p->timestamp_anno().assign_now();
    }

    Timestamp now = Timestamp::now();
    if(p->timestamp_anno() <= now)
    {
      if(_model.checkTXCollision(_wireless_port))
      {
        p->timestamp_anno() = _model.getCollisionTime(_wireless_port);
        _our_p = p;
        Timestamp expiry = p->timestamp_anno() - Timer::adjustment();
        if(expiry <= now)
        {
          _notifier.wake();
        }
        else
        {
          _timer.schedule_at(expiry);
        }
        return NULL;
      }
      else
      {
        p->timestamp_anno() = now;
        p->set_anno_u64(PERFCTR_ANNO_OFFSET, _our_nonce);
        _model.setCollision(_wireless_port, p->length(), _our_nonce);
        _our_nonce++;
        return p;
      }
    }
    else
    {
      Timestamp expiry = p->timestamp_anno() - Timer::adjustment();
      if(expiry <= now)
      {
        _notifier.wake();
      }
      else
      {
        _timer.schedule_at(expiry);
      }
      return NULL;
    }
  }
  else if (!_upstream_signal)
  {
    _notifier.sleep();
  }

  return NULL;
}

void
SimpleCSMA::run_timer(Timer* t)
{
  _notifier.wake();
}

CLICK_ENDDECLS
EXPORT_ELEMENT(SimpleCSMA)
