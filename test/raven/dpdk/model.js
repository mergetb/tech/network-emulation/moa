let name = "moa-"+Math.random().toString().substr(-6)
topo = {
  name: name,
  nodes: [deb('a', 2, 2), deb('b', 2, 2), deb('moa', 8, 24)],
  links: [
    Link('a', 0, 'moa', 0),
    Link('b', 0, 'moa', 1)
  ],
}

function deb(name, cores, mem) {
  return {
    name: name,
    image: 'debian-buster',
    machine: "q35",
    cpu: { cores: cores, passthru: true },
    memory: { capacity: GB(mem) },
    mounts: [{ source: env.PWD+'/../../', point: '/tmp/moa' }],
    //defaultnic: 'e1000',
  }
}
