#!/bin/bash

set -x

ip link del vgreen
ip link del vblue
ip netns del green
ip netns del blue
