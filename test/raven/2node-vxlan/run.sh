#!/bin/bash

RED="\e[31m"
BLUE="\e[34m"
GREEN="\e[32m"
CYAN="\e[36m"
NORMAL="\e[39m"
BLINK="\e[5m"
RESET="\e[0m"

blue() { 
  echo -e "$BLUE$1$NORMAL" 
}
green() { 
  echo -e "$GREEN$1$NORMAL" 
}
cyan() { 
  echo -e "$CYAN$1$NORMAL" 
}
stage() {
  echo -e "$BLUE$1$BLINK 🔨$RESET"
}

blue "clearing out any existing environments"
rvn destroy

blue "building testing environment"
rvn build

blue "deploying testing environment"
rvn deploy

blue "waiting on nodes"
rvn pingwait a b moa

blue "running base configuration"
rvn configure

blue "checking topology status"
rvn status

blue "configuring emulator node"
ansible-playbook -i .rvn/ansible-hosts moa-setup.yml

blue "configuring edge nodes"
ansible-playbook -i .rvn/ansible-hosts node-setup.yml

green "finished"
