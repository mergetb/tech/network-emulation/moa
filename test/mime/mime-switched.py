import mergexp as mx
from mergexp.meta import kind
from mergexp.unit import gbps
from mergexp.net import capacity
from mergexp.mime import (Mime, Flow)
from mergexp.moa  import switch

topo = mx.Topology('mime-switched')

a = Mime(
    name = 'a',
    mac = '00:00:00:00:00:01',
    ip = '10.0.0.1',
    flows = [Flow(
        compute = '10ms',
        data = int(1e8),
        dst = '10.0.0.2',
        mac = '00:00:00:00:00:02'
    )],
    stop = True
)

b = Mime(
    name = 'b',
    ip = '10.0.0.2',
    mac = '00:00:00:00:00:02',
    flows = [Flow(
        compute = '50ms',
    )]
)

topo.add_devices(a, b)

switch = topo.device('switch', kind == switch)

topo.connect([a, switch], capacity == gbps(1))
topo.connect([b, switch], capacity == gbps(1))

mx.experiment(topo)
