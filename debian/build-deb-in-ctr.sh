#!/bin/bash

moadir=$(realpath `pwd`/..)

set -e

docker build ${BUILD_ARGS} -f builder.dock -t moa-builder .
docker run -v $moadir:/moa moa-builder ./build-deb.sh
