#!/bin/bash

canopy abort

canopy set port master bridge swp2,swp3 sw
canopy set port access 4 swp2 sw
canopy set port access 7 swp3 sw
canopy set port mtu 9216 swp1,swp2,swp3 sw
canopy set vtep present 4 10.99.0.2 vtep4 sw
canopy set vtep present 7 10.99.0.2 vtep7 sw
canopy set port master bridge vtep4,vtep7 sw
canopy set port access 4 vtep4 sw
canopy set port access 7 vtep7 sw
canopy set port mtu 9166 vtep4,vtep7 sw
canopy set port link up vtep4,vtep7 sw

canopy set vtep present 4 10.99.0.1 vtep4 moa -p eth1
canopy set vtep present 7 10.99.0.1 vtep7 moa -p eth1
canopy set port link up vtep4,vtep7 moa
canopy set port mtu 9216 eth1 moa
canopy set port mtu 9166 vtep4,vtep7 moa

canopy commit
