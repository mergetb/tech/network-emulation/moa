#pragma once
/*
=c

Mime([COMPUTE, IP, I<keywords>])

=s basicsources

A workload emulator.

=d

A Mime is an element that emulates local computation and communication with
other Mimes. A set of Mimes can be used to emulate a distributed workload over
an emulated network. Mimes can have multiple flows. A flow is an interleaving of
communication and computation. A multiflow mime is in essence, a multithread
machine.

Keyword arguments are:

=over 4

=item IP

The ip address of the Mime.

=item MAC

Mac address. If not specified one is randomly generated.

=item STOP

Stop the emulation when the mime has exhausted it's data to send.

=item COMPUTE[i]

The ammount of time spent processing each message on flow i.

=item DATA[i]

The amount of data to generate for flow i. Only used in client mode.
Defaults to 1GB.

=item DST[i]

Destination IP address for flow i.

=item MAC[i]

Destination Mac address for flow i.

=item MAXPKT[i]

The maximum packet size for flow i. Default is 1500. Actual packet size 
determined by normal distribution between MAXPKT and MINPKT.

=item MINPKT[i].

The minimum packet size for flow i. Default is 256. Actual packet size 
determined by normal distribution between MAXPKT and MINPKT.


=back

=e

  a :: Mime(
    IP       10.0.0.1,
    MAC      00:00:00:00:00:01,
    COMPUTE0 10ms, 
    DATA0    100000000,
    DST0     10.0.0.2,
    MAC0     00:00:00:00:00:02,
    STOP     true
  );

  b :: Mime(
    IP      10.0.0.2,
    MAC     00:00:00:00:00:02,

    COMPUTE0 50ms
  );

  a -> Queue(100000) -> b;
  b -> Queue(100000) -> a;

=n

Useful for creating full system emulations, both compute and communication.
This is a push/pull element. Egress packets are pulled from Mime elements and
ingress packets are pushed into mimes.
*/

#include <map>
#include <click/config.h>
#include <click/element.hh>
#include <click/etheraddress.hh>
#include <click/ipaddress.hh>
#include "../tcpudp/fasttcpflows.hh"
CLICK_DECLS

static constexpr unsigned  BURST{256};

class FlowTimer;

struct Flow {
  IPAddress dstip{};
  EtherAddress dstmac{}; //TODO XXX drop in favor of ARP
  unsigned long long data{0};
  flow_t flow{};
  unsigned short len{};

  // packet size range
  unsigned short maxpkt{}, 
                 minpkt{};


  // packet counters
  unsigned long long tx{},
                     rx{};

  // data counters
  unsigned long long txd{},
                     rxd{};

  // computation overhead of the flow
  Timestamp compute{};

  // timer based on @compute for this flow
  FlowTimer *timer{nullptr};

};

class FlowTimer : public Timer {
  public:
    Flow *flow;
};


struct Request {
  WritablePacket *packet;
  bool started;
};

class Mime : public Element {

  public:

    enum class Mode { client, server };

    // lifetime management
    Mime() CLICK_COLD = default;
    ~Mime() CLICK_COLD = default;
    int configure(Vector<String>&, ErrorHandler*) override final CLICK_COLD;
    int initialize(ErrorHandler*) override final CLICK_COLD;

    // packet processing
    void run_timer(Timer *t)          override final;

    void request_push();
    void response_push();
    void do_pull();

    // metadata
    const char* class_name() const override final;
    const char* processing() const override final;
    const char* port_count() const override final;

    // handlers
    void add_handlers() override final;

    // handler data
    bool _rq_finished{false}, _rp_finished{false};

  private:
  
    unsigned long long rtx{}, rrx{};

    // packet processing
    Packet* pull_client(int port);
    Packet* pull_server(int port);

    /*
     * Handlers
     */

    HandlerCall *_stop_h{};         

    /*
     * Data members
     */

    // ip address
    IPAddress _ip{};

    // mac address
    EtherAddress _mac{};

    // operating mode
    Mode _mode{Mode::server};

    // flows keyed on destination address (client mode)
    std::map<uint32_t,Flow*> _flows{};

    // buckets of requests to process (server mode)
    std::vector<Request> _new_requests{};
    std::vector<Request> _processing_requests{};

    // stop emulation when complete
    bool _stop{};


};

CLICK_ENDDECLS
