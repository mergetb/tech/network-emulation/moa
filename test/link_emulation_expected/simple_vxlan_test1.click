## DPDK Output for Device eth0 (PCI 0)
eth0_dpdk_out :: ToDPDKDevice(0);

## DPDK Input for Device eth0 (PCI 0)
eth0_dpdk_in :: FromDPDKDevice(0);

## Link 1 Elements
link_1_left_in_queue :: ThreadSafeQueue(1000);
link_1_left_codel :: CoDel();
link_1_left_loss :: RandomSample(DROP 0);
link_1_left_bwdelay :: LinkUnqueue(0ms, 10Gbps);
link_1_right_in_queue :: ThreadSafeQueue(1000);
link_1_right_codel :: CoDel();
link_1_right_loss :: RandomSample(DROP 0);
link_1_right_bwdelay :: LinkUnqueue(0ms, 10Gbps);

## Link 2 Elements
link_2_left_in_queue :: ThreadSafeQueue(1000);
link_2_left_codel :: CoDel();
link_2_left_loss :: RandomSample(DROP 0);
link_2_left_bwdelay :: LinkUnqueue(0ms, 100Gbps);
link_2_right_in_queue :: ThreadSafeQueue(1000);
link_2_right_codel :: CoDel();
link_2_right_loss :: RandomSample(DROP 0);
link_2_right_bwdelay :: LinkUnqueue(0ms, 100Gbps);

## Link 1 Pipeline
eth0_dpdk_in -> link_1_left_in_queue -> link_1_left_codel -> link_1_left_loss -> link_1_left_bwdelay -> eth0_dpdk_out;
eth0_dpdk_in -> link_1_right_in_queue -> link_1_right_codel -> link_1_right_loss -> link_1_right_bwdelay -> eth0_dpdk_out;

## Link 2 Pipeline
eth0_dpdk_in -> link_2_left_in_queue -> link_2_left_codel -> link_2_left_loss -> link_2_left_bwdelay -> eth0_dpdk_out;
eth0_dpdk_in -> link_2_right_in_queue -> link_2_right_codel -> link_2_right_loss -> link_2_right_bwdelay -> eth0_dpdk_out;

