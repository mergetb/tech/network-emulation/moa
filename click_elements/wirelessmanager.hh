#ifndef CLICK_WIRELESS_MANAGER_HH
#define CLICK_WIRELESS_MANAGER_HH

#include <click/config.h>
#include <click/element.hh>
#include <click/glue.hh>
#include <click/timer.hh>
#include "wireless/wirelessmodel.hh"

CLICK_DECLS

class WirelessManager : public Element
{
  public:
    WirelessManager() CLICK_COLD;
    ~WirelessManager() CLICK_COLD;

    const char *class_name() const { return "WirelessManager"; }
    const char *port_count() const { return PORTS_0_0; }

    int configure(Vector<String> &, ErrorHandler*) CLICK_COLD;
    int initialize(ErrorHandler*) CLICK_COLD;

    void run_timer(Timer*);


  private:
    //
    WirelessModel& _model = WirelessModel::getInstance();
    int _interval;
    bool _mobility_enabled;

    Timer _timer;
};

CLICK_ENDDECLS
#endif
