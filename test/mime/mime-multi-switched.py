import mergexp as mx
from mergexp.meta import kind
from mergexp.unit import gbps
from mergexp.net import capacity
from mergexp.mime import (Mime, Flow)
from mergexp.moa  import switch

topo = mx.Topology('mime-multi-switched')

a = Mime(
    name = 'a',
    mac = '00:00:00:00:00:01',
    ip = '10.0.0.1',
    flows = [Flow(
        compute = '10ms',
        data = int(1e8),
        dst = '10.0.0.2',
        mac = '00:00:00:00:00:02'
    )],
    stop = True
)

b = Mime(
    name = 'b',
    ip = '10.0.0.2',
    mac = '00:00:00:00:00:02',
    flows = [Flow(
        compute = '50ms',
    )]
)

topo.add_devices(a, b)

spine = topo.device('spine', kind == switch)
leaf  = [topo.device('leaf%d'%i,  kind == switch) for i in range(2)]

topo.connect([leaf[0], spine], capacity == gbps(1))
topo.connect([leaf[1], spine], capacity == gbps(1))
topo.connect([a, leaf[0]], capacity == gbps(1))
topo.connect([b, leaf[1]], capacity == gbps(1))

mx.experiment(topo)
