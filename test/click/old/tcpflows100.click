// instrumentation

define($N 1000000, 
       $L 1000000)

// a ~~~~~

a :: FastTCPFlows(
  RATE     $N,
  LIMIT    $L,
  LENGTH   9000,
  SRCETH   00:00:00:00:00:01,
  SRCIP    10.0.0.1,
  DSTETH   00:00:00:00:00:02,
  DSTIP    10.0.0.2,
  FLOWS    1,
  FLOWSIZE $L,
  STOP true,
);

// b ~~~~~

b :: TCPReflector();

c :: Counter()

// connection ~~~~~~~

//a_b :: LinkUnqueue(0us, 10Gbps);
//b_a :: LinkUnqueue(0us, 10Gbps);
a_b :: BandwidthRatedUnqueue(RATE 10Gbps)
b_a :: BandwidthRatedUnqueue(RATE 10Gbps)

a -> a_b -> c -> Strip(14) -> b; 
b -> Queue(10000) -> b_a -> Discard();

