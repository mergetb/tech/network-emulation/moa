#!/bin/bash

sudo apt update -y

sudo apt install -y \
  libnuma-dev \
  libibverbs-dev \
  rdma-core \
  lshw \
  pciutils \
  zlib1g-dev \
  graphviz \
  libgraphviz-dev \
  python-pip

