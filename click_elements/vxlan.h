/* SPDX-License-Identifier: GPL-2.0 */
/* Pushing this license until we have a better system */

#ifndef CLICK_VXLAN_H
#define CLICK_VXLAN_H

#include <linux/types.h>
#include <asm/byteorder.h>

#ifdef __CHECKER__
#define __force__ __attribute__((force))
#else
#define __force__
#endif
#define __force __force__

#define VXLAN_PORT 4789
#define BIT(nr)			(1UL << (nr))

/*
 * VXLAN header definition
 *  Based off / taken from Linux's include/net/vxlan.h
 *

 * Relevant RFCS:
 *   RFC 7348 Virtual eXtensible Local Area Network (VXLAN)
 */

/* VXLAN protocol (RFC 7348) header:
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |R|R|R|R|I|R|R|R|               Reserved                        |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |                VXLAN Network Identifier (VNI) |   Reserved    |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *
 * I = VXLAN Network Identifier (VNI) present.
 */
struct vxlanhdr {
	__be32 vx_flags;
	__be32 vx_vni;
};

/* VXLAN header flags. */
#define VXLAN_HF_VNI	__cpu_to_be32(BIT(27))

#define VXLAN_N_VID     (1u << 24)
#define VXLAN_VID_MASK  (VXLAN_N_VID - 1)

static inline __be32 vxlan_vni(__be32 vni_field)
{
#if defined(__BIG_ENDIAN)
	return (__force __be32)((__force __u32)vni_field >> 8);
#else
	return (__force __be32)((__force __u32)(vni_field & VXLAN_VNI_MASK) << 8);
#endif
}

static inline __be32 vxlan_vni_field(__be32 vni)
{
#if defined(__BIG_ENDIAN)
	return (__force __be32)((__force __u32)vni << 8);
#else
	return (__force __be32)((__force __u32)vni >> 8);
#endif
}

#endif
