#ifndef CLICK_REACHABILITYSWITCH_HH
#define CLICK_REACHABILITYSWITCH_HH

#include <click/element.hh>
#include <click/hashtable.hh>
#include <click/bitvector.hh>
#include <click/vector.hh>
#include <vector>
#include <map>
#include "wireless/wirelessmodel.hh"
CLICK_DECLS

/*
TODO Better description!

Forward packets through outputs based on who can "hear" whom (reachable)
*/

class ReachabilitySwitch : public Element {
  public:
    ReachabilitySwitch() CLICK_COLD;
    ~ReachabilitySwitch() CLICK_COLD;

    const char *class_name() const     	{ return "ReachabilitySwitch"; }
    const char *port_count() const     	{ return "2-/="; }
    const char *processing() const     	{ return PUSH; }

    int configure(Vector<String> &, ErrorHandler *) CLICK_COLD;
    int initialize(ErrorHandler *) CLICK_COLD;
    //void add_handlers() CLICK_COLD;

    void push(int port, Packet* p);

    
  private:
    std::vector<std::set<WirelessNode*>> _forwarding_table;
    std::vector<std::set<WirelessNode*>::iterator> _pointer_status;

    std::vector<EtherAddress*> _port_to_address_map;
    std::map<EtherAddress*, int> _address_to_port_map;
    WirelessModel& _model = WirelessModel::getInstance();

    void broadcast(int port, Packet *p);
   
};
    
CLICK_ENDDECLS
#endif
