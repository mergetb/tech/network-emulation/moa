#ifndef CLICK_REMOVE_DUPLICATES_HH
#define CLICK_REMOVE_DUPLICATES_HH

#include <click/config.h>
#include <click/element.hh>
#include <click/glue.hh>
#include <list>
#include <string>

CLICK_DECLS

class RemoveDuplicates : public Element {
  public:
    RemoveDuplicates() CLICK_COLD;
    ~RemoveDuplicates() CLICK_COLD;

    const char *class_name() const     	{ return "RemoveDuplicates"; }
    const char *port_count() const     	{ return "1/1"; }
    const char *processing() const     	{ return AGNOSTIC; }
    
    int configure(Vector<String> &, ErrorHandler *) CLICK_COLD;
    int initialize(ErrorHandler*) CLICK_COLD;
    
    Packet* simple_action(Packet *p);
    
  private:
    std::list<std::string> _history;
    int _history_length;

    int _duplicate_count;
    short _ethtype_ip_nbo;
    short _ethtype_vlan_nbo;


};

CLICK_ENDDECLS
#endif
