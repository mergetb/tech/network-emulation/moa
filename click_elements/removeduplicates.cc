#include "removeduplicates.hh"
#include <click/args.hh>
#include <click/error.hh>
#include <clicknet/ip.h>
#include <clicknet/ether.h>

CLICK_DECLS

RemoveDuplicates::RemoveDuplicates()
{
}

RemoveDuplicates::~RemoveDuplicates()
{
}

int
RemoveDuplicates::configure(Vector<String> &conf, ErrorHandler *errh)
{
  int length = 16;
  if (Args(conf, this, errh)
      .read("LENGTH", length)
      .complete() < 0)
    return errh->error("Bad Configuration");

  _history_length = length;
  return 0;
}

int
RemoveDuplicates::initialize(ErrorHandler *)
{
  _duplicate_count = 0;
  _ethtype_ip_nbo = htons(ETHERTYPE_IP);
  _ethtype_vlan_nbo = htons(ETHERTYPE_8021Q);
  return 0;
}



Packet*
RemoveDuplicates::simple_action(Packet *p)
{
  int ethh_length = 0;
  std::string p_ident;

  const click_ether *ethh = (const click_ether*) p->data(); // Cast to an ethernet header.
  const click_ip *iph = NULL;

  if (ethh->ether_type == _ethtype_ip_nbo)
  {
    ethh_length = sizeof(click_ether); //Ether header length should be 14 bytes.
  }
  else if (ethh->ether_type == _ethtype_vlan_nbo)
  {
    const click_ether_vlan *eth_vlanh = (const click_ether_vlan*) p->data(); // Cast to an ethernet header with 802.1q tags
      
    if(eth_vlanh->ether_vlan_encap_proto != _ethtype_ip_nbo)
    {
      return p;
    }
    ethh_length = sizeof(click_ether_vlan);
  }
  else
  {
    return p;
  }

  iph = (const click_ip*) (p->data() + ethh_length);

  p_ident = std::to_string(iph->ip_src.s_addr) + std::to_string(iph->ip_dst.s_addr) + std::to_string(iph->ip_id) + std::to_string(iph->ip_ttl);

  for (auto ident : _history)
  {
    if (ident == p_ident)
    {
      p->kill();
      return NULL;
    }
  }

  _history.push_front(p_ident);

  if(_history.size() > _history_length)
  {
    _history.pop_back();
  }
  return p;
}

      
CLICK_ENDDECLS
EXPORT_ELEMENT(RemoveDuplicates)
