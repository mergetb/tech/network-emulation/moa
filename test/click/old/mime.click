// basic mime test

a :: Mime(
  IP       10.0.0.1,
  MAC      00:00:00:00:00:01,
  COMPUTE0 10ms, 
  DATA0    100000000,
  DST0     10.0.0.2,
  MAC0     00:00:00:00:00:02,
  STOP     true
);

b :: Mime(
  IP      10.0.0.2,
  MAC     00:00:00:00:00:02,

  COMPUTE0 50ms
);

a -> Queue(100000) -> b;
b -> Queue(100000) -> a;
