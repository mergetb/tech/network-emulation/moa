## DPDK Output for Device eth1 (PCI 1)
eth1_dpdk_out :: ToDPDKDevice(1);

## VXLAN Encap Elements for Device eth1 (PCI 1)
encap_vxlan_221 :: VXLANEncap(SRC_MAC 20:10:22:A1:B2:C3, DST_MAC 20:AA:BB:CC:11:22, SRC_IP 10.2.0.1, DST_IP 10.2.0.2, VXLAN_ID 221);
encap_vxlan_231 :: VXLANEncap(SRC_MAC 20:10:22:A1:B2:C3, DST_MAC 20:11:22:AA:BB:33, SRC_IP 10.2.0.1, DST_IP 10.2.0.3, VXLAN_ID 231);

## ToDevice output chains with VXLAN Encapsulation
encap_vxlan_221 -> CheckIPHeader(14) -> eth1_dpdk_out;
encap_vxlan_231 -> CheckIPHeader(14) -> eth1_dpdk_out;

## DPDK Output for Device eth0 (PCI 0)
eth0_dpdk_out :: ToDPDKDevice(0);

## VXLAN Encap Elements for Device eth0 (PCI 0)
encap_vxlan_121 :: VXLANEncap(SRC_MAC 00:10:22:A1:B2:C3, DST_MAC 00:AA:BB:CC:11:22, SRC_IP 10.1.0.1, DST_IP 10.1.0.2, VXLAN_ID 121);
encap_vxlan_131 :: VXLANEncap(SRC_MAC 00:10:22:A1:B2:C3, DST_MAC 00:11:22:AA:BB:33, SRC_IP 10.1.0.1, DST_IP 10.1.0.3, VXLAN_ID 131);
encap_vxlan_141 :: VXLANEncap(SRC_MAC 00:10:22:A1:B2:C3, DST_MAC 00:FF:12:34:56:78, SRC_IP 10.1.0.1, DST_IP 10.1.0.4, VXLAN_ID 141);
encap_vxlan_151 :: VXLANEncap(SRC_MAC 00:10:22:A1:B2:C3, DST_MAC AA:11:22:AA:BB:33, SRC_IP 10.1.0.1, DST_IP 10.1.0.5, VXLAN_ID 151);

## ToDevice output chains with VXLAN Encapsulation
encap_vxlan_121 -> CheckIPHeader(14) -> eth0_dpdk_out;
encap_vxlan_131 -> CheckIPHeader(14) -> eth0_dpdk_out;
encap_vxlan_141 -> CheckIPHeader(14) -> eth0_dpdk_out;
encap_vxlan_151 -> CheckIPHeader(14) -> eth0_dpdk_out;

