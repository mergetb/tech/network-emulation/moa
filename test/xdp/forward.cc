#include <queue>
#include <thread>
#include <string>
#include <array>
#include <memory>
#include "json.hpp"

extern "C" {
#include <bpf/libbpf.h>
#include <bpf/bpf.h>
#include <poll.h>
#include <time.h>
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <errno.h>
#include <pthread.h>
#include <getopt.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <net/if.h>
#include <sys/mman.h>
#include <linux/bpf.h>
#include <sys/socket.h>
#include <sys/resource.h>
#include <linux/if_xdp.h>
#include <linux/if_link.h>
}

using namespace std;
using nlohmann::json;

#ifndef AF_XDP
#define AF_XDP 44
#endif

#ifndef SOL_XDP
#define SOL_XDP 283
#endif

#define barrier() __asm__ __volatile__("": : :"memory")
#define u_smp_wmb() barrier()
#define u_smp_rmb() barrier()

#define likely(x)       __builtin_expect((x),1)
#define unlikely(x)     __builtin_expect((x),0)

#define MAX_ERRNO	4095
#define IS_ERR_VALUE(x) unlikely((unsigned long)(void *)(x) >= (unsigned long)-MAX_ERRNO)
inline bool IS_ERR_OR_NULL(const void *ptr)
{
	return unlikely(!ptr) || IS_ERR_VALUE((unsigned long)ptr);
}

#define NUM_FRAMES 131072
//#define NUM_FRAMES 524288
#define FRAME_SIZE 2048
#define FRAME_SHIFT 11
#define FRAME_HEADROOM 0
#define FRAME_TAILROOM 0
#define FQ_NUM_DESCS 1024
#define CQ_NUM_DESCS 1024
#define NUM_DESCS 2048
#define BATCH_SIZE 16

typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

struct xdp_uqueue {
	u32 cached_prod;
	u32 cached_cons;
	u32 mask;
	u32 size;
	u32 *producer{nullptr};
	u32 *consumer{nullptr};
	struct xdp_desc *ring;
	void *map;
};

struct xdpsock {
	struct xdp_uqueue rx;
	struct xdp_uqueue tx;
	int sfd;
	struct xdp_umem *umem;
	u32 outstanding_tx;
	unsigned long rx_npkts;
	unsigned long tx_npkts;
	unsigned long prev_rx_npkts;
	unsigned long prev_tx_npkts;
};

struct xdp_umem_uqueue {
	u32 cached_prod;
	u32 cached_cons;
	u32 mask;
	u32 size;
	u32 *producer{nullptr};
	u32 *consumer{nullptr};
	u64 *ring;
	void *map;
};

struct xdp_umem {
	char *frames;
	struct xdp_umem_uqueue fq;
	struct xdp_umem_uqueue cq;
	int fd;
};

#ifndef __NR_bpf
# if defined(__i386__)
#  define __NR_bpf 357
# elif defined(__x86_64__)
#  define __NR_bpf 321
# elif defined(__aarch64__)
#  define __NR_bpf 280
# elif defined(__sparc__)
#  define __NR_bpf 349
# elif defined(__s390__)
#  define __NR_bpf 351
# else
#  error __NR_bpf not defined. bpf does not support your arch.
# endif
#endif

static string exec(const char* cmd) {
    array<char, 128> buffer;
    string result;
    unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
    if (!pipe) {
        throw std::runtime_error("popen() failed!");
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        result += buffer.data();
    }
    return result;
}

static int bpf(int cmd, union bpf_attr *attr, unsigned int size)
{
	return syscall(__NR_bpf, cmd, attr, size);
}

struct Packet {
  size_t len;
  void *data;
};

// stuff =======================================================================

struct XDPDevice {

  XDPDevice(string dev) : _dev{dev} {}

  XDPDevice *peer{nullptr};

  queue<Packet> Q;

  bool _trace{false};
  string _dev;
  string _mode{"skb"};
  string _prog{"xdpallrx"};
  bool _load{true};

  struct bpf_map *_xsk_map,
                 *_vni_map,
                 *_qidconf_map;
  xdpsock *_xsk;
  struct bpf_object *_bpf_obj;

  string _prog_file{"/usr/lib/click/xdpallrx.o"};

  int _xsk_map_fd,
      _vni_map_fd,
      _qidconf_map_fd;

  u32 _ifx_index{0};
  u16 _flags{0};
  u16 _bind_flags{0};
  int _sfd,
      _bpf_fd;

  int _vni{0};

  void initialize();

  void rx();
  void tx();

  void set_rlimit();

  void init_bpf();
  void load_bpf_program();
  void open_bpf_program();
  void load_bpf_maps();
  void init_bpf_qidconf();

  void init_xsk();

  int xq_deq(struct xdp_uqueue *uq, struct xdp_desc *descs, int ndescs);
  void *xq_get_data(struct xdpsock *xsk, u64 addr);
  void hex_dump(void *pkt, size_t length, u64 addr);
  int umem_fill_to_kernel_ex(struct xdp_umem_uqueue *fq, struct xdp_desc *d, size_t nb);
  u32 xq_nb_free(struct xdp_uqueue *q, u32 ndescs);
  u32 nb_free(struct xdp_uqueue *q);
  void kick_tx(int fd);
  size_t umem_complete_from_kernel( struct xdp_umem_uqueue *cq, u64 *d, size_t nb);
  u32 umem_nb_avail(struct xdp_umem_uqueue *q, u32 nb);
  int umem_fill_to_kernel(struct xdp_umem_uqueue *fq, u64 *d, size_t nb);
  u32 xq_nb_avail(struct xdp_uqueue *q, u32 nb);
  u32 umem_nb_free(struct xdp_umem_uqueue *q, u32 nb);
  struct xdp_umem *umem_config(int sfd);

};

// functions ===================================================================

u32 XDPDevice::umem_nb_avail(struct xdp_umem_uqueue *q, u32 nb)
{
  u32 entries = q->cached_prod - q->cached_cons;

  if (entries == 0) {
    q->cached_prod = *q->producer;
    entries = q->cached_prod - q->cached_cons;
  }

  return (entries > nb) ? nb : entries;
}

u32 XDPDevice::xq_nb_avail(struct xdp_uqueue *q, u32 nb)
{
  u32 entries = q->cached_prod - q->cached_cons;
  //printf("entries: %u\n", entries);

  if (entries == 0) {
    q->cached_prod = *q->producer;
    entries = q->cached_prod - q->cached_cons;
  }

  return (entries > nb) ? nb : entries;
}

int XDPDevice::xq_deq(struct xdp_uqueue *uq, struct xdp_desc *descs, int ndescs)
{
  struct xdp_desc *r = uq->ring;
  int entries = xq_nb_avail(uq, ndescs);

  u_smp_rmb();

  for (int i = 0; i < entries; i++) {
    int idx = uq->cached_cons++ & uq->mask;
    descs[i] = r[idx];
  }

  if (entries > 0) {
    u_smp_wmb();
    *uq->consumer = uq->cached_cons;
  }

  return entries;
}

void XDPDevice::hex_dump(void *pkt, size_t length, u64 addr)
{
	const unsigned char *address = (unsigned char *)pkt;
	const unsigned char *line = address;
	size_t line_size = 32;
	unsigned char c;
	char buf[32];
	int i = 0;

	sprintf(buf, "addr=%lu", addr);
	printf("length = %zu\n", length);
	printf("%s | ", buf);
	while (length-- > 0) {
		printf("%02X ", *address++);
		if (!(++i % line_size) || (length == 0 && i % line_size)) {
			if (length == 0) {
				while (i++ % line_size)
					printf("__ ");
			}
			printf(" | ");	/* right close */
			while (line < address) {
				c = *line++;
				printf("%c", (c < 33 || c == 255) ? 0x2E : c);
			}
			printf("\n");
			if (length > 0)
				printf("%s | ", buf);
		}
	}
	printf("\n");
}

void *XDPDevice::xq_get_data(struct xdpsock *xsk, u64 addr)
{
  return &xsk->umem->frames[addr];
}


// calculate the number of free entries in a queue
u32 XDPDevice::umem_nb_free(struct xdp_umem_uqueue *q, u32 nb)
{
  u32 free_entries = q->cached_cons - q->cached_prod;

  if (free_entries >= nb)
    return free_entries;

  q->cached_cons = *q->consumer + q->size;

  return q->cached_cons - q->cached_prod;
}

// send descriptors to the kernel
int XDPDevice::umem_fill_to_kernel_ex(struct xdp_umem_uqueue *q, struct xdp_desc *d, size_t nb)
{
  // see if there is enough space in the queue
  if (umem_nb_free(q, nb) < nb)
    return -ENOSPC;

  // add the descriptors to the ring, updating the cached producer index
  for (size_t i = 0; i < nb; i++) {
    u32 idx = q->cached_prod++ & q->mask;
    q->ring[idx] = d[i].addr;
  }

  // write through the cached producer to the producer variable after a memory
  // barrier
  u_smp_wmb();
  *q->producer = q->cached_prod;

  return 0;
}

// send descriptors to the kernel by address
int XDPDevice::umem_fill_to_kernel(struct xdp_umem_uqueue *q, u64 *d,
    size_t nb) 
{

  // see if there is enough space in the queue
  if (umem_nb_free(q, nb) < nb)
    return -ENOSPC;

  // add the descriptors to the ring, updating the cached producer index
  for (size_t i = 0; i < nb; i++) {
    u32 idx = q->cached_prod++ & q->mask;
    q->ring[idx] = d[i];
  }

  // write through the cached producer to the producer variable after a memory
  // barrier
  u_smp_wmb();
  *q->producer = q->cached_prod;

  return 0;
}

struct xdp_umem* XDPDevice::umem_config(int sfd) 
{

  struct xdp_umem *umem;
  void *bufs;

  umem = (xdp_umem*)calloc(1, sizeof(struct xdp_umem));
  if (umem == NULL) {
    fprintf(stderr, "%s\n", "unable to allocate umem struct");
    exit(EXIT_FAILURE);
  }

  int err = posix_memalign(&bufs, getpagesize(), NUM_FRAMES * FRAME_SIZE);
  if (err) {
    fprintf(stderr, "failed to align buffer memory: %s\n", strerror(err));
    exit(EXIT_FAILURE);
  }

  struct xdp_umem_reg mr = {
    .addr = (u64)bufs,
    .len = NUM_FRAMES * FRAME_SIZE,
    .chunk_size = FRAME_SIZE,
    .headroom = FRAME_HEADROOM,
  };

  err = setsockopt(sfd, SOL_XDP, XDP_UMEM_REG, &mr, sizeof(mr));
  if (err) {
    fprintf(stderr, "failed to register umem: %s\n", strerror(err));
    exit(EXIT_FAILURE);
  }

  int fq_size = FQ_NUM_DESCS,
      cq_size = CQ_NUM_DESCS;

  err = setsockopt(sfd, SOL_XDP, XDP_UMEM_FILL_RING, &fq_size, sizeof(int));
  if (err) {
    fprintf(stderr, "failed to set fill ring: %s\n", strerror(err));
    exit(EXIT_FAILURE);
  }

  err = setsockopt(sfd, SOL_XDP, XDP_UMEM_COMPLETION_RING, &cq_size, sizeof(int));
  if (err) {
    fprintf(stderr, "failed to set completion ring: %s\n", strerror(err));
    exit(EXIT_FAILURE);
  }

  struct xdp_mmap_offsets off;
  socklen_t optlen = sizeof(struct xdp_mmap_offsets);
  err = getsockopt(sfd, SOL_XDP, XDP_MMAP_OFFSETS, &off, &optlen);
  if (err) {
    fprintf(stderr, "failed to get xdp mmap offsets: %s\n", strerror(err));
    exit(EXIT_FAILURE);
  }
  if (_trace) {
    printf("off.fr.producer %llx\n", off.fr.producer);
    printf("off.fr.consumer %llx\n", off.fr.consumer);
    printf("off.cr.producer %llx\n", off.cr.producer);
    printf("off.cr.consumer %llx\n", off.cr.consumer);
  }

  // fill ring
  umem->fq.map = mmap(
      0,
      off.fr.desc + FQ_NUM_DESCS*sizeof(u64),
      PROT_READ | PROT_WRITE,
      MAP_SHARED | MAP_POPULATE,
      sfd,
      XDP_UMEM_PGOFF_FILL_RING
      );
  if (umem->fq.map == MAP_FAILED) {
    fprintf(stderr, "%s\n", "failed to map fill ring");
    exit(EXIT_FAILURE);
  }

  umem->fq.mask = FQ_NUM_DESCS - 1;
  umem->fq.size = FQ_NUM_DESCS;
  umem->fq.producer = (u32*)((char*)umem->fq.map + off.fr.producer);
  umem->fq.consumer = (u32*)((char*)umem->fq.map + off.fr.consumer);
  umem->fq.ring = (u64*)((char*)umem->fq.map + off.fr.desc);
  umem->fq.cached_cons = FQ_NUM_DESCS;

  // completion ring
  umem->cq.map = mmap(
      0,
      off.cr.desc + CQ_NUM_DESCS*sizeof(u64),
      PROT_READ | PROT_WRITE,
      MAP_SHARED | MAP_POPULATE,
      sfd,
      XDP_UMEM_PGOFF_COMPLETION_RING
  );
  if (umem->cq.map == MAP_FAILED) {
    fprintf(stderr, "%s\n", "failed to map completion ring");
    exit(EXIT_FAILURE);
  }

  umem->cq.mask = CQ_NUM_DESCS - 1;
  umem->cq.size = CQ_NUM_DESCS;
  umem->cq.producer = (u32*)((char*)umem->cq.map + off.cr.producer);
  umem->cq.consumer = (u32*)((char*)umem->cq.map + off.cr.consumer);
  umem->cq.ring = (u64*)((char*)umem->cq.map + off.cr.desc);

  umem->frames = (char*)bufs;
  umem->fd = sfd;

  return umem;

}

void XDPDevice::set_rlimit() {

  struct rlimit r = {RLIM_INFINITY, RLIM_INFINITY};
  int err = setrlimit(RLIMIT_MEMLOCK, &r);
  if (err) {
    printf("failed to set rlimit: %s\n", strerror(err));
    exit(1);
  }

}

void XDPDevice::load_bpf_program() 
{
  struct bpf_prog_load_attr pla = {
    .file = _prog_file.c_str(),
    .prog_type = BPF_PROG_TYPE_XDP,
  };

  int err = bpf_prog_load_xattr(&pla, &_bpf_obj, &_bpf_fd);
  if (err) {
    printf("failed to load bpf program %s: %s\n", pla.file, strerror(err));
    exit(1);
  }
  if (_bpf_fd < 0) {
    printf("failed to load bpf program (fd): %s\n", strerror(_bpf_fd));
    exit(1);
  }

  // apply the bpf program to the specified link
  err = bpf_set_link_xdp_fd(_ifx_index, _bpf_fd, _flags);
  if(err < 0) {
    printf("xdp link set failed: %s\n", strerror(err));
    exit(1);
  }
}

void XDPDevice::open_bpf_program()
{
  struct bpf_object_open_attr attr = {
    .file = _prog_file.c_str(),
    .prog_type = BPF_PROG_TYPE_XDP,
  };

  _bpf_obj = bpf_object__open_xattr(&attr);
  if (IS_ERR_OR_NULL(_bpf_obj)) {
    printf("failed to open bpf object\n");
    exit(1);
  }

  string cmd = "ip -j link show dev " + _dev;
  string out = exec(cmd.c_str());

  json j = json::parse(out);
  int prog_id = j[0]["xdp"]["prog"]["id"];

  union bpf_attr battr = {};
  battr.prog_id = prog_id;
  _bpf_fd = bpf(BPF_PROG_GET_FD_BY_ID, &battr, sizeof(battr));
  if (_bpf_fd < 0) {
    printf("failed to get bpf program fd using id: %s\n", strerror(_bpf_fd));
    exit(1);
  }

  /*
  struct bpf_program *prog = bpf_program__next(NULL, _bpf_obj);
  if (IS_ERR_OR_NULL(_bpf_obj))
    errh->fatal("failed to open bpf program");

  _bpf_fd = bpf_program__fd(prog);
  if (_bpf_fd < 0) {
    errh->fatal("failed to load bpf program (fd): %s", strerror(_bpf_fd));
  }
  */

}

void XDPDevice::load_bpf_maps()
{
  // load socket map
  _xsk_map = bpf_object__find_map_by_name(_bpf_obj, "xsk_map");
  if (IS_ERR_OR_NULL(_xsk_map)) {
    printf("could not find xsk_map\n");
    exit(1);
  }

  _xsk_map_fd = bpf_map__fd(_xsk_map);
  if (_xsk_map_fd < 0) {
    printf("failed to load xsk_map: %s\n", strerror(_xsk_map_fd));
    exit(1);
  }

  // load vni map
  if (_vni) {
    _vni_map = bpf_object__find_map_by_name(_bpf_obj, "vni_map");
    _vni_map_fd = bpf_map__fd(_vni_map);
    if (_vni_map_fd < 0) {
      printf("failed to load vni_map: %s\n", strerror(_vni_map_fd));
      exit(1);
    }
  }

  // load queue id config map
  _qidconf_map = bpf_object__find_map_by_name(_bpf_obj, "qidconf_map");
  _qidconf_map_fd = bpf_map__fd(_qidconf_map);
  if (_qidconf_map_fd < 0) {
    printf("failed to load qidconf_map: %s\n", strerror(_qidconf_map_fd));
    exit(1);
  }
}

void XDPDevice::init_bpf_qidconf()
{
  int key = 0, q = 0;
  int err = bpf_map_update_elem(_qidconf_map_fd, &key, &q, 0);
  if (err) {
    printf("failed to configure qidconf map: %s\n", strerror(err));
    exit(1);
  }
}

void XDPDevice::init_bpf() 
{
  if (_load) 
    load_bpf_program();
  else 
    open_bpf_program();

  load_bpf_maps();
  init_bpf_qidconf();
}

void XDPDevice::init_xsk() {

  _sfd = socket(AF_XDP, SOCK_RAW, 0);
  if(_sfd < 0) {
    printf("socket failed: %s\n", strerror(_sfd));
    exit(1);
  }

  _xsk = (xdpsock*)calloc(1, sizeof(struct xdpsock));
  if(_xsk == NULL) {
    printf("%s", "failed to allocated xdp socket\n");
    exit(EXIT_FAILURE);
  }
  _xsk->sfd = _sfd;
  _xsk->outstanding_tx = 0;
  _xsk->rx_npkts = 0;
  _xsk->tx_npkts = 0;
  _xsk->prev_rx_npkts = 0;
  _xsk->prev_tx_npkts = 0;

  _xsk->umem = umem_config(_sfd);

  int ndescs = FQ_NUM_DESCS;
  int err = setsockopt(_sfd, SOL_XDP, XDP_RX_RING, &ndescs, sizeof(int));
  if (err) {
    printf("failed to set rx ring descriptor: %s\n", strerror(err));
    exit(1);
  }

  ndescs = CQ_NUM_DESCS;
  err = setsockopt(_sfd, SOL_XDP, XDP_TX_RING, &ndescs, sizeof(int));
  if (err) {
    printf("failed to set tx ring descriptor: %s\n", strerror(err));
    exit(1);
  }

  struct xdp_mmap_offsets off;
  socklen_t optlen = sizeof(struct xdp_mmap_offsets);
  err = getsockopt(_sfd, SOL_XDP, XDP_MMAP_OFFSETS, &off, &optlen);
  if (err) {
    printf("failed to get mmap offsets: %s\n", strerror(err));
    exit(1);
  }
  if (_trace) {
    printf("off.rx.producer %llx\n", off.rx.producer);
    printf("off.rx.consumer %llx\n", off.rx.consumer);
    printf("off.tx.producer %llx\n", off.tx.producer);
    printf("off.tx.consumer %llx\n", off.tx.consumer);
  }

  // rx 
  _xsk->rx.map = mmap(
      NULL,
      off.rx.desc + FQ_NUM_DESCS * sizeof(struct xdp_desc),
      PROT_READ | PROT_WRITE,
      MAP_SHARED | MAP_POPULATE,
      _sfd,
      XDP_PGOFF_RX_RING
  );
  if (_xsk->rx.map == MAP_FAILED) {
    printf("failed to mmap rx ring\n");
    exit(1);
  }

  for (u64 i=0; i < FQ_NUM_DESCS*FRAME_SIZE; i += FRAME_SIZE) {
    err = umem_fill_to_kernel(&_xsk->umem->fq, &i, 1);
    if (err) {
      printf("failed to fill rx frame to kernel: %s\n", strerror(err));
      exit(1);
    }
  }

  // tx 
  _xsk->tx.map = mmap(
      NULL,
      off.tx.desc + CQ_NUM_DESCS * sizeof(struct xdp_desc),
      PROT_READ | PROT_WRITE,
      MAP_SHARED | MAP_POPULATE,
      _sfd,
      XDP_PGOFF_TX_RING
  );
  if (_xsk->tx.map == MAP_FAILED) {
    printf("failed to mmap tx ring\n");
    exit(1);
  }

  _xsk->rx.mask = FQ_NUM_DESCS - 1;
  _xsk->rx.size = FQ_NUM_DESCS;
  _xsk->rx.producer = (u32*)((char*)_xsk->rx.map + off.rx.producer);
  _xsk->rx.consumer = (u32*)((char*)_xsk->rx.map + off.rx.consumer);
  _xsk->rx.ring = (xdp_desc*)((char*)_xsk->rx.map + off.rx.desc);

  _xsk->tx.mask = CQ_NUM_DESCS - 1;
  _xsk->tx.size = CQ_NUM_DESCS;
  _xsk->tx.producer = (u32*)((char*)_xsk->tx.map + off.tx.producer);
  _xsk->tx.consumer = (u32*)((char*)_xsk->tx.map + off.tx.consumer);
  _xsk->tx.ring = (xdp_desc*)((char*)_xsk->tx.map + off.tx.desc);
  _xsk->tx.cached_cons = CQ_NUM_DESCS;

  struct sockaddr_xdp sxdp = {
    .sxdp_family = AF_XDP,
    .sxdp_flags = _bind_flags,
    .sxdp_ifindex = _ifx_index,
    .sxdp_queue_id = 0,
  };

  err = bind(_sfd, (struct sockaddr *)&sxdp, sizeof(sxdp));
  if (err) {
    printf("failed to bind xdp socket: %s\n", strerror(err));
    exit(1);
  }

  // insert xdp sockets into map
  if (_vni) {
    //printf("%s adding vni map [%d]%d -> %d\n", name().c_str(), _vni_map_fd, _vni, _xsk->sfd);
    err = bpf_map_update_elem(_vni_map_fd, &_vni, &_xsk->sfd, 0);
    if (err) {
      printf("failed to add socket to map: %s\n", strerror(err));
      exit(1);
    }
  }

  err = bpf_map_update_elem(_xsk_map_fd, &_vni, &_xsk->sfd, 0);
  if (err) {
    printf("failed to add socket to map: %s\n", strerror(err));
    exit(1);
  }

}

u32 XDPDevice::nb_free(struct xdp_uqueue *q)
{
  return q->size - (q->cached_cons - q->cached_prod);
}

u32 XDPDevice::xq_nb_free(struct xdp_uqueue *q, u32 ndescs)
{
  u32 free_entries = q->cached_cons - q->cached_prod;

  if(free_entries >= ndescs)
    return free_entries;

  q->cached_cons = *q->consumer + q->size;
  return q->cached_cons - q->cached_prod;
}

void XDPDevice::kick_tx(int fd)
{
  int ret = sendto(fd, NULL, 0, MSG_DONTWAIT, NULL, 0);
  if (ret >= 0 || errno == ENOBUFS || errno == EAGAIN || errno == EBUSY)
    return;

  fprintf(stderr, "failed to kick tx: %s\n", strerror(ret));
  exit(EXIT_FAILURE);
}

size_t XDPDevice::umem_complete_from_kernel( struct xdp_umem_uqueue *cq, u64 *d, size_t nb)
{
  u32 idx, i, entries = umem_nb_avail(cq, nb);

  u_smp_rmb();

  for (i = 0; i < entries; i++) {
    idx = cq->cached_cons++ & cq->mask;
    d[i] = cq->ring[idx];
  }

  if (entries > 0) {
    if (_trace) {
      //printf("%s) kernel completed %d entries\n", name().c_str(), entries);
    }
    u_smp_wmb();
    *cq->consumer = cq->cached_cons;
  }

  return entries;
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


void XDPDevice::rx()
{

  u32 nf = nb_free(&_xsk->rx);
  //if (nf < 3) {
    //printf("rx: ring capacity %u\n", nf);
  //}

  static struct xdp_desc descs[BATCH_SIZE];
  unsigned int rcvd = xq_deq(&_xsk->rx, descs, BATCH_SIZE);
  if (rcvd == 0) {
    return;
  }
  if (_trace) {
    printf("%s) recvd: %u\n", _dev.c_str(), rcvd);
  }

  for (unsigned int i = 0; i < rcvd; i++) {
    char *pkt = (char*)xq_get_data(_xsk, descs[i].addr);

    if (_trace) {
      //hex_dump(pkt, descs[i].len, descs[i].addr);
      printf("%s) rx %ub @ %llu\n", _dev.c_str(), descs[i].len, descs[i].addr);
    }

    Packet p;
    p.len = descs[i].len;
    //p.data = pkt;
    p.data = malloc(p.len);
    memcpy(p.data, pkt, p.len);

    peer->Q.push(p);
  }

  _xsk->rx_npkts += rcvd;
  //printf("%s) recvd: %lu\n", _dev.c_str(), _xsk->rx_npkts);

  umem_fill_to_kernel_ex(&_xsk->umem->fq, descs, rcvd);
}

void XDPDevice::tx()
{
  struct xdp_uqueue *uq = &_xsk->tx;
  struct xdp_desc *r = uq->ring;

  kick_tx(_xsk->sfd);

  size_t i{0};
  while (!Q.empty()) {

    Packet p = Q.front();
    Q.pop();
    i++;

    if (_trace) {
      //printf("%s sending packet (%d)\n", name().c_str(), p->length());
      //hex_dump((void*)p->data(), p->length(), 1701);
    }

    //printf("%s) tx-free %d\n", name().c_str(), xq_nb_free(uq, 1));
    if (xq_nb_free(uq, 1) < 1) {
      printf("tx: ring overflow\n");
      return;
    }

    u32 idx = uq->cached_prod++ & uq->mask;
    u64 addr = (idx + FQ_NUM_DESCS) << FRAME_SHIFT;
    r[idx].addr = addr;
    r[idx].len = p.len;
    
    if(_trace) {
      printf("%s) tx %zub @ %zu\n", _dev.c_str(), p.len, addr);
    }
    memcpy(
        &_xsk->umem->frames[addr],
        p.data,
        p.len
    );

    free(p.data);

  }

  u_smp_wmb();
  *uq->producer = uq->cached_prod;
  //_xsk->outstanding_tx++;
  _xsk->outstanding_tx += i;

  //if(i == 0) return;


  u64 descs[BATCH_SIZE];
  size_t ndescs = 
    (_xsk->outstanding_tx > BATCH_SIZE) ? BATCH_SIZE : _xsk->outstanding_tx;

  unsigned int rcvd = umem_complete_from_kernel(&_xsk->umem->cq, descs, ndescs);

  if (i > 0 || rcvd > 0) {
    if(_trace) {
      printf("%s) sending %zu,%d\n", _dev.c_str(), i, _xsk->outstanding_tx);
    }
  }

  if (rcvd > 0) {
    //umem_fill_to_kernel(&_xsk->umem->fq, descs, rcvd);
    _xsk->outstanding_tx -= rcvd;
    _xsk->tx_npkts += rcvd;
  }


  kick_tx(_xsk->sfd);
}

void XDPDevice::initialize() {

  _ifx_index = if_nametoindex(_dev.c_str());
  if (!_ifx_index) {
    printf("interface \"%s\" does not exist\n", _dev.c_str());
    exit(1);
  }
  _flags |= XDP_FLAGS_SKB_MODE;

  set_rlimit();
  init_bpf();
  init_xsk();

}

int main()
{
  XDPDevice eth1{"eth1"};
  eth1.initialize();

  XDPDevice eth2{"eth2"};
  eth2.initialize();

  eth1.peer = &eth2;
  eth2.peer = &eth1;

  printf("reticulating packets\n");

  for(;;) {
    eth1.rx();
    eth2.tx();
    eth2.rx();
    eth1.tx();
  }

  /*
  thread a([&eth1, &eth2]() {
    for(;;) {

      eth1.rx();
      eth2.tx();
      usleep(10);

    }
  });

  thread b([&eth1, &eth2]() {
    for(;;) {

      eth2.rx();
      eth1.tx();
      usleep(10);

    }
  });

  a.join();
  b.join();
  */
}
