package manager

import (
	"fmt"
	"google.golang.org/grpc"
)

const ControlPort = 50052

// WithMoa executes a Moa API call aganst the provided endpoint
func WithMoa(endpoint string, f func(MoaClient) error) error {

	conn, err := grpc.Dial(endpoint, grpc.WithInsecure())
	if err != nil {
		return fmt.Errorf("failed to connect to moa service: %v", err)
	}
	client := NewMoaClient(conn)
	defer conn.Close()

	return f(client)

}
