#!/usr/bin/env python3

import sys
import subprocess
import os.path
import json

if len(sys.argv) < 4:
    print('''usage: %s <host> <bitrate> <loss>
bitrate: specify how much bandwidth to use during UDP test
loss:    specify the expected packet loss rate
''' % (os.path.basename(sys.argv[0]),), file=sys.stderr)
    sys.exit(1)

target = sys.argv[1]
bitrate = sys.argv[2]
loss = float(sys.argv[3]) * 100.0

p = subprocess.run(['iperf3', '-c', target, '-u', '-b', bitrate, '--json'], capture_output=True)
if p.returncode != 0:
    print(p.stderr, file=sys.stderr)
    os.exit(1)

o = json.loads(p.stdout.decode())
measured = float(o['end']['sum']['lost_percent'])

if loss < measured * 0.9 or loss > measured * 1.1:
    print('measured loss of %f%%, but expected %f%%' % (measured, loss), file=sys.stderr)
    sys.exit(1)

print('measured loss of %f' % loss)

sys.exit(0)
