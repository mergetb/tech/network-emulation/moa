
eth1_from :: XDPFromDevice(DEV vtep4, MODE skb, TRACE false)
eth1_to   :: XDPToDevice(DEV vtep4, MODE skb, TRACE false)

eth2_from :: XDPFromDevice(DEV vtep7, MODE skb, TRACE false)
eth2_to   :: XDPToDevice(DEV vtep7, MODE skb, TRACE false)

eth1_from -> ThreadSafeQueue(1000000) -> LinkUnqueue(BANDWIDTH 100Mbps, LATENCY 50ms) -> eth2_to
eth2_from -> ThreadSafeQueue(1000000) -> LinkUnqueue(BANDWIDTH 100Mbps, LATENCY 50ms) -> eth1_to

