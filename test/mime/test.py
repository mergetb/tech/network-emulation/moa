#!/usr/bin/python3

import sys, os, json
sys.path.insert(0, '../../')

from MoaErrors import *
from Moa import Moa
import grpc
import moa_pb2
import moa_pb2_grpc


link_blob = json.dumps([
    {
        'id': 'a',
        'type': 'mime',
        'ip': '10.0.0.1',
        'mac': '00:00:00:00:00:01',
        'stop': True,
        'flows': [{
            'compute': '10ms',
            'data': int(1e8),  #100MB
            'dst': '10.0.0.2',
            'mac': '00:00:00:00:00:02'
        }]
    },
    {
        'id': 'b',
        'type': 'mime',
        'ip': '10.0.0.2',
        'mac': '00:00:00:00:00:02',
        'flows': [{
            'compute': '50ms',
        }]
    },
    {
        'id':   '1',
        'type': 'link',

        'left_to_right': {
            'args': {
                'bandwidth':            '1Gbps',
                'delay':                '0ms',
                'loss':                 '0',
                'use_codel':             True
            },
            'id':       '1_left',
            'inputs':   [ 'a' ],
            'output':   'b',
            'type':     'unilink',
        },

        'right_to_left': {
            'args': {
                'bandwidth':            '1Gbps',
                'delay':                '0ms',
                'loss':                 '0',
                'use_codel':             True
            },
            'id':       '1_right',
            'inputs':   [ 'b' ],
            'output':   'a',
            'type':     'unilink',
        }
    }
])


with grpc.insecure_channel('127.0.0.1:50052') as channel:

    stub = moa_pb2_grpc.MoaStub(channel)

    try:
        resp = stub.NewEmulation(moa_pb2.NewEmulationRequest(
            emulation_id = 1,
            emulator = 'click',
            emulation_blob = link_blob,
        ))
        print("new emulation ok")
        print(resp)

        resp = stub.FinalizeEmulation(moa_pb2.FinalizeEmulationRequest(
            emulation_id = 1,
        ))
        print("finalize emulation ok")
        print(resp)

        #resp = stub.StartEmulation(moa_pb2.StartEmulationRequest(
        #    emulation_id = 1,
        #))
        #print("start emulation ok")
        #print(resp)

    except grpc.RpcError as e:
        print("grpc error")
        raise e

    except Exception as e:
        raise e

