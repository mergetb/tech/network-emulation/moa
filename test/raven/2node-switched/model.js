/* 2 Node Moa Testing Topology
 * ===========================
 *
 *             -------
 *             | moa |
 *             -------
 *                | 
 *        ------------------
 *        |     switch     |
 *        ------------------
 *            |        |
 *          -----    -----
 *          | a |    | b |
 *          -----    -----
 *
 */

topo = {
  name: 'moa-2node-switched',
  nodes: [deb('a'), deb('b'), deb('moa')],
  switches: [ cumulus('sw') ],
  links: [
    Link('moa', 1, 'sw', 1),
    Link('a',   1, 'sw', 2),
    Link('b',   1, 'sw', 3),
  ],
}

function deb(name) {
  return {
    name: name,
    image: 'debian-buster',
    cpu: { cores: 2 },
    memory: { capacity: GB(4) },
    mounts: [{ source: env.PWD+"/../../..", point: '/tmp/moa'}],
  }
}

function cumulus(name) {
  return {
    name: name,
    image: 'cumulusvx-3.7',
    cpu: { cores: 2 },
    memory: { capacity: GB(4) },
    mounts: [{ source: env.PWD+"/../../..", point: '/tmp/moa'}],
  }
}
