package main

import (
	// standard
	"bytes"
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"net"
	"os"
	"os/exec"
	"strconv"
	"time"

	// merge deps
	"gitlab.com/mergetb/tech/network-emulation/moa/pkg/common"
	"gitlab.com/mergetb/tech/network-emulation/moa/pkg/manager"

	// deps
	log "github.com/sirupsen/logrus"
	bolt "go.etcd.io/bbolt"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type EmuState struct {
	Id   int
	Mzid string
	Pid  int // unused, fetched from systemd
}

type LinkDef struct {
	Id         int      // unique link ID (internal, no clients use this)
	Interfaces []string // network interfaces for this link
	Capacity   int64    // bps
	Latency    int32    // ns
	Loss       float32  // drop prob
	Tags       []string // tags from the XIR
	ExpNodes   []string // experimental node names connected to this link
}

const dbpath = "/var/moa/state.db"
const emulation_dir = "/var/moa/emulation" // directory where click configuration files are stored
const log_dir = "/var/moa/log"             // directory where click logs are stored
const nthreads = 8                         // number of threads to create (must match what is in the click@.service file!)

type moaServer struct {
	manager.UnimplementedMoaServer
}

var (
	db       *bolt.DB
	stateKey = []byte("state")
	thrElems [nthreads]int // # of elements in each thread
)

// return the click configuration filename for this emulation
func (p *EmuState) Path() string {
	return fmt.Sprintf("%s/emulation_%d.moa", emulation_dir, p.Id)
}

func (p *EmuState) ControlPath() string {
	return fmt.Sprintf("/click%d", p.Id)
}

// Query systemd to determine if the click process for this emulation is running
func (s *EmuState) IsActive() bool {
	svc := fmt.Sprintf("click@%d", s.Id)
	cmd := exec.Command("systemctl", "is-active", svc)
	if err := cmd.Run(); err != nil {
		if _, ok := err.(*exec.ExitError); ok {
			// ran but exited abnormally -- indicates the service is not active
		} else {
			// some other error
			log.WithFields(log.Fields{"cmd": cmd, "err": err, "state": s}).Error("unable to determine active state")
		}
		return false
	}
	return true
}

func (s *EmuState) Systemctl(action string) error {
	name := fmt.Sprintf("click@%d", s.Id)
	cmd := exec.Command("systemctl", action, name)
	if err := cmd.Run(); err != nil {
		log.WithFields(log.Fields{"cmd": cmd, "err": err, "action": action}).Error("failed to invoke systemctl")
		return status.Error(codes.Internal, "failed to start/stop emulation service")
	}
	return nil
}

func (s *EmuState) RemoveSocket() error {
	sockpath := s.ControlPath()
	// remove the control socket if present
	// verify that the unix control socket isn't present already
	// this can happen if click was already running and our state is out of sync
	err := os.Remove(sockpath)
	if err != nil && os.IsNotExist(err) == false {
		log.WithFields(log.Fields{"err": err, "path": sockpath}).Error("failed to remove control socket")
		return status.Error(codes.Internal, "failed to remove click control socket")
	}
	return nil
}

func (s *EmuState) Start() error {
	if s.IsActive() {
		log.Warn("click service is already running")
		return nil
	}
	// try to remove the socket in order to prevent error on startup
	if err := s.RemoveSocket(); err != nil {
		return err
	}
	if err := s.Systemctl("start"); err != nil {
		return err
	}
	return nil
}

// Stop the Click for this Emulation
func (s *EmuState) Stop() {
	s.Systemctl("stop")
	s.RemoveSocket()
}

// Save the emulation state back to the DB
func (s EmuState) Save(b *bolt.Bucket) error {
	log.WithFields(log.Fields{"state": s}).Info("saving state")
	statebuf, err := json.Marshal(s)
	if err != nil {
		log.WithFields(log.Fields{"err": err}).Error("error marshaling state")
		return status.Error(codes.Internal, "failed to marshal emulation state")
	}

	// store the state
	err = b.Put(stateKey, statebuf)
	if err != nil {
		log.WithFields(log.Fields{
			"err":   err.Error(),
			"key":   "state",
			"value": statebuf,
		}).Error("failed to put state")
		return status.Error(codes.Internal, "failed to save emulation state")
	}
	return nil
}

// return the thread thread with the least number of elements packed into it
func getThread() int {
	idx := 0
	cnt := 1<<31 - 1

	for k, v := range thrElems {
		if v < cnt {
			cnt = v
			idx = k
		}
	}

	return idx
}

// Generate a click config snippet for the specified network link
func genLinkConfig(f *os.File, ld *LinkDef) {
	fmt.Fprintf(f, "\n// Link ID %d, tags=%v, interfaces=%v\n\n", ld.Id, ld.Tags, ld.Interfaces)

	for _, iface := range ld.Interfaces {
		fmt.Fprintf(f, "%s_random_sample :: RandomSample(DROP %f);\n", iface, ld.Loss)
		fmt.Fprintf(f, "%s_bwdelay :: LinkUnqueue(LATENCY %dns, BANDWIDTH %dbps);\n", iface, ld.Latency, ld.Capacity)
		fmt.Fprintf(f, "%s_input :: XDPFromDevice(DEV %s, MODE skb);\n", iface, iface)
		fmt.Fprintf(f, "%s_set_ts :: SetTimestamp(FIRST true) -> ThreadSafeQueue(1000000) -> CoDel() -> %s_random_sample -> %s_bwdelay -> XDPToDevice(DEV %s, MODE skb);\n\n", iface, iface, iface, iface)
	}

	if len(ld.Interfaces) == 2 {
		// duplex
		fmt.Fprintf(f, "%s_input -> %s_set_ts;\n", ld.Interfaces[0], ld.Interfaces[1])
		fmt.Fprintf(f, "%s_input -> %s_set_ts;\n", ld.Interfaces[1], ld.Interfaces[0])
	} else {
		// mp
		// connect all interfaces via an ethernet switch
		fmt.Fprintf(f, "mpl_%d_switch :: EtherSwitch();\n", ld.Id)
		for n, iface := range ld.Interfaces {
			fmt.Fprintf(f, "%s_input -> [%d]mpl_%d_switch;\n", iface, n, ld.Id)
			fmt.Fprintf(f, "mpl_%d_switch[%d] -> %s_set_ts;\n", ld.Id, n, iface)
		}
	}

	// Map this group of interfaces to a click router thread.
	// Keep all interfaces that are part of a MPL in the same thread, but
	// try to distribute equal number of elements among available threads.
	tid := getThread()
	thrElems[tid] += len(ld.Interfaces)

	fmt.Fprintf(f, "StaticThreadSched(")
	for n, iface := range ld.Interfaces {
		if n > 0 {
			fmt.Fprintf(f, ", ")
		}
		fmt.Fprintf(f, "%s_input %d, %s_bwdelay %d", iface, tid, iface, tid)
	}
	fmt.Fprintf(f, ");\n")
}

func (s *moaServer) Create(ctx context.Context, req *manager.CreateRequest) (*manager.MoaResponse, error) {
	log.WithFields(log.Fields{
		"request": req,
	}).Info("create emulation")

	k := strconv.FormatInt(int64(req.EmulationId), 10)

	var err error
	var prefix string

	// map of tags to link-ID
	tags := make(map[string][]string, 0)

	err = db.Update(func(tx *bolt.Tx) error {
		// create a root-level bucket for this emulation
		b, err := tx.CreateBucket([]byte(k))
		if err == bolt.ErrBucketExists {
			log.Error("emulation already exists")
			return status.Error(codes.AlreadyExists, "emulation already exists")
		} else if err != nil {
			return status.Error(codes.Internal, "failed to create bucket")
		}

		state := EmuState{
			Id:   int(req.EmulationId),
			Mzid: req.Mzid,
		}

		var fp *os.File
		fp, err = os.Create(state.Path())
		if err != nil {
			log.WithFields(log.Fields{"path": state.Path(), "err": err}).Error("failed to open file for writing")
			return status.Error(codes.Internal, "could not open config file for writing")
		}
		defer fp.Close()
		fmt.Fprintf(fp, "// Emulation ID %d, Mzid %s, created by moad on %s\n", state.Id, state.Mzid, time.Now())

		// loop over all links defined for this emulation
		for _, link := range req.GetLinks() {
			id, _ := b.NextSequence()

			params := link.GetParams()

			e := LinkDef{
				Id:         int(id),
				Interfaces: link.GetInterfaces(),
				Tags:       link.GetTags(),
				Capacity:   params.GetCapacity(),
				Latency:    params.GetLatency(),
				Loss:       params.GetLoss(),
				ExpNodes:   link.GetExpNodes(),
			}

			prefix = fmt.Sprintf("link/%d/", e.Id)
			err = storeLink(b, prefix, &e)
			if err != nil {
				return err
			}

			// update tag map to include this link
			for _, tag := range e.Tags {
				taglist, ok := tags[tag]
				if !ok {
					taglist = make([]string, 0)
				}
				taglist = append(taglist, prefix)
				tags[tag] = taglist
			}

			genLinkConfig(fp, &e)
		}

		// write the tag map into the DB
		for tag, v := range tags {
			buf, err := json.Marshal(v)
			if err != nil {
				log.Error("failed to marshal tag entry")
				return status.Error(codes.Internal, "failed to marshal tag entry")
			}
			// append a trailing slash on the tag so we don't match "ab" against "abab"
			prefix = fmt.Sprintf("tag/%s/", tag)
			err = b.Put([]byte(prefix), buf)
			if err != nil {
				log.WithFields(log.Fields{"err": err}).Error("failed to store tag entry")
				return status.Error(codes.Internal, "failed to store tag entry")
			}
		}

		err = state.Start()
		if err == nil {
			err = state.Save(b)
		}

		return err
	})

	return &manager.MoaResponse{}, err
}

func fetchEmulation(tx *bolt.Tx, id int32) (b *bolt.Bucket, err error) {
	b = tx.Bucket(emulationKey(id))
	if b == nil {
		log.WithFields(log.Fields{"id": id}).Error("emulation bucket does not exist")
		err = status.Error(codes.InvalidArgument, "emulation does not exist")
	}
	return
}

func fetchState(b *bolt.Bucket) (*EmuState, error) {
	state := b.Get(stateKey)
	if state == nil {
		log.Error("could not get emulation state from db")
		return nil, status.Error(codes.Internal, "could not find state for emulation")
	}

	var emu EmuState
	if err := json.Unmarshal(state, &emu); err != nil {
		log.WithFields(log.Fields{"json": state}).Error("unable to parse json")
		return nil, status.Error(codes.Internal, "unable to parse emulation state")
	}
	return &emu, nil
}

func emulationKey(eid int32) []byte {
	return []byte(strconv.FormatInt(int64(eid), 10))
}

func (s *moaServer) Destroy(ctx context.Context, req *manager.EmulationRequest) (*manager.MoaResponse, error) {
	log.WithFields(log.Fields{"req": req}).Info("destroy emulation")

	err := db.Update(func(tx *bolt.Tx) error {
		b, err := fetchEmulation(tx, req.EmulationId)
		if err != nil {
			return err
		}

		state, err := fetchState(b)
		if err != nil {
			return err
		}

		state.Stop()

		// remove click configuration file
		err = os.Remove(state.Path())
		if err != nil {
			log.WithFields(log.Fields{"path": state.Path()}).Warn("failed to remove file")
			// ignore
		}

		err = tx.DeleteBucket(emulationKey(req.EmulationId))
		if err != nil {
			log.WithFields(log.Fields{"err": err}).Error("delete bucket failed")
			return status.Error(codes.Internal, "failed to delete bucket")
		}

		return nil
	})

	return &manager.MoaResponse{}, err
}

func (s *moaServer) Start(ctx context.Context, req *manager.EmulationRequest) (*manager.MoaResponse, error) {
	log.WithFields(log.Fields{"req": req}).Info("start emulation")
	err := db.Update(func(tx *bolt.Tx) error {
		var state *EmuState

		b, err := fetchEmulation(tx, req.EmulationId)
		if err == nil {
			state, err = fetchState(b)
		}
		if err == nil {
			err = state.Start()
		}
		if err == nil {
			err = state.Save(b)
		}
		return err
	})
	return &manager.MoaResponse{}, err
}

func (s *moaServer) Stop(ctx context.Context, req *manager.EmulationRequest) (*manager.MoaResponse, error) {
	log.WithFields(log.Fields{"req": req}).Info("stop emulation")
	err := db.Update(func(tx *bolt.Tx) error {
		var state *EmuState

		b, err := fetchEmulation(tx, req.EmulationId)
		if err == nil {
			state, err = fetchState(b)
		}
		if err == nil {
			state.Stop()
			err = state.Save(b)
		}
		return err
	})
	return &manager.MoaResponse{}, err
}

func parseLink(buf []byte) *LinkDef {
	var ld LinkDef
	err := json.Unmarshal(buf, &ld)
	if err != nil {
		log.WithFields(log.Fields{"buf": buf}).Error("unable to parse linkdef json")
		return nil
	}
	return &ld
}

// fetch the LinkDef from the db and deserialize the json
func fetchLink(b *bolt.Bucket, s string) *LinkDef {
	v := b.Get([]byte(s))
	if v == nil {
		log.WithFields(log.Fields{"link": s}).Error("unable to fetch linkdef")
		return nil
	}
	return parseLink(v)
}

func storeLink(b *bolt.Bucket, key string, ld *LinkDef) error {
	buf, err := json.Marshal(ld)
	if err != nil {
		log.Error("failed to marshal linkdef")
		return status.Error(codes.Internal, "failed to put marshal link def")
	}

	err = b.Put([]byte(key), buf)
	if err != nil {
		log.WithFields(log.Fields{"link": key, "json": string(buf)}).Error("failed to put link update")
		return status.Error(codes.Internal, "failed to put link update")
	}

	return nil
}

func updateLink(emuid int, b *bolt.Bucket, link string, p *manager.LinkParams) error {
	ld := fetchLink(b, link)
	if ld == nil {
		return status.Error(codes.Internal, "failed to fetch link def")
	}

	conn, err := Connect(fmt.Sprintf("/click%d", emuid))
	if err != nil {
		log.WithFields(log.Fields{"err": err}).Error("failed to connect to click socket")
		return status.Error(codes.Internal, "failed to connect to click control socket")
	}
	defer conn.Close()

	changed := false
	if p.Capacity != 0 && p.Capacity != ld.Capacity {
		log.WithFields(log.Fields{"new": p.Capacity, "old": ld.Capacity, "id": ld.Id}).Info("link capacity changed")
		changed = true
		ld.Capacity = p.Capacity

		for _, iface := range ld.Interfaces {
			v := fmt.Sprintf("%dbps", ld.Capacity)
			if err := conn.WriteHandler(fmt.Sprintf("%s_bwdelay", iface), "bandwidth", v); err != nil {
				log.WithFields(log.Fields{"err": err}).Error("write handler failed")
				return status.Error(codes.Internal, "click write handler failed")
			}
		}
	}
	if p.Latency != 0 && p.Latency != ld.Latency {
		log.WithFields(log.Fields{"new": p.Latency, "old": ld.Latency, "id": ld.Id}).Info("link latency changed")
		changed = true
		ld.Latency = p.Latency
		for _, iface := range ld.Interfaces {
			v := fmt.Sprintf("%dns", ld.Latency)
			if err := conn.WriteHandler(fmt.Sprintf("%s_bwdelay", iface), "latency", v); err != nil {
				log.WithFields(log.Fields{"err": err}).Error("write handler failed")
				return status.Error(codes.Internal, "click write handler failed")
			}
		}
	}
	if p.EnforceLoss && p.Loss != ld.Loss {
		log.WithFields(log.Fields{"new": p.Loss, "old": ld.Loss, "id": ld.Id}).Info("link loss changed")
		changed = true
		ld.Loss = p.Loss
		for _, iface := range ld.Interfaces {
			v := fmt.Sprintf("%f", ld.Loss)
			if err := conn.WriteHandler(fmt.Sprintf("%s_random_sample", iface), "drop_prob", v); err != nil {
				log.WithFields(log.Fields{"err": err}).Error("write handler failed")
				return status.Error(codes.Internal, "click write handler failed")
			}
		}
	}
	if changed {
		err := storeLink(b, link, ld)
		if err != nil {
			return err
		}
	}
	return nil
}

func (s *moaServer) Update(ctx context.Context, req *manager.UpdateRequest) (*manager.MoaResponse, error) {
	log.WithFields(log.Fields{"req": req}).Info("update emulation")

	err := db.Update(func(tx *bolt.Tx) error {
		b, err := fetchEmulation(tx, req.EmulationId)
		if err != nil {
			return err
		}

		// for each link update
		for _, u := range req.GetUpdates() {
			// for each tag that belongs to this link update
			for _, tag := range u.GetTags() {
				// retrieve the click elements that match this tag
				key := fmt.Sprintf("tag/%s/", tag)
				v := b.Get([]byte(key))
				if v == nil {
					log.WithFields(log.Fields{"tag": tag}).Warn("tag not defined")
					return status.Error(codes.InvalidArgument, fmt.Sprintf("%s: tag is not defined", tag))
				}

				// the value is a json-encoded list of link keys
				var e []string
				err := json.Unmarshal(v, &e)
				if err != nil {
					log.WithFields(log.Fields{"json": v}).Error("unable to unmarshal json")
					return status.Error(codes.Internal, "unable to unmarshal json tag entry")
				}

				// list of link keys
				for _, link := range e {
					err = updateLink(int(req.EmulationId), b, link, u.LinkParams)
					if err != nil {
						return err
					}
				}
			}
		}

		return nil
	})

	return &manager.MoaResponse{}, err
}

func (s *moaServer) List(ctx context.Context, req *manager.ListRequest) (*manager.ListResponse, error) {
	log.Info("list request")

	emulations := make([]*manager.Emulation, 0)

	err := db.View(func(tx *bolt.Tx) error {
		c := tx.Cursor()
		for k, _ := c.First(); k != nil; k, _ = c.Next() {

			emulationBucket := tx.Bucket(k)

			emu, err := fetchState(emulationBucket)
			if err != nil {
				return err
			}

			active := 0
			if emu.IsActive() {
				active = 1
			}
			emulations = append(emulations, &manager.Emulation{
				Id:   int32(emu.Id),
				Mzid: emu.Mzid,
				Pid:  int32(active),
			})
		}
		return nil
	})
	return &manager.ListResponse{Emulations: emulations}, err
}

func showEmu(b *bolt.Bucket) ([]*manager.LinkSpec, error) {
	links := make([]*manager.LinkSpec, 0)

	// all links in this emulation have a "link/" prefix for the key
	prefix := []byte("link/")
	linkCursor := b.Cursor()
	for k, v := linkCursor.Seek(prefix); k != nil && bytes.HasPrefix(k, prefix); k, v = linkCursor.Next() {
		link := parseLink(v)
		if link == nil {
			return nil, status.Error(codes.Internal, "unable to parse link def")
		}
		links = append(links, &manager.LinkSpec{
			Interfaces: link.Interfaces,
			Params: &manager.LinkParams{
				Capacity: link.Capacity,
				Latency:  link.Latency,
				Loss:     link.Loss,
			},
			Tags:     link.Tags,
			ExpNodes: link.ExpNodes,
		})
	}

	return links, nil
}

// Return a list of all links that belong to a given materialzation
func (s *moaServer) Show(ctx context.Context, req *manager.ShowRequest) (*manager.ShowResponse, error) {

	var links []*manager.LinkSpec
	var mzid string
	var emuid int32

	log.WithFields(log.Fields{"req": req}).Info("show request")

	// there is no way to look up by mzid, but the number of emulations should be small enough that iterating over all emulations will be fast
	// enough for our purposes
	err := db.View(func(tx *bolt.Tx) error {
		// special case, if the mzid is an integer, interpret it as the Emulation ID instead
		if v, err := strconv.Atoi(req.Mzid); err == nil {
			// show by Emu ID
			emuid = int32(v)

			b := tx.Bucket([]byte(req.Mzid))
			if b == nil {
				return status.Error(codes.InvalidArgument, fmt.Sprintf("%s: emulation not found", req.Mzid))
			}

			if state, err := fetchState(b); err != nil {
				return err
			} else {
				mzid = state.Mzid
			}

			if v, err := showEmu(b); err != nil {
				return err
			} else {
				links = v
			}
		} else {
			// show by mzid

			mzid = req.Mzid
			c := tx.Cursor()
			for k, _ := c.First(); k != nil; k, _ = c.Next() {
				b := tx.Bucket(k)

				if state, err := fetchState(b); err != nil {
					return err
				} else if state.Mzid == req.Mzid {
					emuid = int32(state.Id)
					if v, err := showEmu(b); err != nil {
						return err
					} else {
						links = v
						break // there should only be one emulation per Mzid
					}

				}
			}
		}
		return nil
	})

	return &manager.ShowResponse{Links: links, Mzid: mzid, EmulationId: emuid}, err
}

func init_bolt() {
	var err error
	db, err = bolt.Open(dbpath, 0666, nil)
	if err != nil {
		log.WithFields(log.Fields{
			"err":  err.Error(),
			"path": dbpath,
		}).Error("failed to open database")
		os.Exit(1)
	}
}

func init_logging() {
}

func main() {

	debug := flag.Bool("debug", false, "enable extra debug logging")

	flag.Parse()

	// daemon mode
	if *debug {
		log.SetLevel(log.DebugLevel)
	} else {
		log.SetLevel(log.InfoLevel)
	}
	log.Info(fmt.Sprintf("moad %s starting up on port %d", common.Version, manager.ControlPort))

	init_bolt()

	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", manager.ControlPort))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	grpcServer := grpc.NewServer()
	manager.RegisterMoaServer(grpcServer, &moaServer{})
	grpcServer.Serve(lis)
}
