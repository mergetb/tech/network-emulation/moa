// 2 xdp interface test

eth1 :: XDPDevice(DEV eth1, MODE skb, PROG xdpvxlan)
eth2 :: XDPDevice(DEV eth2, MODE skb, PROG xdpvxlan)

eth1 -> Queue(10) -> eth2;
eth2 -> Queue(10) -> eth1;

