a :: Mime(
  IP       10.0.0.1,
  MAC      00:00:00:00:00:01,

  COMPUTE0 10ms, 
  DATA0    100000000,
  DST0     10.0.0.2,
  MAC0     00:00:00:00:00:02,

  COMPUTE1 50ms, 
  DATA1    100000000,
  DST1     10.0.0.2,
  MAC1     00:00:00:00:00:02,

  STOP     true
);

b :: Mime(
  IP      10.0.0.2,
  MAC     00:00:00:00:00:02,

  COMPUTE0 50ms,
  COMPUTE1 50ms
);

a -> Queue(100000) -> b;
b -> Queue(100000) -> a;
