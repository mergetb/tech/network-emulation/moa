#ifndef CLICK_RANDOM_MOBILITY_HH
#define CLICK_RANDOM_MOBILITY_HH

#include "mobility.hh"
#include <random>

CLICK_DECLS

class RandomMobility : public Mobility
{
  protected:
    double _move_probability;     // The likelihood that we move
    double _move_speed;         // The velocity to move at
    int _direction_segments; // How many different directions we can move
    double _discrete_angle;
    double _angle_calculator_multiplier;

    std::minstd_rand _generator(std::random_device{}()) ;
    std::uniform_real_distribution<double> _distribution;

    void setup();
    
  public:
    RandomMobility();
    RandomMobility(double m_prob, double speed, int directions);
    ~RandomMobility();
    
    void updateNode(WirelessNode* node, double &dx, double &dy);
    void configureFromFile(std::string filename);
};

CLICK_ENDDECLS
#endif
