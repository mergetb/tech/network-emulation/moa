import sys
sys.path.insert(0, '../')

from ClickBuilder import ClickBuilder
from ClickWriter import ClickWriter
from MoaErrors import *

class TestWriter():
    def __init__(self):
        self.data = ""

    def clear(self):
        self.data = ""

    def validateTest(self, expected_string, error_type, error_msg_hdr = ""):
        if self.data != expected_string:
            raise MoaTestError(error_type, "%s %s != %s" % (error_msg_hdr, expected_string, self.data))

        return 0

    # Ignore comments
    def writeElementDeclarations(self, elements, comment):
        self.data += "## %s\n" % comment
        for element in elements:
            self.data += element
            self.data += '\n'

        self.data += '\n'

    def writePipelines(self, pipelines, comment):
        if(comment != ""):
            self.data += "## %s\n" % comment
            
        for pipeline in pipelines:
            self.data += pipeline
            self.data += '\n'

        self.data += '\n'

    def printData(self):
        print self.data
        
def validateTest(expected, result, error_type, error_msg_hdr = ""):
    if(expected != result):
        raise MoaTestError(error_type, "%s %s != %s" % (error_msg_hdr, expected, result))

def main():
    unitTests()

def unitTests():
    tw = TestWriter()
    cb = ClickBuilder(tw)
    
    testShapeUnidirectionalLink(cb)
    testShapeLink(cb)
    testShapeLinks(cb, tw)

    testBuildMPL(cb, tw)
    
    testCreateInboundVXLANHandling(cb, tw)
    testCreateOutboundVXLANHandling(cb, tw) 
    testUpdateLinkIOElements(cb)
    
def testShapeUnidirectionalLink(cb):
    args = {'loss': '0.2', 'bandwidth': '2Gbps', 'delay': '1ms', 'useCodel': True}
    (pipeline, declarations) = cb.shapeUnidirectionalLink("test", "in", "out", args)
    test_pipeline = "in -> test_in_queue -> test_codel -> test_loss -> test_bwdelay -> out;"
    test_declarations = ["test_in_queue :: ThreadSafeQueue(1000);",
                         "test_codel :: CoDel();",
                         "test_loss :: RandomSample(DROP 0.2);",
                         "test_bwdelay :: LinkUnqueue(1ms, 2Gbps);"]

    validateTest(test_pipeline, pipeline, "Shape Unidirectional Link Error", error_msg_hdr = "Pipeline")
    validateTest(test_declarations, declarations, "Shape Unidirectional Link Error", error_msg_hdr = "Declarations")

    print "Shape Unidirectional Link Passed"
    return 0

def testShapeLink(cb):
    l_pipeline = ""
    r_pipeline = ""
    elements = []
    args = {'loss': '0.2', 'bandwidth': '2Gbps', 'delay': '1ms', 'useCodel': True}

    (l_pipeline, r_pipeline, elements) = cb.shapeLink("1", "in1", "in2", "out1", "out2", args)
    test_l_pipeline = "in1 -> link_1_left_in_queue -> link_1_left_codel -> link_1_left_loss -> link_1_left_bwdelay -> out2;"
    test_r_pipeline = "in2 -> link_1_right_in_queue -> link_1_right_codel -> link_1_right_loss -> link_1_right_bwdelay -> out1;"
    test_elements = ["link_1_left_in_queue :: ThreadSafeQueue(1000);",
                     "link_1_left_codel :: CoDel();",
                     "link_1_left_loss :: RandomSample(DROP 0.2);",
                     "link_1_left_bwdelay :: LinkUnqueue(1ms, 2Gbps);",
                     "link_1_right_in_queue :: ThreadSafeQueue(1000);",
                     "link_1_right_codel :: CoDel();",
                     "link_1_right_loss :: RandomSample(DROP 0.2);",
                     "link_1_right_bwdelay :: LinkUnqueue(1ms, 2Gbps);"]

    validateTest(test_l_pipeline, l_pipeline, "Shape Link Error", error_msg_hdr = "Left Pipeline")
    validateTest(test_r_pipeline, r_pipeline, "Shape Link Error", error_msg_hdr = "Right Pipeline")    
    validateTest(test_elements, elements, "Shape Link Error", error_msg_hdr = "Declarations")

    print "Shape Link Passed"
    return 0

def testShapeLinks(cb, tw):
    link1 = {'left_inbound': 'vxlan[0]', 'left_outbound': 'vxlan_encap1',
             'right_inbound': 'vxlan[1]', 'right_outbound': 'vxlan_encap2',
             'id': '1',
             'args': {'loss': '0.1', 'bandwidth': '10Gbps', 'delay': '10ms', 'useCodel': True}}
    link2 = {'left_inbound': 'vxlan[2]', 'left_outbound': 'vxlan_encap3',
             'right_inbound': 'vxlan[3]', 'right_outbound': 'vxlan_encap4',
             'id': '2',
             'args': {'loss': '0', 'bandwidth': '100Gbps', 'delay': '5ms', 'useCodel': True}}


    try:
        cb.shapeLinks([])
    except MoaInputError as mie:
        if(mie.iput != "Length of links <= 0"):
            raise

    tw.clear()
    cb.shapeLinks([link1])
    expected_fh = open("builder_expected/shape_links_test1.click", "r")
    expected = expected_fh.read()
    expected_fh.close()
    tw.validateTest(expected, "Output mismatch", error_msg_hdr="ShapeLinks")

    tw.clear()
    cb.shapeLinks([link1, link2])
    expected_fh = open("builder_expected/shape_links_test2.click", "r")
    expected = expected_fh.read()
    expected_fh.close()
    tw.validateTest(expected, "Output mismatch", error_msg_hdr="ShapeLinks")

    print "Shape Links Passed"
    return 0

def testBuildMPL(cb, tw):
    mpl1 = {'type': 'broadcast', 'id': '1',
           'output_elements': ['vxlan1', 'vxlan2', 'vxlan3']}
    
    mpl2 = {'type': 'broadcast', 'id': '1',
            'output_elements': ['todev1', 'vxlan2']}

    try:
        cb.buildMPL(None)
    except MoaInputError as mie:
        if(mie.iput != "Invalid MPL to construct"):
            raise

    tw.clear()
    cb.buildMPL(mpl1)
    expected_fh = open("builder_expected/broadcast_mpl_test1.click", "r")
    expected = expected_fh.read()
    expected_fh.close()
    tw.validateTest(expected, "Output mismatch", error_msg_hdr="BuildMPL")

    tw.clear()
    cb.buildMPL(mpl2)
    expected_fh = open("builder_expected/broadcast_mpl_test2.click", "r")
    expected = expected_fh.read()
    expected_fh.close()
    tw.validateTest(expected, "Output mismatch", error_msg_hdr="BuildMPL")

    print "Build MPL Test Passed"
    return 0
    

def testCreateInboundVXLANHandling(cb, tw):
    # create inputs!
    vxlans1 = [{'id': '121', 'dst_ip': '10.1.0.2', 'dst_mac': '00:AA:BB:CC:11:22'},
               {'id': '131', 'dst_ip': '10.1.0.3', 'dst_mac': '00:11:22:AA:BB:33'},
               {'id': '141', 'dst_ip': '10.1.0.4', 'dst_mac': '00:FF:12:34:56:78'},
               {'id': '151', 'dst_ip': '10.1.0.5', 'dst_mac': 'AA:11:22:AA:BB:33'}]

    vxlans2 = [{'id': '221', 'dst_ip': '10.2.0.2', 'dst_mac': '20:AA:BB:CC:11:22'},
               {'id': '231', 'dst_ip': '10.2.0.3', 'dst_mac': '20:11:22:AA:BB:33'}]
    
    interfaces = {}
    interfaces['eth0'] = {'vxlans': vxlans1, 'name': 'eth0',
                          'pci': '0', 'prefix': '10.1.0.0/24', 'ip': '10.1.0.1',
                          'mac': '00:10:22:A1:B2:C3',
                          'todevice_element': 'out1'}
    interfaces['eth1'] = {'vxlans': vxlans2, 'name': 'eth1',
                          'pci': '1', 'prefix': '10.2.0.0/24', 'ip': '10.2.0.1',
                          'mac': '20:10:22:A1:B2:C3',
                          'todevice_element': 'out2'}

    try:
        cb.createInboundVXLANHandling({})
    except MoaInputError as mie:
        if(mie.iput != "Length of interfaces <= 0"):
            raise

    tw.clear()
    cb.createInboundVXLANHandling({'eth0': interfaces['eth0']})

    expected_fh = open("builder_expected/inbound_vxlan_test1.click", "r")
    expected = expected_fh.read()
    expected_fh.close()
    tw.validateTest(expected, "Output mismatch", error_msg_hdr="createInboundVXLANHandling")

    tw.clear()
    cb.createInboundVXLANHandling(interfaces)
    
    expected_fh = open("builder_expected/inbound_vxlan_test2.click", "r")
    expected = expected_fh.read()
    expected_fh.close()
    tw.validateTest(expected, "Output mismatch", error_msg_hdr="createInboundVXLANHandling")

    validateTest(interfaces['eth0']['input_element'], 'vxlan_demux_1', "Input Element Mismatch",
                 error_msg_hdr="createInboundVXLANHandling")
    validateTest(interfaces['eth1']['input_element'], 'vxlan_demux_0', "Input Element Mismatch",
                 error_msg_hdr="createInboundVXLANHandling")

    print "Inbound VXLAN Handling Passed"
    return 0

def testCreateOutboundVXLANHandling(cb, tw):
    # create inputs!
    vxlans1 = [{'id': '121', 'dst_ip': '10.1.0.2', 'dst_mac': '00:AA:BB:CC:11:22'},
               {'id': '131', 'dst_ip': '10.1.0.3', 'dst_mac': '00:11:22:AA:BB:33'},
               {'id': '141', 'dst_ip': '10.1.0.4', 'dst_mac': '00:FF:12:34:56:78'},
               {'id': '151', 'dst_ip': '10.1.0.5', 'dst_mac': 'AA:11:22:AA:BB:33'}]

    vxlans2 = [{'id': '221', 'dst_ip': '10.2.0.2', 'dst_mac': '20:AA:BB:CC:11:22'},
               {'id': '231', 'dst_ip': '10.2.0.3', 'dst_mac': '20:11:22:AA:BB:33'}]
    
    interfaces = {}
    interfaces['eth0'] = {'vxlans': vxlans1, 'name': 'eth0',
                          'pci': '0', 'prefix': '10.1.0.0/24', 'ip': '10.1.0.1',
                          'mac': '00:10:22:A1:B2:C3'}
    interfaces['eth1'] = {'vxlans': vxlans2, 'name': 'eth1',
                          'pci': '1', 'prefix': '10.2.0.0/24', 'ip': '10.2.0.1',
                          'mac': '20:10:22:A1:B2:C3'}

    try:
        cb.createOutboundVXLANHandling({})
    except MoaInputError as mie:
        if(mie.iput != "Length of interfaces <= 0"):
            raise

    tw.clear()
    cb.createOutboundVXLANHandling({'eth0': interfaces['eth0']})

    expected_fh = open("builder_expected/outbound_vxlan_test1.click", "r")
    expected = expected_fh.read()
    expected_fh.close()
    tw.validateTest(expected, "Output mismatch", error_msg_hdr="createOutboundVXLANHandling")

    tw.clear()
    cb.createOutboundVXLANHandling(interfaces)

    expected_fh = open("builder_expected/outbound_vxlan_test2.click", "r")
    expected = expected_fh.read()
    expected_fh.close()
    tw.validateTest(expected, "Output mismatch", error_msg_hdr="createOutboundVXLANHandling")

    validateTest(interfaces['eth0']['todevice_element'], 'eth0_dpdk_out', "ToDevice Element Mismatch",
                 error_msg_hdr="createOutboundVXLANHandling")
    validateTest(interfaces['eth1']['todevice_element'], 'eth1_dpdk_out', "ToDevice Element Mismatch",
                 error_msg_hdr="createOutboundVXLANHandling")

    output_vxlan_encaps = ['encap_vxlan_121', 'encap_vxlan_131', 'encap_vxlan_141', 'encap_vxlan_151']
    validateTest(interfaces['eth0']['output_vxlan_encaps'], output_vxlan_encaps, "VXLAN Encaps Mismatch",
                 error_msg_hdr="createOutboundVXLANHandling")
 
    output_vxlan_encaps = ['encap_vxlan_221', 'encap_vxlan_231']
    validateTest(interfaces['eth1']['output_vxlan_encaps'], output_vxlan_encaps, "VXLAN Encaps Mismatch",
                 error_msg_hdr="createOutboundVXLANHandling")
   
    print "Outbound VXLAN Handling Passed"
    return 0
    
def testUpdateLinkIOElements(cb):
    # create inputs!
    vxlans1 = [{'id': '121', 'dst_ip': '10.1.0.2', 'dst_mac': '00:AA:BB:CC:11:22'},
               {'id': '131', 'dst_ip': '10.1.0.3', 'dst_mac': '00:11:22:AA:BB:33'},
               {'id': '141', 'dst_ip': '10.1.0.4', 'dst_mac': '00:FF:12:34:56:78'},
               {'id': '151', 'dst_ip': '10.1.0.5', 'dst_mac': 'AA:11:22:AA:BB:33'}]

    vxlans2 = [{'id': '221', 'dst_ip': '10.2.0.2', 'dst_mac': '20:AA:BB:CC:11:22'},
               {'id': '231', 'dst_ip': '10.2.0.3', 'dst_mac': '20:11:22:AA:BB:33'}]
    
    interfaces = {}
    interfaces['eth0'] = {'vxlans': vxlans1, 'name': 'eth0',
                          'pci': '0', 'prefix': '10.1.0.0/24', 'ip': '10.1.0.1',
                          'mac': '00:10:22:A1:B2:C3',
                          'todevice_element': 'out1',
                          'input_element': 'in1',
                          'output_vxlan_encaps': ['vxlan_encap_121',
                                                  'vxlan_encap_131',
                                                  'vxlan_encap_141',
                                                  'vxlan_encap_151']}
    interfaces['eth1'] = {'vxlans': vxlans2, 'name': 'eth1',
                          'pci': '1', 'prefix': '10.2.0.0/24', 'ip': '10.2.0.1',
                          'mac': '20:10:22:A1:B2:C3',
                          'todevice_element': 'out2',
                          'input_element': 'in2',
                          'output_vxlan_encaps': ['vxlan_encap_221',
                                                  'vxlan_encap_231']}

    # Same interface links
    link1 = {'left_vxlan': '121', 'right_vxlan': '131',
             'left_interface': interfaces['eth0'],
             'right_interface': interfaces['eth0'],
             'id': '1',
             'args': {'loss': '0', 'bandwidth': '10Gbps', 'delay': '0ms', 'useCodel': True}}
    link2 = {'left_vxlan': '141', 'right_vxlan': '151',
             'left_interface': interfaces['eth0'],
             'right_interface': interfaces['eth0'],
             'id': '2',
             'args': {'loss': '0', 'bandwidth': '100Gbps', 'delay': '0ms', 'useCodel': True}}
    link3 = {'left_vxlan': '221', 'right_vxlan': '231',
             'left_interface': interfaces['eth1'],
             'right_interface': interfaces['eth1'],
             'id': '3',
             'args': {'loss': '0', 'bandwidth': '10Gbps', 'delay': '0ms', 'useCodel': True}}

    # Cross interface links
    clink1 = {'left_vxlan': '121', 'right_vxlan': '221',
              'left_interface': interfaces['eth0'],
              'right_interface': interfaces['eth1'],
              'id': '4',
              'args': {'loss': '0', 'bandwidth': '10Gbps', 'delay': '0ms', 'useCodel': True}}
    clink2 = {'left_vxlan': '131', 'right_vxlan': '231',
              'left_interface': interfaces['eth0'],
              'right_interface': interfaces['eth1'],
              'id': '5',
              'args': {'loss': '0', 'bandwidth': '10Gbps', 'delay': '0ms', 'useCodel': True}}
  
    # test 0 links
    try:
        cb.updateLinkIOElements([])
    except MoaInputError as mie:
        if(mie.iput != "Length of links <= 0"):
            raise

    # test 1 link
    cb.updateLinkIOElements([link1])
    validateTest(link1['left_inbound'], "in1[0]", "Inbound Element Mismatch",
                 error_msg_hdr = "updateLinkIOElements")
    validateTest(link1['right_inbound'], "in1[1]", "Inbound Element Mismatch",
                 error_msg_hdr = "updateLinkIOElements")
    
    validateTest(link1['left_outbound'], "vxlan_encap_121", "Inbound Element Mismatch",
                 error_msg_hdr = "updateLinkIOElements")
    validateTest(link1['right_outbound'], "vxlan_encap_131", "Inbound Element Mismatch",
                 error_msg_hdr = "updateLinkIOElements")

    # reset
    link1['left_inbound'] = ''
    link1['right_inbound'] = ''
    link1['left_outbound'] = ''
    link1['right_outbound'] = ''

    # test 2 links, same interface
    cb.updateLinkIOElements([link1, link2])
    validateTest(link1['left_inbound'], "in1[0]", "Inbound Element Mismatch",
                 error_msg_hdr = "updateLinkIOElements")
    validateTest(link1['right_inbound'], "in1[1]", "Inbound Element Mismatch",
                 error_msg_hdr = "updateLinkIOElements")
    validateTest(link2['left_inbound'], "in1[2]", "Inbound Element Mismatch",
                 error_msg_hdr = "updateLinkIOElements")
    validateTest(link2['right_inbound'], "in1[3]", "Inbound Element Mismatch",
                 error_msg_hdr = "updateLinkIOElements")
    
    validateTest(link1['left_outbound'], "vxlan_encap_121", "Inbound Element Mismatch",
                 error_msg_hdr = "updateLinkIOElements")
    validateTest(link1['right_outbound'], "vxlan_encap_131", "Inbound Element Mismatch",
                 error_msg_hdr = "updateLinkIOElements")
    validateTest(link2['left_outbound'], "vxlan_encap_141", "Inbound Element Mismatch",
                 error_msg_hdr = "updateLinkIOElements")
    validateTest(link2['right_outbound'], "vxlan_encap_151", "Inbound Element Mismatch",
                 error_msg_hdr = "updateLinkIOElements")

    # reset
    link1['left_inbound'] = ''
    link1['right_inbound'] = ''
    link1['left_outbound'] = ''
    link1['right_outbound'] = ''
    link2['left_inbound'] = ''
    link2['right_inbound'] = ''
    link2['left_outbound'] = ''
    link2['right_outbound'] = ''

    # test 2 links, different interfaces
    cb.updateLinkIOElements([link1, link3])
    validateTest(link1['left_inbound'], "in1[0]", "Inbound Element Mismatch",
                 error_msg_hdr = "updateLinkIOElements")
    validateTest(link1['right_inbound'], "in1[1]", "Inbound Element Mismatch",
                 error_msg_hdr = "updateLinkIOElements")
    validateTest(link3['left_inbound'], "in2[0]", "Inbound Element Mismatch",
                 error_msg_hdr = "updateLinkIOElements")
    validateTest(link3['right_inbound'], "in2[1]", "Inbound Element Mismatch",
                 error_msg_hdr = "updateLinkIOElements")
    
    validateTest(link1['left_outbound'], "vxlan_encap_121", "Inbound Element Mismatch",
                 error_msg_hdr = "updateLinkIOElements")
    validateTest(link1['right_outbound'], "vxlan_encap_131", "Inbound Element Mismatch",
                 error_msg_hdr = "updateLinkIOElements")
    validateTest(link3['left_outbound'], "vxlan_encap_221", "Inbound Element Mismatch",
                 error_msg_hdr = "updateLinkIOElements")
    validateTest(link3['right_outbound'], "vxlan_encap_231", "Inbound Element Mismatch",
                 error_msg_hdr = "updateLinkIOElements")

    # test cross links - 3 links, 2 cross interfaces
    cb.updateLinkIOElements([clink1, clink2, link2])
    validateTest(clink1['left_inbound'], "in1[0]", "Inbound Element Mismatch",
                 error_msg_hdr = "updateLinkIOElements")
    validateTest(clink1['right_inbound'], "in2[0]", "Inbound Element Mismatch",
                 error_msg_hdr = "updateLinkIOElements")
    validateTest(clink2['left_inbound'], "in1[1]", "Inbound Element Mismatch",
                 error_msg_hdr = "updateLinkIOElements")
    validateTest(clink2['right_inbound'], "in2[1]", "Inbound Element Mismatch",
                 error_msg_hdr = "updateLinkIOElements")
    
    validateTest(clink1['left_outbound'], "vxlan_encap_121", "Inbound Element Mismatch",
                 error_msg_hdr = "updateLinkIOElements")
    validateTest(clink1['right_outbound'], "vxlan_encap_221", "Inbound Element Mismatch",
                 error_msg_hdr = "updateLinkIOElements")
    validateTest(clink2['left_outbound'], "vxlan_encap_131", "Inbound Element Mismatch",
                 error_msg_hdr = "updateLinkIOElements")
    validateTest(clink2['right_outbound'], "vxlan_encap_231", "Inbound Element Mismatch",
                 error_msg_hdr = "updateLinkIOElements")


    validateTest(link2['left_inbound'], "in1[2]", "Inbound Element Mismatch",
                 error_msg_hdr = "updateLinkIOElements")
    validateTest(link2['right_inbound'], "in1[3]", "Inbound Element Mismatch",
                 error_msg_hdr = "updateLinkIOElements")
    
    validateTest(link2['left_outbound'], "vxlan_encap_141", "Inbound Element Mismatch",
                 error_msg_hdr = "updateLinkIOElements")
    validateTest(link2['right_outbound'], "vxlan_encap_151", "Inbound Element Mismatch",
                 error_msg_hdr = "updateLinkIOElements")


    # Need to test native handling here!
    
    print "Update Link IO Elements Passed"
    return 0

if __name__ == "__main__":
    main()
