#include "randommobility.hh"
#include <math.h>
#include <iostream>
#include <fstream>

RandomMobility::RandomMobility()
  : Mobility(), _move_probability(0),
    _move_speed(0), _direction_segments(1),
    _discrete_angle(0), _angle_calculator_multiplier(0)
{
  _distribution = std::uniform_real_distribution<double>(0.0, 1.0);
}

RandomMobility::RandomMobility(double m_prob, double speed,
                               int directions)
  : Mobility(), _move_probability(m_prob),
    _move_speed(speed), _direction_segments(directions)
{
  setup();
}

RandomMobility::~RandomMobility()
{

}

void
RandomMobility::setup()
{
  double pi = 3.14159265;
  if(_direction_segments < 4)
  {
    _direction_segments = 4;
  }
  _distribution = std::uniform_real_distribution<double>(0.0, 1.0);
  _discrete_angle = (2 * pi) / _direction_segments;
  _angle_calculator_multiplier = 100.0 / _direction_segments;
}


void
RandomMobility::updateNode(WirelessNode* node, double &dx, double &dy)
{
  int angle_multiplier = 0;
  if(_distribution(_generator) <= _move_probability)
  {
    angle_multiplier = floor(_distribution(_generator) *
                             _angle_calculator_multiplier);
    if(angle_multiplier < 0)
    {
      click_chatter("this is bad");
    }
    else {
      double angle = _discrete_angle * angle_multiplier;
      dx = _move_speed * cos(angle);
      dy = _move_speed * sin(angle);
    }
  }
}

void
RandomMobility::configureFromFile(std::string filename)
{
  std::ifstream f_handler;
  double m_prob = -1.0;
  double speed = -1.0;
  int directions = 0;

  f_handler.open(filename);

  if(f_handler)
  {
    while(f_handler >> m_prob >> speed >> directions)
    {
      // This should only run once
      break;
    }

    if(m_prob == -1.0 || speed == -1.0 || directions == 0)
    {
      f_handler.close();
      return;
    }

    _move_probability = m_prob;
    _move_speed = speed;
    _direction_segments = directions;

    setup();
    f_handler.close();
  }
}

CLICK_ENDDECLS
ELEMENT_PROVIDES(RandomMobility)
