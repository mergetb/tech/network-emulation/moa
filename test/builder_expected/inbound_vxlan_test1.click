## DPDK Input for Device eth0 (PCI 0)
eth0_dpdk_in :: FromDPDKDevice(0);

## VXLAN Demux for Interface eth0 (PCI 0)
vxlan_demux_0 :: VXLANDemux(VXLAN 121, VXLAN 131, VXLAN 141, VXLAN 151);

## Classifier to seperate ARP requests for VXLAN Demux 0
vxlan_0_classifier :: Classifier(12/0806 20/0001, -);

## Interface eth0 (PCI 0) ARP Responder
arp_responder_vxlan_0 :: ARPResponder(10.1.0.0/24 00:10:22:A1:B2:C3);

## DPDK Packet Arrival Handling for Device eth0 (PCI 0)
eth0_dpdk_in -> vxlan_0_classifier;

## VXLAN Demux 0 Classifier Outputs
vxlan_0_classifier[0] -> arp_responder_vxlan_0 -> out1;
vxlan_0_classifier[1] -> vxlan_demux_0;
vxlan_0_classifier[2] -> Print("Unknown Packet") -> Discard();

## VXLAN Demux 0 error pipeline
vxlan_demux_0 -> Print("Unknown Packet from VXLAN Demux 0") -> Discard();

