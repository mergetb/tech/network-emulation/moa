import mergexp as mx
from mergexp.unit import gbps
from mergexp.net import capacity
from mergexp.mime import (Mime, Flow)

topo = mx.Topology('mime-basic')

a = Mime(
    name = 'a',
    mac = '00:00:00:00:00:01',
    ip = '10.0.0.1',
    flows = [Flow(
        compute = '10ms',
        data = int(1e8),
        dst = '10.0.0.2',
        mac = '00:00:00:00:00:02'
    )],
    stop = True
)

b = Mime(
    name = 'b',
    ip = '10.0.0.2',
    mac = '00:00:00:00:00:02',
    flows = [Flow(
        compute = '50ms',
    )]
)

topo.add_devices(a,b)
topo.connect([a,b], capacity == gbps(1))

mx.experiment(topo)
