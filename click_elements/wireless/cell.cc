#include "cell.hh"

CLICK_DECLS

Cell::Cell()
  : _x_min(0), _x_max(0), _y_min(0), _y_max(0),
    _x_pos(0), _y_pos(0)
{

}

Cell::Cell(double x_min, double x_max,
           double y_min, double y_max,
           int cell_x_pos, int cell_y_pos)
  : _x_min(x_min), _x_max(x_max), _y_min(y_min), _y_max(y_max),
    _x_pos(cell_x_pos), _y_pos(cell_y_pos)
{

}

Cell::~Cell()
{

}

std::set<WirelessNode*>* 
Cell::checkReachable(WirelessNode* node)
{
  std::set<WirelessNode*>* reachable = new std::set<WirelessNode*>();
  for (auto const & node_to_check : _nodes)
  {
    if(node != node_to_check &&
       node->checkReachability(node_to_check))
    {
      reachable->insert(node_to_check);
    }
  }
  return reachable;
}

void
Cell::addNode(WirelessNode* node)
{
  _nodes.push_back(node);
}

void
Cell::removeNode(WirelessNode* node)
{
  _nodes.remove(node);
}

CLICK_ENDDECLS
ELEMENT_PROVIDES(Cell)
