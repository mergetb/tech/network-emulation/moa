// test using xdpvxlanvni on a single interface

x0 :: XDPDevice(DEV eth1, MODE skb, PROG xdpvxlanvni, VNI 47)
x1 :: XDPDevice(DEV eth1, MODE skb, PROG xdpvxlanvni, VNI 48)

x0 -> Queue(10) -> x1;
x1 -> Queue(10) -> x0;

