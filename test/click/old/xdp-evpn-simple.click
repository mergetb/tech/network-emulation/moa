// test using xdp with vtep devices under evpn control

vtep4 :: XDPDevice(DEV vtep4, MODE skb, PROG xdpallrx, TRACE false)
vtep7 :: XDPDevice(DEV vtep7, MODE skb, PROG xdpallrx, TRACE false)

vtep4 -> Queue(10000000) -> vtep7;
vtep7 -> Queue(10000000) -> vtep4;
