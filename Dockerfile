FROM busybox:glibc
COPY build/moactld /usr/bin/moactld
RUN mkdir /etc/moa

# moactld grpc
EXPOSE 15000

CMD /usr/bin/moactld
