#!/bin/bash

set -x

if [[ $UID -ne 0 ]]; then
  echo "must be root"
  exit 1
fi

ip link del veth10
ip netns del ns10

ip link del veth11
ip netns del ns11

