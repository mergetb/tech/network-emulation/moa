// mimes interconnected by a switch

a :: Mime(
  IP       10.0.0.1,
  MAC      00:00:00:00:00:01,

  COMPUTE0 10ms, 
  DATA0    100000000,
  DST0     10.0.1.1,
  MAC0     00:00:00:00:00:02,

  STOP     true
);

b :: Mime(
  IP      10.0.1.1,
  MAC     00:00:00:00:00:02,

  COMPUTE0 50ms
);

leaf0 :: MultipathRouter(
  10.0.0.0/24 0,
  10.0.1.0/24 1,
  10.0.1.0/24 2
);

leaf1 :: MultipathRouter(
  10.0.1.0/24 0,
  10.0.0.0/24 1,
  10.0.0.0/24 2
);

spine0 :: MultipathRouter(
  10.0.0.0/24 0,
  10.0.1.0/24 1,
);

spine1 :: MultipathRouter(
  10.0.0.0/24 0,
  10.0.1.0/24 1,
);

a -> [0]leaf0[0] -> Queue(100000) -> a;
b -> [0]leaf1[0] -> Queue(100000) -> b;

leaf0[1] -> [0]spine0[0] -> [1]leaf0;
leaf0[2] -> [0]spine1[0] -> [2]leaf0;

leaf1[1] -> [1]spine0[1] -> [1]leaf1;
leaf1[2] -> [1]spine1[1] -> [2]leaf1;
