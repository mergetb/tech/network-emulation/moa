package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
	"text/tabwriter"

	"github.com/ghodss/yaml"
	"github.com/spf13/cobra"
	"gitlab.com/mergetb/tech/network-emulation/moa/pkg/common"
	moa "gitlab.com/mergetb/tech/network-emulation/moa/pkg/manager"
)

var (
	host string
	port int
)

var tw = tabwriter.NewWriter(os.Stdout, 0, 0, 4, ' ', 0)

func main() {

	log.SetFlags(0)

	root := &cobra.Command{
		Use:     "moactl",
		Short:   "control moa emulators",
		Version: common.Version,
	}
	root.PersistentFlags().StringVarP(
		&host, "server", "s", "localhost", "Moa server to use")
	root.PersistentFlags().IntVarP(
		&port, "port", "p", 50052, "Moa port to use")

	create := &cobra.Command{
		Use:   "create <ID> <blob.json | blob.yml>",
		Short: "Create a new emulation",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			id, err := strconv.Atoi(args[0])
			if err != nil {
				log.Fatal(err)
			}
			createEmu(id, args[1])
		},
	}
	root.AddCommand(create)

	destroy := &cobra.Command{
		Use:   "destroy <ID>",
		Short: "Destroy an emulation",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			id, err := strconv.Atoi(args[0])
			if err != nil {
				log.Fatal(err)
			}
			destroyEmu(id)
		},
	}
	root.AddCommand(destroy)
	/*
		finalize := &cobra.Command{
			Use:   "finalize <ID>",
			Short: "Finalize an emulation",
			Args:  cobra.ExactArgs(1),
			Run: func(cmd *cobra.Command, args []string) {
				id, err := strconv.Atoi(args[0])
				if err != nil {
					log.Fatal(err)
				}
				finalizeEmu(id)
			},
		}
		root.AddCommand(finalize)
	*/

	start := &cobra.Command{
		Use:   "start <ID>",
		Short: "Start an emulation",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			id, err := strconv.Atoi(args[0])
			if err != nil {
				log.Fatal(err)
			}
			startEmu(id)
		},
	}
	root.AddCommand(start)

	stop := &cobra.Command{
		Use:   "stop <ID>",
		Short: "Stop an emulation",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			id, err := strconv.Atoi(args[0])
			if err != nil {
				log.Fatal(err)
			}
			stopEmu(id)
		},
	}
	root.AddCommand(stop)

	/*
		discard := &cobra.Command{
			Use:   "discard <ID>",
			Short: "Discard an emulation",
			Args:  cobra.ExactArgs(1),
			Run: func(cmd *cobra.Command, args []string) {
				id, err := strconv.Atoi(args[0])
				if err != nil {
					log.Fatal(err)
				}
				discardEmu(id)
			},
		}
		root.AddCommand(discard)
	*/

	list := &cobra.Command{
		Use:   "list",
		Short: "List emulations",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			list()
		},
	}
	root.AddCommand(list)

	show := &cobra.Command{
		Use:   "show <MZID>|<EMU-ID>",
		Short: "Show emulated links belonging to a materialization",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			show(args[0])
		},
	}
	root.AddCommand(show)

	build := &cobra.Command{
		Use:   "build",
		Short: "Generate CreateRequest JSON",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			build()
		},
	}
	root.AddCommand(build)

	root.Execute()

}

func addr() string { return fmt.Sprintf("%s:%d", host, port) }

func createEmu(id int, filename string) {

	blob, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatal(err)
	}

	// if we got a yaml file, translate to json to hand off to moa
	if strings.HasSuffix(filename, "yml") || strings.HasSuffix(filename, "yaml") {

		blob, err = yaml.YAMLToJSON(blob)
		if err != nil {
			log.Fatal(err)
		}

	}

	var req moa.CreateRequest
	err = json.Unmarshal(blob, &req)
	if err != nil {
		log.Fatal(err)
	}
	// overwrite what is in the json/yaml to allow multiple copies
	req.EmulationId = int32(id)

	moa.WithMoa(addr(), func(c moa.MoaClient) error {

		_, err := c.Create(context.TODO(), &req)
		if err != nil {
			log.Fatal(err)
		}

		return nil

	})

}

func destroyEmu(id int) {

	moa.WithMoa(addr(), func(c moa.MoaClient) error {

		_, err := c.Destroy(context.TODO(), &moa.EmulationRequest{EmulationId: int32(id)})
		if err != nil {
			log.Fatal(err)
		}

		return nil

	})

}

/*
func finalizeEmu(id int) {

	moa.WithMoa(addr(), func(c moa.MoaClient) error {

		_, err := c.FinalizeEmulation(context.TODO(), &moa.FinalizeEmulationRequest{
			EmulationId: int32(id),
		})

		if err != nil {
			log.Fatal(err)
		}
		return nil

	})

}
*/

func startEmu(id int) {

	moa.WithMoa(addr(), func(c moa.MoaClient) error {

		_, err := c.Start(context.TODO(), &moa.EmulationRequest{
			EmulationId: int32(id),
		})

		if err != nil {
			log.Fatal(err)
		}
		return nil

	})

}

func stopEmu(id int) {

	moa.WithMoa(addr(), func(c moa.MoaClient) error {

		_, err := c.Stop(context.TODO(), &moa.EmulationRequest{
			EmulationId: int32(id),
		})

		if err != nil {
			log.Fatal(err)
		}
		return nil

	})

}

/*
func discardEmu(id int) {

	moa.WithMoa(addr(), func(c moa.MoaClient) error {

		_, err := c.DiscardEmulation(context.TODO(), &moa.DiscardEmulationRequest{
			EmulationId: int32(id),
		})

		if err != nil {
			log.Fatal(err)
		}
		return nil

	})

}
*/

func cols(items ...string) string {
	return strings.Join(items, "\t")
}

func list() {

	moa.WithMoa(addr(), func(c moa.MoaClient) error {

		result, err := c.List(context.TODO(), &moa.ListRequest{})
		if err != nil {
			log.Fatal(err)
		}

		fmt.Fprintf(tw, cols("EMU-ID", "MZID", "ACTIVE\n"))
		for _, emu := range result.Emulations {

			fmt.Fprintf(tw,
				cols("%d", "%s", "%v\n"),
				emu.Id,
				emu.Mzid,
				emu.Pid > 0,
			)

		}

		tw.Flush()

		return nil

	})

}

func show(mzid string) {
	moa.WithMoa(addr(), func(c moa.MoaClient) error {

		result, err := c.Show(context.TODO(), &moa.ShowRequest{Mzid: mzid})
		if err != nil {
			log.Fatal(err)
		}

		fmt.Fprintf(tw, cols("INTERFACES", "CAPACITY", "LATENCY", "LOSS", "TAGS", "EXP NODES\n"))
		for _, lnk := range result.Links {

			fmt.Fprintf(tw,
				cols("%v", "%s", "%s", "%f", "%v", "%v\n"),
				lnk.Interfaces,
				common.PrettyCapacity(lnk.Params.Capacity),
				common.PrettyLatency(lnk.Params.Latency),
				lnk.Params.Loss,
				lnk.Tags,
				lnk.ExpNodes,
			)

		}

		tw.Flush()

		return nil

	})
}

func build() {
	req := moa.CreateRequest{
		EmulationId: 100,
		Mzid:        "test.simple.elkins",
		Links: []*moa.LinkSpec{
			&moa.LinkSpec{
				Interfaces: []string{"vtep100", "vtep101"},
				Params: &moa.LinkParams{
					Capacity: 100 * 1000 * 1000, //bps
					Latency:  50 * 1000 * 1000,  //ms
					Loss:     0.03,
				},
				Tags:     []string{"a", "wan"},
				ExpNodes: []string{"a", "b"},
			},
			&moa.LinkSpec{
				Interfaces: []string{"vtep102", "vtep103"},
				Params: &moa.LinkParams{
					Capacity: 100 * 1000 * 1000, //bps
					Latency:  50 * 1000 * 1000,  //ms
					Loss:     0.03,
				},
				Tags:     []string{"b", "wan"},
				ExpNodes: []string{"b", "c"},
			},
		},
	}

	buf, err := json.Marshal(req)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(string(buf))
}
