#!/usr/bin/env python3

import subprocess
import sys
import re

if len(sys.argv) < 3:
    print('usage: validate-ping.py <host> <latency>', file=sys.stderr)
    sys.exit(1)

target = sys.argv[1]
latency = float(sys.argv[2])

rtt_re = re.compile('rtt.*/([0-9.]+)/[0-9.]+/[0-9.]+ ms')

o = subprocess.run(["ping", "-q", "-c", "30", target], capture_output=True)
if o.returncode != 0:
    print(o.stderr)
    sys.exit(1)
s = o.stdout.decode()
g = rtt_re.search(s)
avg = float(g.group(1)) / 2.0

if avg < latency*0.9 or avg > latency*1.1:
    print('expected latency of %f but got %f\n' % (latency, avg), file=sys.stderr)
    print(s, file=sys.stderr)
    sys.exit(1)

print('measured latency %fms' % avg)

sys.exit(0)
