package main

import (
	"context"
	"fmt"
	"os"
	"strconv"
	"strings"
	"text/tabwriter"

	"github.com/spf13/cobra"

	"gitlab.com/mergetb/tech/network-emulation/moa/pkg/common"
	"gitlab.com/mergetb/tech/network-emulation/moa/pkg/moactld"
)

var (
	host string
)

func parse_capacity(s string) int64 {

	var v string
	var base int64

	if strings.HasSuffix(s, "Gbps") || strings.HasSuffix(s, "gbps") {
		// gbits/sec
		v = s[0 : len(s)-4]
		base = 1000 * 1000 * 1000
	} else if strings.HasSuffix(s, "G") || strings.HasSuffix(s, "g") {
		v = s[0 : len(s)-1]
		base = 1000 * 1000 * 1000
	} else if strings.HasSuffix(s, "Mbps") || strings.HasSuffix(s, "mbps") {
		// mbits/sec
		v = s[0 : len(s)-4]
		base = 1000 * 1000
	} else if strings.HasSuffix(s, "M") || strings.HasSuffix(s, "m") {
		v = s[0 : len(s)-1]
		base = 1000 * 1000
	} else if strings.HasSuffix(s, "Kbps") || strings.HasSuffix(s, "kbps") {
		// kbits/sec
		v = s[0 : len(s)-4]
		base = 1000
	} else if strings.HasSuffix(s, "K") || strings.HasSuffix(s, "k") {
		v = s[0 : len(s)-1]
		base = 1000
	} else if strings.HasSuffix(s, "bps") {
		// bits/sec
		v = s[0 : len(s)-3]
		base = 1
	} else {
		// bits/sec
		v = s
		base = 1
	}

	r, err := strconv.ParseInt(v, 10, 64)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %s\n", v, err)
		os.Exit(1)
	}
	if r < 0 {
		fmt.Fprintf(os.Stderr, "%d: invalid capacity value\n", r)
	}
	return r * base
}

func parse_latency(s string) int32 {
	var v string
	var base int32
	if strings.HasSuffix(s, "ns") {
		base = 1
		v = s[0 : len(s)-2]
	} else if strings.HasSuffix(s, "us") {
		base = 1000
		v = s[0 : len(s)-2]
	} else if strings.HasSuffix(s, "ms") {
		base = 1000 * 1000
		v = s[0 : len(s)-2]
	} else if strings.HasSuffix(s, "s") {
		base = 1000 * 1000 * 1000
		v = s[0 : len(s)-1]
	} else {
		// assume ms
		v = s
		base = 1000 * 1000
	}
	r, err := strconv.ParseInt(v, 10, 32)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %s\n", v, err)
		os.Exit(1)
	}
	if r < 0 {
		fmt.Fprintf(os.Stderr, "%d: invalid latency value\n", r)
	}
	return int32(r) * base
}

func doSet(values []string) {
	req := &moactld.UpdateRequest{
		Tags: []string{values[0]},
	}

	for j := 1; j < len(values); j = j + 2 {
		param := values[j]
		value := values[j+1]

		if param == "capacity" || param == "bandwidth" || param == "bw" || param == "cap" {
			req.Capacity = parse_capacity(value)
		} else if param == "latency" || param == "delay" || param == "dly" || param == "lat" {
			req.Latency = parse_latency(value)
		} else if param == "loss" {
			v, err := strconv.ParseFloat(value, 32)
			if err != nil {
				fmt.Fprintf(os.Stderr, "%s: %s\n", v, err)
				os.Exit(1)
			}
			if v < 0.0 || v > 1.0 {
				fmt.Fprintf(os.Stderr, "%d: invalid loss value (must be betweeen 0.0 and 1.0)\n", v)
				os.Exit(1)
			}
			req.Loss = float32(v)
			req.EnforceLoss = true
		} else {
			fmt.Fprintf(os.Stderr, "%s: invalid link parameter\n", param)
			os.Exit(1)
		}
	}

	err := moactld.WithMoaControl(host, func(c moactld.MoaControlClient) error {
		_, r := c.Update(context.TODO(), req)
		return r
	})
	if err != nil {
		fmt.Fprintln(os.Stderr, "rpc to moactld failed: %v", err)
		os.Exit(1)
	}
}

func cols(items ...string) string {
	return strings.Join(items, "\t")
}

func doShow() {
	err := moactld.WithMoaControl(host, func(c moactld.MoaControlClient) error {
		resp, err := c.Show(context.TODO(), &moactld.ShowRequest{})
		if err != nil {
			fmt.Fprintln(os.Stderr, "rpc to moactld failed: %v", err)
			os.Exit(1)
		}

		tw := tabwriter.NewWriter(os.Stdout, 0, 0, 4, ' ', 0)
		fmt.Fprintf(tw, cols("NODES", "CAPACITY", "LATENCY", "LOSS", "TAGS\n"))
		for _, lnk := range resp.Links {
			fmt.Fprintf(tw,
				cols("%v", "%s", "%s", "%f", "%v\n"),
				lnk.ExpNodes,
				common.PrettyCapacity(lnk.Params.Capacity),
				common.PrettyLatency(lnk.Params.Latency),
				lnk.Params.Loss,
				lnk.Tags,
			)

		}

		tw.Flush()

		return nil
	})

	if err != nil {
		fmt.Fprintln(os.Stderr, "rpc to moactld failed: %v", err)
		os.Exit(1)
	}
}

func main() {
	var rootCmd = &cobra.Command{
		Use:     "moacmd",
		Short:   "moacmd is a tool for dynamically configuring the moa emulator",
		Version: common.Version,
	}
	rootCmd.PersistentFlags().StringVarP(&host, "host", "s", "moactl", "specify the host for moactld")

	var setCmd = &cobra.Command{
		Use:   "set <tag> <param> <value> [ <param> <value> ...]",
		Short: "set a parameter on the set of links by tag",
		Long: `Changes the named link parameter.

[tag] refers to the user tags applied to the links in your model. All links with [tag] will be updated by this command.

[param] may be any of:
  capacity, cap, bandwidth, bw
  latency, delay, lat, dly
  loss

[value] can be of the following forms:
  capacity: 1G 1Gbps 1gbps 10M 10Mbps 10mbps 500K 500Kbps 500kbps (if no unit is specified, it is interpretted as bits/sec)
  latency:  1s 500us 50ms 20000ns (if no unit is specified, it is interpretted as ms)
  loss:     range from 0.0 to 1.0 (packet loss probability)`,
		Args: cobra.MinimumNArgs(3),
		Run: func(cmd *cobra.Command, args []string) {
			doSet(args)
		},
	}
	rootCmd.AddCommand(setCmd)

	var showCmd = &cobra.Command{
		Use:   "show",
		Short: "show current link configurations",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			doShow()
		},
	}
	rootCmd.AddCommand(showCmd)

	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
