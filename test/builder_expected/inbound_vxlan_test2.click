## DPDK Input for Device eth1 (PCI 1)
eth1_dpdk_in :: FromDPDKDevice(1);

## VXLAN Demux for Interface eth1 (PCI 1)
vxlan_demux_0 :: VXLANDemux(VXLAN 221, VXLAN 231);

## Classifier to seperate ARP requests for VXLAN Demux 0
vxlan_0_classifier :: Classifier(12/0806 20/0001, -);

## Interface eth1 (PCI 1) ARP Responder
arp_responder_vxlan_0 :: ARPResponder(10.2.0.0/24 20:10:22:A1:B2:C3);

## DPDK Packet Arrival Handling for Device eth1 (PCI 1)
eth1_dpdk_in -> vxlan_0_classifier;

## VXLAN Demux 0 Classifier Outputs
vxlan_0_classifier[0] -> arp_responder_vxlan_0 -> out2;
vxlan_0_classifier[1] -> vxlan_demux_0;
vxlan_0_classifier[2] -> Print("Unknown Packet") -> Discard();

## VXLAN Demux 0 error pipeline
vxlan_demux_0 -> Print("Unknown Packet from VXLAN Demux 0") -> Discard();

## DPDK Input for Device eth0 (PCI 0)
eth0_dpdk_in :: FromDPDKDevice(0);

## VXLAN Demux for Interface eth0 (PCI 0)
vxlan_demux_1 :: VXLANDemux(VXLAN 121, VXLAN 131, VXLAN 141, VXLAN 151);

## Classifier to seperate ARP requests for VXLAN Demux 1
vxlan_1_classifier :: Classifier(12/0806 20/0001, -);

## Interface eth0 (PCI 0) ARP Responder
arp_responder_vxlan_1 :: ARPResponder(10.1.0.0/24 00:10:22:A1:B2:C3);

## DPDK Packet Arrival Handling for Device eth0 (PCI 0)
eth0_dpdk_in -> vxlan_1_classifier;

## VXLAN Demux 1 Classifier Outputs
vxlan_1_classifier[0] -> arp_responder_vxlan_1 -> out1;
vxlan_1_classifier[1] -> vxlan_demux_1;
vxlan_1_classifier[2] -> Print("Unknown Packet") -> Discard();

## VXLAN Demux 1 error pipeline
vxlan_demux_1 -> Print("Unknown Packet from VXLAN Demux 1") -> Discard();

