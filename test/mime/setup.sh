#!/bin/bash

if [[ $UID -ne 0 ]]; then
  echo "must be root"
  exit 1
fi

set -x

ip netns add green

ip link add vgreen type veth peer eth0
ip link set netns green eth0
ip netns exec green ip addr add 10.0.0.1/24 dev eth0
ip netns exec green ip link set up dev eth0
ip link set up dev vgreen

ip netns add blue
ip link add vblue type veth peer eth0
ip link set netns blue eth0
ip netns exec blue ip addr add 10.0.0.2/24 dev eth0
ip netns exec blue ip link set up dev eth0
ip link set up dev vblue
