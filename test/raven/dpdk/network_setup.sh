#!/bin/bash

set -x

if [[ $UID -ne 0 ]]; then
  echo "must be root"
  exit 1
fi

ip link set up dev lo

# left side ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# namespace
ip netns add ns10

# veth
ip link add eth0 type veth peer veth10
ip link set up dev veth10
ip link set netns ns10 dev eth0
ip netns exec ns10 ip link set addr 00:11:00:22:00:10 dev eth0
ip netns exec ns10 ip addr add 10.0.0.10/24 dev eth0
ip netns exec ns10 ip link set up dev eth0

# right side ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# namespace
ip netns add ns11

# veth
ip link add eth0 type veth peer veth11
ip link set up dev veth11
ip link set netns ns11 dev eth0
ip netns exec ns11 ip link set addr 00:11:00:22:00:11 dev eth0
ip netns exec ns11 ip addr add 10.0.0.11/24 dev eth0
ip netns exec ns11 ip link set up dev eth0

